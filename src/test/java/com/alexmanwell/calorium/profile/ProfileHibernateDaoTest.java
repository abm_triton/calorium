package com.alexmanwell.calorium.profile;

import org.junit.Test;

import javax.persistence.NoResultException;
import java.util.Collection;
import java.util.Objects;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ProfileHibernateDaoTest {

    @Test
    public void loginProfile() throws Exception {
        final ProfileHibernateDao hibernateDao = new ProfileHibernateDao();
        String nickname = "test";
        String password = "test";
        Profile profile = hibernateDao.loginProfile(nickname, CryptPassword.cryptPassword(password));
        assertTrue(Objects.equals(profile.getNickname(), nickname));
        assertTrue(Objects.equals(profile, new Profile(profile.getId(), "test", "test@mail.com")));
    }

    @Test
    public void createProfile() throws Exception {
        final ProfileHibernateDao hibernateDao = new ProfileHibernateDao();
        String nickname = "test1";
        String password = "test1";
        String mail = "test1@mail.com";
        Profile profile = new Profile(nickname, mail);
        hibernateDao.createProfile(profile, CryptPassword.cryptPassword(password));

        profile = hibernateDao.searchProfile(nickname);
        assertTrue(Objects.equals(profile.getNickname(), nickname));
        assertTrue(Objects.equals(profile, new Profile(profile.getId(), nickname, mail)));

        hibernateDao.deleteProfile(nickname);
    }

    @Test(expected = NoResultException.class)
    public void deleteProfile() throws Exception {
        final ProfileHibernateDao hibernateDao = new ProfileHibernateDao();

        String nickname = "test1";
        hibernateDao.createProfile(new Profile(nickname, "test1@mail.com"), CryptPassword.cryptPassword("test1"));

        hibernateDao.deleteProfile(nickname);
        hibernateDao.searchProfile(nickname);
    }

    @Test
    public void editProfile() throws Exception {
        final ProfileHibernateDao hibernateDao = new ProfileHibernateDao();
        String nickname = "test3";
        String password = "test5";
        Random random = new Random();
        String mail = "test5@mail.com" + random.nextInt(10);
        Profile profile = new Profile(nickname, mail);
        hibernateDao.editProfile(profile, CryptPassword.cryptPassword(password));

        profile = hibernateDao.searchProfile(nickname);
        assertTrue(Objects.equals(profile.getMail(), mail));
    }

    @Test
    public void searchProfile() throws Exception {
        final ProfileHibernateDao hibernateDao = new ProfileHibernateDao();
        String nickname = "test";
        Profile profile = hibernateDao.searchProfile(nickname);

        assertTrue(Objects.equals(profile.getNickname(), nickname));
        assertTrue(Objects.equals(profile, new Profile(profile.getId(), "test", "test@mail.com")));
    }

    @Test
    public void searchProfileNicknamePassword() throws Exception {
        final ProfileHibernateDao hibernateDao = new ProfileHibernateDao();
        String nickname = "test";
        String password = "test";
        Profile profile = hibernateDao.loginProfile(nickname, CryptPassword.cryptPassword(password));

        assertTrue(Objects.equals(profile.getNickname(), nickname));
        assertTrue(Objects.equals(profile, new Profile(profile.getId(), "test", "test@mail.com")));
    }

    @Test
    public void searchAllProfiles() throws Exception {
        final ProfileHibernateDao hibernateDao = new ProfileHibernateDao();
        Collection<Profile> profiles = hibernateDao.searchAllProfiles();
        for (Profile profile : profiles) {
            System.out.println(profile);
        }
    }
}