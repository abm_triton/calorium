package com.alexmanwell.calorium.disheating;

import com.alexmanwell.calorium.eating.UserEating;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class UserDishEatingHibernateDaoTest {
    @Test
    public void add() throws Exception {
        final UserDishEatingDao hibernateDao = new UserDishEatingHibernateDao();

        UserEating.Builder b = new UserEating.Builder();
        b.setProfileId(1);
        b.setEatingId(35);
        UserEating userEating = b.build();

        UserDishEating ude = hibernateDao.add(new UserDishEating(2, 200, userEating));
        assertEquals(ude.getDishEatingId(), hibernateDao.select(ude).getDishEatingId());

        hibernateDao.delete(ude.getDishEatingId());
    }

    @Test (expected = IllegalStateException.class)
    public void delete() throws Exception {
        final UserDishEatingDao hibernateDao = new UserDishEatingHibernateDao();

        UserEating.Builder b = new UserEating.Builder();
        b.setProfileId(1);
        b.setEatingId(35);
        UserEating userEating = b.build();

        UserDishEating ude = hibernateDao.add(new UserDishEating(2, 200, userEating));

        hibernateDao.add(ude);
        hibernateDao.delete(ude.getDishEatingId());
        hibernateDao.select(ude.getDishEatingId());
    }

    @Test
    public void update() throws Exception {
        final UserDishEatingDao hibernateDao = new UserDishEatingHibernateDao();

        UserDishEating ude = hibernateDao.select(1);
        ude.setDishId(3);
        ude.setAmountDish(300);
        hibernateDao.update(ude);

        assertEquals(ude.getDishId(), hibernateDao.select(ude.getDishEatingId()).getDishId());
    }

    @Test
    public void select() throws Exception {
        final UserDishEatingDao hibernateDao = new UserDishEatingHibernateDao();

        UserDishEating userDishEating = hibernateDao.select(1);

        assertEquals(100, userDishEating.getAmountDish(), 0.1);
        assertEquals(5, userDishEating.getUserEating().getEatingId());
        assertEquals(3, userDishEating.getDishId());
        assertNotNull(userDishEating.getDishEatingId());
    }

    @Test
    public void select1() throws Exception {

    }

}