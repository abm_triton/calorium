package com.alexmanwell.calorium.eating;

import com.alexmanwell.calorium.disheating.UserDishEating;
import com.alexmanwell.calorium.disheating.UserDishEatingHibernateDao;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class UserEatingHibernateDaoTest {
    @Test
    public void addEating() throws Exception {

    }

    @Test
    public void addDishEating() throws Exception {

    }

    @Test
    public void removeDishEating() throws Exception {

    }

    @Test
    public void editEating() throws Exception {

    }

    @Test
    public void searchUserEating() throws Exception {

    }

    @Test
    public void searchUserEatingToday() throws Exception {

    }

    @Test
    public void selectMealtime() throws Exception {
        final UserEatingHibernateDao hibernateDao = new UserEatingHibernateDao();

        UserEating.Builder b = new UserEating.Builder();
        String mealtime = "Завтрак";
        Date date = ParseUtils.parseDate("2016-09-27 08:00:00");
        b.setProfileId(1).setMealtime(mealtime).setDateEating(date).setEatingId(110);
        UserEating userEating = b.build();

        userEating = hibernateDao.selectMealtime(userEating);

        assertNotNull(userEating.getEatingId());
    }

    @Test
    public void createMealtime() throws Exception {
        final UserEatingHibernateDao hibernateDao = new UserEatingHibernateDao();
        final UserDishEatingHibernateDao udeHibernateDao = new UserDishEatingHibernateDao();

        UserEating.Builder b = new UserEating.Builder();

        String mealtime = "Завтрак";
        Date date = ParseUtils.parseDate("2016-10-19 08:00:00");

        b.setProfileId(3).setMealtime(mealtime).setDateEating(date);
        UserEating userEating = b.build();

        userEating = hibernateDao.createMealtime(userEating);
        userEating = hibernateDao.selectMealtime(userEating);
        UserDishEating firstDish = udeHibernateDao.add(new UserDishEating(2, 630, new UserEating(userEating.getEatingId())));
        UserDishEating secondDish = udeHibernateDao.add(new UserDishEating(3, 425, new UserEating(userEating.getEatingId())));

        assertEquals(mealtime, userEating.getMealtime());
        assertEquals(date, userEating.getDate());

        udeHibernateDao.delete(firstDish.getDishEatingId());
        udeHibernateDao.delete(secondDish.getDishEatingId());
        hibernateDao.deleteMealtimeEating(userEating.getEatingId());

    }

    @Test
    public void editEating1() throws Exception {
        final UserEatingHibernateDao hibernateDao = new UserEatingHibernateDao();
        final UserDishEatingHibernateDao udeHibernateDao = new UserDishEatingHibernateDao();

        UserEating.Builder b = new UserEating.Builder();

        String mealtime = "Завтрак";
        Date date = ParseUtils.parseDate("2016-10-20 08:00:00");

        b.setProfileId(3).setMealtime(mealtime).setDateEating(date);
        UserEating userEating = b.build();

        userEating = hibernateDao.createMealtime(userEating);

        UserDishEating firstDish = udeHibernateDao.add(new UserDishEating(2, 630, new UserEating(userEating.getEatingId())));
        UserDishEating secondDish = udeHibernateDao.add(new UserDishEating(3, 230, new UserEating(userEating.getEatingId())));

        List<UserDishEating> dishEatings = new ArrayList<>();
        dishEatings.add(firstDish);
        dishEatings.add(secondDish);
        userEating.setUserDishEatings(dishEatings);

        firstDish.setAmountDish(929);
        secondDish.setAmountDish(329);
        udeHibernateDao.update(firstDish);
        udeHibernateDao.update(secondDish);

        udeHibernateDao.delete(firstDish.getDishEatingId());
        udeHibernateDao.delete(secondDish.getDishEatingId());
        hibernateDao.deleteMealtimeEating(userEating.getEatingId());
    }

}