package com.alexmanwell.servlet.dish;

import com.alexmanwell.calorium.ConnectionFactory;
import com.alexmanwell.calorium.dish.Dish;
import com.alexmanwell.calorium.dish.DishDao;
import com.alexmanwell.calorium.dish.DishJdbcDao;
import jdk.nashorn.internal.ir.annotations.Ignore;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CreateDishServletTest extends Mockito {

    @Test @Ignore
    public void insertDish() throws Exception {
        ServletConfig config = mock(ServletConfig.class);
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        RequestDispatcher dispatcher = mock(RequestDispatcher.class);

        when(request.getParameter("dishName")).thenReturn("testName");
        when(request.getParameterValues("products.key")).thenReturn(new String[]{"testProduct1", "testProduct2"});
        when(request.getParameterValues("products.value")).thenReturn(new String[]{"1", "2"});
        when(request.getRequestDispatcher("/WEB-INF/printDish.jsp")).thenReturn(dispatcher);

        DishDao dishDao = new DishJdbcDao(ConnectionFactory.getConnection());
        Collection<Dish> dishesBeforeCreate = dishDao.findAllDish();

        CreateDishServlet createDishServlet = new CreateDishServlet();
/*
        TODO: Есть проблема в том, что в методе #CreateDishServlet.init(config) переменная context = null из-за того что getServletContext равно null;
        Потому вместо метода
        createDishServlet.init(config);
        используется dishDao, который инициализируется в строке 45.
*/
        createDishServlet.dishDao = new DishJdbcDao(ConnectionFactory.getConnection());
        createDishServlet.doPost(request, response);

        verify(request, atLeast(1)).getParameter("dishName");
        verify(request, atLeast(1)).getParameterValues("products.key");
        verify(request, atLeast(1)).getParameterValues("products.value");

        Collection<Dish> dishesAfterCreate = dishDao.findAllDish();


        assertEquals(dishesAfterCreate.size(), dishesBeforeCreate.size() + 1);
        Dish dish = dishDao.searchDish("testName");
        for (Dish d : dishesAfterCreate) {
            if (d.equals(dish)) {
                assertTrue(dishesAfterCreate.contains(dish));
                break;
            }
        }
        dishDao.deleteDish(dish.getId());
    }
}