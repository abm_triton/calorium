package com.alexmanwell.servlet.profile;

import com.alexmanwell.calorium.profile.CryptPassword;
import com.alexmanwell.calorium.profile.Profile;
import com.alexmanwell.calorium.profile.ProfileDao;
import com.alexmanwell.servlet.MailSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Objects;

public class CreateProfileServlet extends HttpServlet {

    final static Logger logger = LoggerFactory.getLogger(CreateProfileServlet.class);

    private ProfileDao profileDao = null;

    public void init() throws ServletException {
        try {
            ServletContext context = getServletConfig().getServletContext();
            profileDao = (ProfileDao) context.getAttribute("profileDao");
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
    }

    public void destroy() {
        profileDao = null;
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String nickname = request.getParameter("nickname");
        String password = CryptPassword.cryptPassword(request.getParameter("password"));
        String confirmPassword = CryptPassword.cryptPassword(request.getParameter("confirmPassword"));
        String mail = request.getParameter("mail");
        logger.debug("Nickname: {}", nickname);
        try {
            Profile profile = profileDao.searchProfile(nickname);
            if (!Objects.equals(password, confirmPassword)) {
                request.getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
                return;
            }
            if (profile != null) {
                try {
                    request.setAttribute("profile", profile);
                    request.getRequestDispatcher("/WEB-INF/existProfile.jsp").forward(request, response);
                } catch (Exception e) {
                    logger.warn("Error request dispatcher: {}", request, e);
                }
            }

            profile = new Profile(nickname, mail);
            profileDao.createProfile(profile, password);

            MailSender.send(mail, "This is the Subject Line!", "This is actual message");

            HttpSession httpSession = request.getSession(true);
            httpSession.setAttribute("profile", profile);
            request.getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);

        } catch (Exception e) {
            logger.warn("Failed to registration user: {}", nickname, e);
        }
    }
}
