package com.alexmanwell.servlet.profile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LogoutProfileServlet extends HttpServlet {
    private final static Logger logger = LoggerFactory.getLogger(LogoutProfileServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("begin logout profile");
        HttpSession session = request.getSession(false);
        session.removeAttribute("profile");
        session.invalidate();
        request.getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);

        logger.info("end logout profile");
    }
}
