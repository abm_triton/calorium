package com.alexmanwell.servlet.profile;

import com.alexmanwell.calorium.profile.CryptPassword;
import com.alexmanwell.calorium.profile.Profile;
import com.alexmanwell.calorium.profile.ProfileDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;

public class LoginProfileServlet extends HttpServlet {

    private final static Logger logger = LoggerFactory.getLogger(LoginProfileServlet.class);

    private ProfileDao profileDao = null;

    public void init() throws ServletException {
        try {
            ServletContext context = getServletConfig().getServletContext();
            profileDao = (ProfileDao) context.getAttribute("profileDao");
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("begin login profile post query servlet");
        String nickname = request.getParameter("nickname");
        String password = CryptPassword.cryptPassword(request.getParameter("password"));
        logger.debug("Nickname: {}", nickname);
        try {
            Profile profile = profileDao.loginProfile(nickname, password);
            if ( profile != null) {
                HttpSession session = request.getSession(true);
                session.setAttribute("profile", profile);

            } else {
                request.getRequestDispatcher("/WEB-INF/errorPage404.jsp").forward(request, response);
            }
            request.getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
        } catch (Exception e) {
            logger.warn("Failed to login user: {}", nickname, e);
        }
        logger.info("end login profile post query servlet");
    }

}
