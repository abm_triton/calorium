package com.alexmanwell.servlet;

import com.alexmanwell.calorium.ConnectionFactory;
import com.alexmanwell.calorium.dish.DishDao;
import com.alexmanwell.calorium.dish.DishJdbcDao;
import com.alexmanwell.calorium.disheating.UserDishEatingDao;
import com.alexmanwell.calorium.disheating.UserDishEatingJdbcDao;
import com.alexmanwell.calorium.eating.UserEatingDao;
import com.alexmanwell.calorium.eating.UserEatingJdbcDao;
import com.alexmanwell.calorium.product.ProductDao;
import com.alexmanwell.calorium.product.ProductJdbcDao;
import com.alexmanwell.calorium.profile.ProfileDao;
import com.alexmanwell.calorium.profile.ProfileJdbcDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import java.sql.SQLException;

public class ContextListener implements ServletContextListener {
    final static Logger logger = LoggerFactory.getLogger(ContextListener.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ConnectionFactory connection = new ConnectionFactory();
        try {
            ProductDao productDao = new ProductJdbcDao(ConnectionFactory.getConnection());
            DishDao dishDao = new DishJdbcDao(ConnectionFactory.getConnection());
            ProfileDao profileDao = new ProfileJdbcDao(ConnectionFactory.getConnection());
            UserEatingDao userEatingDao = new UserEatingJdbcDao(ConnectionFactory.getConnection());
            UserDishEatingDao userDishEatingDao = new UserDishEatingJdbcDao(ConnectionFactory.getConnection());

            ServletContext context = sce.getServletContext();
            context.setAttribute("productDao", productDao);
            context.setAttribute("dishDao", dishDao);
            context.setAttribute("profileDao", profileDao);
            context.setAttribute("userEatingDao", userEatingDao);
            context.setAttribute("userDishEatingDao", userDishEatingDao);

        } catch (SQLException e) {
            logger.warn("Error connection in DB:", e);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
