package com.alexmanwell.servlet.eating;

import com.alexmanwell.calorium.dish.Dish;
import com.alexmanwell.calorium.dish.DishDao;
import com.alexmanwell.calorium.eating.ParseUtils;
import com.alexmanwell.calorium.eating.UserEating;
import com.alexmanwell.calorium.eating.UserEatingDao;
import com.alexmanwell.calorium.product.ProductDao;
import com.alexmanwell.calorium.profile.Profile;
import com.alexmanwell.calorium.profile.ProfileDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CreateMealtimeUserEatingServlet extends HttpServlet {
    private final static Logger logger = LoggerFactory.getLogger(CreateMealtimeUserEatingServlet.class);

    private ProfileDao profileDao = null;
    private DishDao dishDao = null;
    private UserEatingDao userEatingDao = null;
    private ProductDao productDao = null;

    public void init() throws ServletException {
        try {
            ServletContext context = getServletConfig().getServletContext();
            profileDao = (ProfileDao) context.getAttribute("profileDao");
            dishDao = (DishDao) context.getAttribute("dishDao");
            userEatingDao = (UserEatingDao) context.getAttribute("userEatingDao");
            productDao = (ProductDao) context.getAttribute("productDao");
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("begin reidrect page");

        Map<String, Float> dishes = new HashMap<>();
        request.setAttribute("dishes", dishes);

        String nickname = request.getParameter("nickname");
        try {
            Profile profile = profileDao.searchProfile(nickname);
            request.setAttribute("profile", profile);
            request.getRequestDispatcher("/WEB-INF/createMealtime.jsp").forward(request, response);
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }

        logger.debug("end reidrect page");
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("begin create mealtime");
        request.setCharacterEncoding("UTF-8");

        String nickname = request.getParameter("nickname");
        String date = request.getParameter("date");
        String time = request.getParameter("time");
        String mealtime = request.getParameter("mealtime");

        String[] dishName = request.getParameterValues("dishes.key");
        String[] dishAmount = request.getParameterValues("dishes.value");
        UserEating userEating = null;
        try {
            Profile profile = profileDao.searchProfile(nickname);
            UserEating.Builder b = new UserEating.Builder();
            b.setProfileId(profile.getId());
            Date parseDate = ParseUtils.parseDate(date + " " + time + ":00");
            b.setDateEating(parseDate);
            b.setMealtime(mealtime);

            for (int i = 0; i < dishName.length; i++) {
                Dish dish = dishDao.searchDish(dishName[i]);
                b.addDish(dish, Float.parseFloat(dishAmount[i]));
            }

            userEating = b.build();
            UserEating currentMealtimeDay = userEatingDao.selectMealtime(userEating);
            if (currentMealtimeDay == null) {
                userEating = userEatingDao.createMealtime(userEating);
            } else {
                b.setEatingId(currentMealtimeDay.getEatingId());
                userEating = b.build();
            }

            userEatingDao.addEating(userEating);

            userEating = userEatingDao.selectMealtime(userEating.getEatingId());
            request.setAttribute("userEating", userEating);
            request.getRequestDispatcher("/WEB-INF/printMealtime.jsp").forward(request, response);
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
        logger.info("end create mealtime {}.", userEating);
    }
}
