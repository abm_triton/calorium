package com.alexmanwell.servlet.eating;

import com.alexmanwell.calorium.dish.DishDao;
import com.alexmanwell.calorium.eating.UserEating;
import com.alexmanwell.calorium.eating.UserEatingDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AddDishMealtimeUserEatingServlet extends HttpServlet {
    private final static Logger logger = LoggerFactory.getLogger(AddDishMealtimeUserEatingServlet.class);

    private DishDao dishDao = null;
    private UserEatingDao userEatingDao = null;

    public void init() throws ServletException {
        try {
            ServletContext context = getServletConfig().getServletContext();
            dishDao = (DishDao) context.getAttribute("dishDao");
            userEatingDao = (UserEatingDao) context.getAttribute("userEatingDao");
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
    }

    public void destroy() {
        dishDao = null;
        userEatingDao = null;
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("begin add dish mealtime");
        request.setCharacterEncoding("UTF-8");

        long eatingId = Long.parseLong(request.getParameter("eatingId"));
        String dishName = request.getParameter("dishName");
        float dishAmount = Float.parseFloat(request.getParameter("amountDish"));
        try {
            UserEating.Builder b = new UserEating.Builder();
            b.setEatingId(eatingId);
            b.addDish(dishDao.searchDish(dishName), dishAmount);
            UserEating userEating = b.build();
            logger.info("userEating: {}", userEating);
            logger.info("userEating.dish: {}", userEating.getUserDish());
            userEatingDao.addEating(userEating);
            userEating = userEatingDao.selectMealtime(userEating.getEatingId());
            logger.info("userEating: {}", userEating);
            logger.info("userEating.dish: {}", userEating.getUserDish());

            request.setAttribute("userEating", userEating);
            logger.info("end add dish mealtime {}.", userEating);

            request.getRequestDispatcher("/WEB-INF/printMealtime.jsp").forward(request, response);

        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
