package com.alexmanwell.servlet.eating;

import com.alexmanwell.calorium.dish.Dish;
import com.alexmanwell.calorium.dish.DishDao;
import com.alexmanwell.calorium.eating.UserEating;
import com.alexmanwell.calorium.eating.UserEatingDao;
import com.alexmanwell.calorium.product.ProductDao;
import com.alexmanwell.calorium.profile.ProfileDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class EditMealtimeUserEatingServlet extends HttpServlet {

    private final static Logger logger = LoggerFactory.getLogger(EditMealtimeUserEatingServlet.class);

    private ProfileDao profileDao = null;
    private DishDao dishDao = null;
    private UserEatingDao userEatingDao = null;
    private ProductDao productDao = null;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("begin reidrect page im editMealtime.jsp");

        long eatingId = Long.parseLong(req.getParameter("eatingId"));
        try {
            UserEating ue = userEatingDao.selectMealtime(eatingId);
            req.setAttribute("userEating", ue);
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }

        req.getRequestDispatcher("/WEB-INF/editMealtime.jsp").forward(req, resp);

        logger.debug("begin reidrect page im editMealtime.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.debug("begin edit mealtime");
        req.setCharacterEncoding("UTF-8");

        HttpSession session = req.getSession();
        UserEating ue = (UserEating) session.getAttribute("userEating");
        Map<Dish, Float> dishes = new HashMap<>();
        String[] dishName = req.getParameterValues("dish.key");
        String[] dishAmount = req.getParameterValues("dish.value");

        UserEating.Builder builder = new UserEating.Builder();
        builder.setProfileId(ue.getProfileId()).setEatingId(ue.getEatingId()).setDateEating(ue.getDate()).setMealtime(ue.getMealtime());
        for (int i = 0; i < dishName.length; i++) {
            try {
                Dish dish = dishDao.searchDish(dishName[i]);
                dishes.put(dish, Float.parseFloat(dishAmount[i]));
                builder.addDish(dish, Float.parseFloat(dishAmount[i]));
            } catch (Exception e) {
                logger.warn("Error connection in DB:", e);
            }
        }
        try {
            userEatingDao.editEating(ue.getEatingId(), dishes);

            ue = builder.build();
            req.setAttribute("userEating", ue);
            req.getRequestDispatcher("/WEB-INF/printMealtime.jsp").forward(req, resp);

        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
        logger.debug("end edit mealtime");
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        try {
            ServletContext context = config.getServletContext();
            profileDao = (ProfileDao) context.getAttribute("profileDao");
            dishDao = (DishDao) context.getAttribute("dishDao");
            userEatingDao = (UserEatingDao) context.getAttribute("userEatingDao");
            productDao = (ProductDao) context.getAttribute("productDao");
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
    }

    @Override
    public void destroy() {
        super.destroy();
        profileDao = null;
        dishDao = null;
        userEatingDao = null;
        productDao = null;
    }


}
