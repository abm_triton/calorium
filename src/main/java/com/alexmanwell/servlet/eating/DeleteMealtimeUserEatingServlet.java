package com.alexmanwell.servlet.eating;

import com.alexmanwell.calorium.CalculateNutients;
import com.alexmanwell.calorium.dish.Dish;
import com.alexmanwell.calorium.disheating.UserDishEating;
import com.alexmanwell.calorium.eating.UserEating;
import com.alexmanwell.calorium.eating.UserEatingDao;
import com.alexmanwell.calorium.product.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

public class DeleteMealtimeUserEatingServlet extends HttpServlet {
    private final static Logger logger = LoggerFactory.getLogger(DeleteMealtimeUserEatingServlet.class);

    private UserEatingDao userEatingDao = null;

    public void init() throws ServletException {
        try {
            ServletContext context = getServletConfig().getServletContext();
            userEatingDao = (UserEatingDao) context.getAttribute("userEatingDao");
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
    }

    public void destroy() {
        userEatingDao = null;
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("begin delete mealtime");

        long eatingId = Long.parseLong(request.getParameter("eatingId"));
        long profileId = Long.parseLong(request.getParameter("profileId"));
        try {
            userEatingDao.deleteMealtimeEating(eatingId);

            List<UserEating> userEatings = userEatingDao.searchUserEatingToday(profileId, new Date(System.currentTimeMillis()));
            request.setAttribute("userEatings", userEatings);


            Map<Long, Map<Dish, Product>> resultNutrientsMealtimeDish = new HashMap<>();
            Map<Long, Product> resultNutrientsMealtimeDishes = new HashMap<>();

            CalculateNutients calculate = new CalculateNutients();
            Product resultNutrientsMealtimesToday = calculate.getAllNutrientsUserEatings(userEatings);
            request.setAttribute("resultNutrientsToday", resultNutrientsMealtimesToday);

            for (UserEating ue : userEatings) {
                List<Product> products = new ArrayList<>();
                Map<Dish, Product> resultNutrientsDish = new HashMap<>();
                for (UserDishEating ude : ue.getUserDishEatings()) {
                    Product product = calculate.getNutrientsDish(ude);
                    products.add(product);
                    resultNutrientsDish.put(ude.getDish(), product);
                }
                resultNutrientsMealtimeDish.put(ue.getEatingId(), resultNutrientsDish);
                Product resultNutrientsAllDishes = calculate.resultNutrientsAllDishes(products);
                resultNutrientsMealtimeDishes.put(ue.getEatingId(), resultNutrientsAllDishes);
            }
            request.setAttribute("resultNutrientsDish", resultNutrientsMealtimeDish);
            request.setAttribute("resultNutrientsMealtimeDishes", resultNutrientsMealtimeDishes);

            request.setAttribute("currentDay", PrintMealtimeUserEatingServlet.getCurrentDay());
            request.setAttribute("userEatings", userEatings);
            request.getRequestDispatcher("/WEB-INF/printMealtimeToday.jsp").forward(request, response);

        }  catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
        logger.info("end delete mealtime in eatingId: {}", eatingId);
    }
}
