package com.alexmanwell.servlet.eating;

import com.alexmanwell.calorium.eating.UserEating;
import com.alexmanwell.calorium.eating.UserEatingDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RemoveMealtimeDishUserEatingServlet extends HttpServlet {

    private final static Logger logger = LoggerFactory.getLogger(RemoveMealtimeDishUserEatingServlet.class);
    private UserEatingDao userEatingDao = null;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("begin remove dish in mealtime");

        long dishEatingId = Long.parseLong(req.getParameter("dishEatingId"));
        long eatingId = Long.parseLong(req.getParameter("eatingId"));
        try {
            userEatingDao.removeDishEating(dishEatingId);

            UserEating userEating = userEatingDao.selectMealtime(eatingId);
            req.setAttribute("userEating", userEating);

            req.getRequestDispatcher("/WEB-INF/printMealtime.jsp").forward(req, resp);

        }  catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
        logger.info("end delete mealtime in eatingId: {}", eatingId);
    }

    @Override
    public void destroy() {
        super.destroy();
        userEatingDao = null;
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            ServletContext context = getServletConfig().getServletContext();
            userEatingDao = (UserEatingDao) context.getAttribute("userEatingDao");
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
    }
}
