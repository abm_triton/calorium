package com.alexmanwell.servlet.eating;

import com.alexmanwell.calorium.dish.Dish;
import com.alexmanwell.calorium.dish.DishDao;
import com.alexmanwell.calorium.disheating.UserDishEating;
import com.alexmanwell.calorium.disheating.UserDishEatingDao;
import com.alexmanwell.calorium.eating.UserEating;
import com.alexmanwell.calorium.eating.UserEatingDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class EditDishMealtimeUserEatingServlet extends HttpServlet{
    private final static Logger logger = LoggerFactory.getLogger(EditDishMealtimeUserEatingServlet.class);

    private DishDao dishDao = null;
    private UserDishEatingDao userDishEatingDao = null;
    private UserEatingDao userEatingDao = null;

    public void init() throws ServletException {
        try {
            ServletContext context = getServletConfig().getServletContext();
            dishDao = (DishDao) context.getAttribute("dishDao");
            userDishEatingDao = (UserDishEatingDao) context.getAttribute("userDishEatingDao");
            userEatingDao = (UserEatingDao) context.getAttribute("userEatingDao");
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
    }

    public void destroy() {
        dishDao = null;
        userDishEatingDao = null;
        userEatingDao = null;
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("begin edit dish mealtime");
        request.setCharacterEncoding("UTF-8");

        long eatingId = Long.parseLong(request.getParameter("eatingId"));
        long dishEatingId = Long.parseLong(request.getParameter("dishEatingId"));
        String nameDish = request.getParameter("dishName");
        float dishAmount = Float.parseFloat(request.getParameter("amountDish"));

        try {
            UserDishEating.Builder b = new UserDishEating.Builder();
            Dish dish = dishDao.searchDish(nameDish);
            b.setDish(dish).setDishId(dish.getId()).setDishEatingId(dishEatingId).setAmountDish(dishAmount).setEatingId(eatingId);
            UserDishEating ude = b.build();
            userDishEatingDao.update(ude);

            UserEating userEating = userEatingDao.selectMealtime(eatingId);
            request.setAttribute("userEating", userEating);
            logger.info("end edit dish mealtime {}.", userEating);

            request.getRequestDispatcher("/WEB-INF/printMealtime.jsp").forward(request, response);

        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}