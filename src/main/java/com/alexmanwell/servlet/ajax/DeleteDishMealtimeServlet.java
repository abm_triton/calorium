package com.alexmanwell.servlet.ajax;

import com.alexmanwell.calorium.eating.UserEatingDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DeleteDishMealtimeServlet extends HttpServlet {

    final static Logger logger = LoggerFactory.getLogger(DeleteDishMealtimeServlet.class);
    private UserEatingDao userEatingDao = null;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("begin delete concrete dish in mealtime ajax query");
        long dishEatingId = Long.parseLong(req.getParameter("dishEatingId").trim());
        try {
            userEatingDao.removeDishEating(dishEatingId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("end delete concrete dish in mealtime ajax query");
    }

    @Override
    public void destroy() {
        super.destroy();
        userEatingDao = null;
    }

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            ServletContext context = getServletConfig().getServletContext();
            userEatingDao = (UserEatingDao) context.getAttribute("userEatingDao");
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
    }
}
