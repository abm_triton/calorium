package com.alexmanwell.servlet.ajax;

import com.alexmanwell.calorium.disheating.UserDishEating;
import com.alexmanwell.calorium.disheating.UserDishEatingDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UpdateDishMealtimeServlet extends HttpServlet {

    final static Logger logger = LoggerFactory.getLogger(UpdateDishMealtimeServlet.class);
    private UserDishEatingDao userDishEatingDao = null;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("begin update dish in mealtime ajax query");
        long dishEatingId = Long.parseLong(req.getParameter("dishEatingId").trim());
        String dishName = req.getParameter("dishName").trim();
        float amountDish = Float.parseFloat(req.getParameter("amountDish").trim());
        long eatingId = Long.parseLong(req.getParameter("eatingId").trim());
        long dishId = Long.parseLong(req.getParameter("dishId").trim());
        UserDishEating ude = new UserDishEating(dishEatingId, eatingId, dishId, dishName, amountDish);
        try {
            userDishEatingDao.update(ude);
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("end update dish in mealtime ajax query");
    }

    @Override
    public void destroy() {
        super.destroy();
        userDishEatingDao = null;

    }

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            ServletContext context = getServletConfig().getServletContext();
            userDishEatingDao = (UserDishEatingDao) context.getAttribute("userDishEatingDao");
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
    }
}
