package com.alexmanwell.servlet.ajax;

import com.alexmanwell.calorium.disheating.UserDishEating;
import com.alexmanwell.calorium.disheating.UserDishEatingDao;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SelectConcreteDishMealtimeServlet extends HttpServlet {

    final static Logger logger = LoggerFactory.getLogger(SelectConcreteDishMealtimeServlet.class);
    private UserDishEatingDao userDishEatingDao = null;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("begin select concrete dish in mealtime ajax query");
        long dishEatingId = Long.parseLong(req.getParameter("dishEatingId").trim());
        try {
            UserDishEating userDishEating = userDishEatingDao.select(dishEatingId);

            resp.setCharacterEncoding("UTF-8");
            resp.setContentType("text/plain");

            Gson gson = new Gson();
            String ude = gson.toJson(userDishEating);
            logger.info("userDishEating json: {}", gson.toJson(ude));
            resp.getWriter().write(ude);
        } catch (Exception e) {
            e.printStackTrace();
        }

        logger.info("end select concrete dish in mealtime ajax query");
    }

    @Override
    public void destroy() {
        super.destroy();
        userDishEatingDao = null;
    }

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            ServletContext context = getServletConfig().getServletContext();
            userDishEatingDao = (UserDishEatingDao) context.getAttribute("userDishEatingDao");
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
    }
}
