package com.alexmanwell.servlet.ajax;

import com.alexmanwell.calorium.eating.UserEating;
import com.alexmanwell.calorium.eating.UserEatingDao;
import com.alexmanwell.calorium.profile.Profile;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

public class SelectMealtimeServlet extends HttpServlet {

    final static Logger logger = LoggerFactory.getLogger(SelectMealtimeServlet.class);
    private UserEatingDao userEatingDao = null;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("begin select mealtime ajax query");

        Profile profile = (Profile) req.getSession(true).getAttribute("profile");
        try {
            List<UserEating> userEatings = userEatingDao.searchUserEatingToday(profile.getId(), new Date(System.currentTimeMillis()));

            resp.setCharacterEncoding("UTF-8");
            resp.setContentType("text/plain");

            Gson gson = new Gson();
            String ue = gson.toJson(userEatings);
            logger.info("json: {}", gson.toJson(ue));
            resp.getWriter().write(ue);
        } catch (Exception e) {
            e.printStackTrace();
        }

        logger.info("end select mealtime ajax query");
    }

    @Override
    public void destroy() {
        userEatingDao = null;
    }

    @Override
    public void init() throws ServletException {
        try {
            ServletContext context = getServletConfig().getServletContext();
            userEatingDao = (UserEatingDao) context.getAttribute("userEatingDao");
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
    }
}
