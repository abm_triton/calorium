package com.alexmanwell.servlet.ajax;

import com.alexmanwell.calorium.dish.DishDao;
import com.alexmanwell.calorium.product.ProductDao;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class SearchServlet extends HttpServlet {

    final static Logger logger = LoggerFactory.getLogger(SearchServlet.class);

    private ProductDao productDao = null;
    private DishDao dishDao = null;

    public void init() throws ServletException {
        try {
            ServletContext context = getServletConfig().getServletContext();
            productDao = (ProductDao) context.getAttribute("productDao");
            dishDao = (DishDao) context.getAttribute("dishDao");
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
    }

    public void destroy() {

        productDao = null;
        dishDao = null;
    }
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("begin ajax query");
        String userName = request.getParameter("userName").trim();
        logger.info("userName: {}", userName);
        try {
            List<String> names = productDao.searchProduct(userName);
            logger.info("names: {}", names);

            response.setCharacterEncoding("UTF-8");
            response.setContentType("text/plain");

            Gson gson = new Gson();
            logger.info("json: {}", gson.toJson(names));
            response.getWriter().write(gson.toJson(names));
        } catch (Exception e) {
            e.printStackTrace();
        }

        logger.info("end ajax query");
    }
}