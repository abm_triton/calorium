package com.alexmanwell.servlet.ajax;

import com.alexmanwell.calorium.dish.Dish;
import com.alexmanwell.calorium.dish.DishDao;
import com.alexmanwell.calorium.eating.UserEating;
import com.alexmanwell.calorium.eating.UserEatingDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by qw on 05.11.2016.
 */
public class AddDishMealtimeServlet extends HttpServlet {

    final static Logger logger = LoggerFactory.getLogger(AddDishMealtimeServlet.class);

    private UserEatingDao userEatingDao = null;
    private DishDao dishDao = null;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("begin add dish mealtime ajax query");
        long eatingId = Long.parseLong(req.getParameter("eatingId"));
        String dishName = req.getParameter("dishName");
        long amountDish = Long.parseLong(req.getParameter("amountDish"));

        try {
            Dish dish = dishDao.searchDish(dishName);
            Map<Dish, Long> mealtimeDishes = new HashMap<>(1);
            mealtimeDishes.put(dish, amountDish);
            UserEating.Builder b = new UserEating.Builder();
            b.setEatingId(eatingId);
            b.addDish(dish, amountDish);
            UserEating userEating = b.build();

            userEatingDao.addEating(userEating);

        } catch (Exception e) {
            e.printStackTrace();
        }

        logger.info("end add dish mealtime ajax query");
    }

    @Override
    public void destroy() {
        super.destroy();
        userEatingDao = null;
        dishDao = null;
    }

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            ServletContext context = getServletConfig().getServletContext();
            userEatingDao = (UserEatingDao) context.getAttribute("userEatingDao");
            dishDao = (DishDao) context.getAttribute("dishDao");
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
    }
}
