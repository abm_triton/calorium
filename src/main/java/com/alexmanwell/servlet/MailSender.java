package com.alexmanwell.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class MailSender {
    final static Logger logger = LoggerFactory.getLogger(MailSender.class);

    public static void send(String mail, String sub, String messg) {
        logger.info("begin send message in mailbox");
        final String user = "alexandr.manvelyan@yandex.ru";
        final String pass = "wAr1crAft2drUms1120";

        Properties props = new Properties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.host", "smtp.yandex.ru");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.host", "yandex");
        props.put("mail.user", "alexandr.manvelyan");
        props.put("mail.password", "wAr1crAft2drUms1120");
        props.put("mail.port", "465");

        try {
            Session session = Session.getDefaultInstance(props, new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(user, pass);
                }
            });

            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(user));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(mail));
            message.setSubject(sub);
            message.setText(messg);

            Transport.send(message);

        } catch (MessagingException e) {
            logger.warn("Error send message in mailbox", e);
        }
        logger.info("end send message in mailbox");
    }
}