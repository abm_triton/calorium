package com.alexmanwell.servlet.product;

import com.alexmanwell.calorium.product.Product;
import com.alexmanwell.calorium.product.ProductDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class PrintProductServlet extends HttpServlet {

    final static Logger logger = LoggerFactory.getLogger(PrintProductServlet.class);

    private ProductDao productDao = null;

    public void init() throws ServletException {
        try {
            ServletContext context = getServletConfig().getServletContext();
            productDao = (ProductDao) context.getAttribute("productDao");
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
    }

    public void destroy() {
        productDao = null;
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String productName = request.getParameter("productName");
        try {
            Product product = productDao.selectProduct(productName);

            request.setAttribute("productName", productName);

            if ( product != null) {
                request.setAttribute("product", product);
            }
            request.getRequestDispatcher("/WEB-INF/printProduct.jsp").forward(request, response);
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
    }
}
