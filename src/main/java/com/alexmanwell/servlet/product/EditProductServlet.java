package com.alexmanwell.servlet.product;

import com.alexmanwell.calorium.product.ElementType;
import com.alexmanwell.calorium.product.Product;
import com.alexmanwell.calorium.product.ProductDao;
import com.alexmanwell.calorium.product.VitaminType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class EditProductServlet extends HttpServlet {

    final static Logger logger = LoggerFactory.getLogger(EditProductServlet.class);

    private ProductDao productDao = null;

    public void init() throws ServletException {
        try {
            ServletContext context = getServletConfig().getServletContext();
            productDao = (ProductDao) context.getAttribute("productDao");
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
    }

    public void destroy() {
        productDao = null;
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("begin reidrect page");
        String productName = request.getParameter("productName");
        try {
            Product product = productDao.selectProduct(productName);

            request.setAttribute("product", product);
            request.getRequestDispatcher("/WEB-INF/editProduct.jsp").forward(request, response);

        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
        logger.debug("end reidrect page");
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("begin edit product");

        Product.Builder pb = new Product.Builder();

        pb.setName(request.getParameter("productName"));;
        pb.setKcal(Integer.parseInt(request.getParameter("productKcal")));
        pb.setProtein(Float.parseFloat(request.getParameter("productProtein")));
        pb.setCarbohydrate(Float.parseFloat(request.getParameter("productCarbohydrate")));
        pb.setAFat(Float.parseFloat(request.getParameter("productAFat")));
        pb.setAFat(Float.parseFloat(request.getParameter("productVFat")));

        for (VitaminType vt : VitaminType.values()) {
            float vv = Float.parseFloat(request.getParameter(vt.name()));
            pb.addVitamin(vt, vv);
        }

        for (ElementType et : ElementType.values()) {
            float ev = Float.parseFloat(request.getParameter(et.name().trim()));
            pb.addElement(et, ev);
        }

        Product product = pb.build();
        try {
            productDao.editProduct(product);

            request.setAttribute("product", product);
            request.getRequestDispatcher("/WEB-INF/printProduct.jsp").forward(request, response);
        }  catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
        logger.debug("end edit product {}.", product);
    }
}
