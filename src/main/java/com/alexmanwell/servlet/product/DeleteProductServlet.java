package com.alexmanwell.servlet.product;

import com.alexmanwell.calorium.product.Product;
import com.alexmanwell.calorium.product.ProductDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

public class DeleteProductServlet extends HttpServlet {

    final static Logger logger = LoggerFactory.getLogger(DeleteProductServlet.class);

    private ProductDao productDao = null;

    public void init() throws ServletException {
        try {
            ServletContext context = getServletConfig().getServletContext();
            productDao = (ProductDao) context.getAttribute("productDao");
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
    }

    public void destroy() {
        productDao = null;
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String productName = request.getParameter("productName");
        try {
            logger.info("begin delete product {}.", productName);
            productDao.deleteProduct(productName);
            logger.info("end delete product {}.", productName);

            logger.info("begin send products in printProducts.jsp");
            Collection<Product> products = productDao.findAllProducts();
            request.setAttribute("products", products);
            request.getRequestDispatcher("/WEB-INF/printProducts.jsp").forward(request, response);
            logger.info("end send products in printProducts.jsp {}.", products);

        }  catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
    }
}
