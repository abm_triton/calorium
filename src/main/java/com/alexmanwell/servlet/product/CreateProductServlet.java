package com.alexmanwell.servlet.product;

import com.alexmanwell.calorium.product.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.util.Objects;

@MultipartConfig
public class CreateProductServlet extends HttpServlet {

    final static Logger logger = LoggerFactory.getLogger(CreateProductServlet.class);

    private ProductDao productDao = null;

    public void init() throws ServletException {
        try {
            ServletContext context = getServletConfig().getServletContext();
            productDao = (ProductDao) context.getAttribute("productDao");
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
    }

    public void destroy() {
        productDao = null;
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("begin reidrect page");
        request.setAttribute("vitaminType", VitaminType.values());
        request.setAttribute("elementType", ElementType.values());
        request.setAttribute("groupType", GroupType.values());
        request.getRequestDispatcher("/WEB-INF/insertProduct.jsp").forward(request, response);
        logger.debug("end reidrect page");
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("begin create product");
        request.setCharacterEncoding("UTF-8");
        Product.Builder pb = new Product.Builder();
        pb.setName(request.getParameter("productName"));
        pb.setGroup(request.getParameter("productGroup"));
        logger.info("group product {}.", request.getParameter("productGroup"));
        pb.setKcal(Integer.parseInt(request.getParameter("productKcal")));
        pb.setProtein(Float.parseFloat(request.getParameter("productProtein")));
        pb.setCarbohydrate(Float.parseFloat(request.getParameter("productCarbohydrate")));
        pb.setAFat(Float.parseFloat(request.getParameter("productAFat")));
        pb.setAFat(Float.parseFloat(request.getParameter("productVFat")));

        for (VitaminType vt : VitaminType.values()) {
            float vv = 0;

            if (!Objects.equals(request.getParameter(vt.name()), "")) {
                vv = Float.parseFloat(request.getParameter(vt.name()));
            }
            pb.addVitamin(vt, vv);
        }

        for (ElementType et : ElementType.values()) {
            float ev = 0;

            if (!Objects.equals(request.getParameter(et.name().trim()), "")) {
                ev = Float.parseFloat(request.getParameter(et.name().trim()));
            }
            pb.addElement(et, ev);
        }

        pb.setDescription(request.getParameter("productDescription"));

        final Part filePart = request.getPart("image");
        final String fileName = filePart.getSubmittedFileName();
        uploadImage(filePart, fileName);

        pb.setImageName(fileName);

        Product product = pb.build();
        try {
            productDao.insertProduct(product);

            request.setAttribute("product", product);
            request.getRequestDispatcher("/WEB-INF/printProduct.jsp").forward(request, response);
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
        logger.info("end create product {}.", product);
    }

    private void uploadImage(Part filePart, String fileName) throws IOException {
        final String UPLOAD_DIRECTORY = "C:\\Projects\\calorium\\src\\main\\webapp\\image";
        final String TEMP_DIRECTORY = "C:\\Projects\\calorium\\target\\calorium\\image";
        OutputStream out = null;
        OutputStream outTemp = null;
        InputStream filecontent = null;

        try {
            out = new FileOutputStream(new File(UPLOAD_DIRECTORY + File.separator
                    + fileName));
            outTemp = new FileOutputStream(new File(TEMP_DIRECTORY + File.separator
                    + fileName));
            filecontent = filePart.getInputStream();

            int read = 0;
            final byte[] bytes = new byte[1024];

            while ((read = filecontent.read(bytes)) != -1) {
                out.write(bytes, 0, read);
                outTemp.write(bytes, 0, read);
            }

        } catch (FileNotFoundException fne) {
            logger.warn("Problems during file upload. Error: {0}.",
                    new Object[]{fne.getMessage()});
        } finally {
            if (out != null) {
                out.close();
            }
            if (outTemp != null) {
                outTemp.close();
            }
            if (filecontent != null) {
                filecontent.close();
            }
        }
    }
}
