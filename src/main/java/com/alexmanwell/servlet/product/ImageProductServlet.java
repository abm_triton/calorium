package com.alexmanwell.servlet.product;

import com.alexmanwell.calorium.product.ProductDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

public class ImageProductServlet extends HttpServlet {

    final static Logger logger = LoggerFactory.getLogger(ImageProductServlet.class);

    private ProductDao productDao = null;

    public void init() throws ServletException {
        try {
            ServletContext context = getServletConfig().getServletContext();
            productDao = (ProductDao) context.getAttribute("productDao");
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
    }

    public void destroy() {
        productDao = null;
    }



    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("begin get image");
        String productName = request.getParameter("productName");
        logger.debug("productName = {}", productName);
        try {
            String path = productDao.imgProduct(productName);

            if ( path != null) {
                request.setAttribute("path", path);
            }

            ServletContext context = getServletConfig().getServletContext();
            String filename = context.getRealPath(path);
            String mime = context.getMimeType(filename);
            if (mime == null) {
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                return;
            }

            response.setContentType(mime);
            File file = new File(filename);
            response.setContentLength((int)file.length());

            FileInputStream in = new FileInputStream(file);
            OutputStream out = response.getOutputStream();

            byte[] buf = new byte[1024];
            int count = 0;
            while ((count = in.read(buf)) >= 0) {
                out.write(buf, 0, count);
            }
            out.close();
            in.close();
            logger.debug("end get image");
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
    }
}
