package com.alexmanwell.servlet.product;

import com.alexmanwell.calorium.product.Product;
import com.alexmanwell.calorium.product.ProductDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collection;


//TODO создать маленькое консольное приложение, чтоб научиться использовать логгер для того чтоб понять как-что выводитсья.
public class PrintProductsServlet extends HttpServlet {

    private ProductDao productDao = null;
    final static Logger logger = LoggerFactory.getLogger(PrintProductsServlet.class);

    public void init() throws ServletException {
        try {
            ServletContext context = getServletConfig().getServletContext();
            productDao = (ProductDao) context.getAttribute("productDao");
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
    }

    public void destroy() {
        productDao = null;
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        try {
            logger.info("begin print products");
            Collection<Product> products = productDao.findAllProducts();

            request.setAttribute("products", products);
            request.getRequestDispatcher("/WEB-INF/printProducts.jsp").forward(request, response);

            logger.info("end print products");
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
    }
}
