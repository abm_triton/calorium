package com.alexmanwell.servlet.dish;

import com.alexmanwell.calorium.CalculateNutients;
import com.alexmanwell.calorium.dish.Dish;
import com.alexmanwell.calorium.dish.DishDao;
import com.alexmanwell.calorium.product.Product;
import com.alexmanwell.calorium.product.ProductDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class PrintDishServlet extends HttpServlet {

    final static Logger logger = LoggerFactory.getLogger(PrintDishServlet.class);

    private DishDao dishDao = null;
    private ProductDao productDao = null;

    public void init() throws ServletException {
        try {
            ServletContext context = getServletConfig().getServletContext();
            dishDao = (DishDao) context.getAttribute("dishDao");
            productDao = (ProductDao) context.getAttribute("productDao");
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
    }

    public void destroy() {
        dishDao = null;
        productDao = null;
    }


    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("begin print dish page");
        String dishName = request.getParameter("dishName");
        try {
            Dish dish = dishDao.searchDish(dishName);

            Map<Product, Float> map = productDao.getProductsInDish(dish.getId());
            CalculateNutients calculate = new CalculateNutients();
            List<Product> productList = calculate.resultDefaultNutrientsDish(map);
            Product resultProduct = calculate.resultNutrientsAllDishes(productList);

            logger.info("productFloatMap = {}", map);
            logger.info("productList = {}", productList);
            logger.info("result = {}", resultProduct);

            request.setAttribute("dishName", dishName);

            if (dish != null && productList != null) {
                request.setAttribute("dish", dish);
                request.setAttribute("productList", productList);
                request.setAttribute("result", resultProduct);
            }
            request.getRequestDispatcher("/WEB-INF/printDish.jsp").forward(request, response);
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
        logger.info("end print dish page");
    }
}
