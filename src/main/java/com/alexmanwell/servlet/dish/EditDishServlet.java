package com.alexmanwell.servlet.dish;

import com.alexmanwell.calorium.dish.Dish;
import com.alexmanwell.calorium.dish.DishDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class EditDishServlet extends HttpServlet {

    final static Logger logger = LoggerFactory.getLogger(EditDishServlet.class);

    private DishDao dishDao = null;

    public void init() throws ServletException {
        try {
            ServletContext context = getServletConfig().getServletContext();
            dishDao = (DishDao) context.getAttribute("dishDao");
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
    }

    public void destroy() {
        dishDao = null;
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("begin reidrect page");
        String dishName = request.getParameter("dishName");
        try {
            Dish dish = dishDao.searchDish(dishName);

            request.setAttribute("dish", dish);
            request.getRequestDispatcher("/WEB-INF/editDish.jsp").forward(request, response);

        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
        logger.debug("end reidrect page");
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("begin edit product");
        String dishName = request.getParameter("dishName");

        Map<String, Float> products = new HashMap<>();

        String[] productName = request.getParameterValues("product.key");
        String[] productAmount = request.getParameterValues("product.value");
        for (int i = 0; i < productName.length; i++) {
            products.put(productName[i], Float.parseFloat(productAmount[i]));
        }
        logger.debug("products {}.", products);
        Dish dish = new Dish(dishName, products);
        logger.debug("dish {}.", dish);
        try {
            dishDao.editDish(dish, dish.getId());

            request.setAttribute("dish", dish);
            request.getRequestDispatcher("/WEB-INF/printDish.jsp").forward(request, response);
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
        logger.info("end edit dish {}.", dish);
    }

}
