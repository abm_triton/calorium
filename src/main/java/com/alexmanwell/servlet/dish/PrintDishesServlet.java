package com.alexmanwell.servlet.dish;

import com.alexmanwell.calorium.dish.Dish;
import com.alexmanwell.calorium.dish.DishDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

public class PrintDishesServlet extends HttpServlet {

    private final static Logger logger = LoggerFactory.getLogger(PrintDishesServlet.class);

    DishDao dishDao = null;

    public void init() throws ServletException {
        try {
            ServletContext context = getServletConfig().getServletContext();
            dishDao = (DishDao) context.getAttribute("dishDao");
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
    }

    public void destroy() {
        dishDao = null;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("begin print dishes page");

        try {
            Collection<Dish> dishes = null;
            dishes = dishDao.findAllDish();

            request.setAttribute("dishes", dishes);

            request.getRequestDispatcher("/WEB-INF/printDishes.jsp").forward(request, response);

            logger.info("end print dishes page");

        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
    }
}
