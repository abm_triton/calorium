package com.alexmanwell.servlet.dish;

import com.alexmanwell.calorium.dish.Dish;
import com.alexmanwell.calorium.dish.DishDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CreateDishServlet extends HttpServlet {

    final static Logger logger = LoggerFactory.getLogger(CreateDishServlet.class);

    DishDao dishDao = null;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        try {
            ServletContext context = config.getServletContext();
            dishDao = (DishDao) context.getAttribute("dishDao");
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
    }

    public void destroy() {
        dishDao = null;
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.debug("begin reidrect page");
        Map<String, Float> products = new HashMap<>();
        request.setAttribute("products", products);

        request.getRequestDispatcher("/WEB-INF/insertDish.jsp").forward(request, response);
        logger.debug("end reidrect page");
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("begin create dish");
        request.setCharacterEncoding("UTF-8");
        String dishName = request.getParameter("dishName");

        Map<String, Float> products = new HashMap<>();
        logger.debug("products {}.", products);

        String[] productName = request.getParameterValues("products.key");
        String[] productAmount = request.getParameterValues("products.value");
        for (int i = 0; i < productName.length; i++) {
            products.put(productName[i], Float.parseFloat(productAmount[i]));
        }
        logger.debug("this.products {}.", products);
        Dish dish = new Dish(dishName, products);
        logger.debug("dish {}.", dish);
        try {
            dishDao.insertDish(dish);

            request.setAttribute("dish", dish);
            request.getRequestDispatcher("/WEB-INF/printDish.jsp").forward(request, response);
        } catch (Exception e) {
            logger.warn("Error connection in DB:", e);
        }
        logger.info("end create dish {}.", dish);
    }
}
