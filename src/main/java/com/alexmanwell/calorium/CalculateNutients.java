package com.alexmanwell.calorium;

import com.alexmanwell.calorium.disheating.UserDishEating;
import com.alexmanwell.calorium.eating.UserEating;
import com.alexmanwell.calorium.product.ElementType;
import com.alexmanwell.calorium.product.Product;
import com.alexmanwell.calorium.product.VitaminType;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CalculateNutients {

    public CalculateNutients() {
    }

    public Product getAllNutrientsUserEatings(List<UserEating> userEatings) {
        List<Product> nutrientsAllDishes = new ArrayList<>();
        for (UserEating ue : userEatings) {
            for (UserDishEating ude : ue.getUserDishEatings()) {
                Product resultNutrientsDish = getNutrientsDish(ude);
                nutrientsAllDishes.add(resultNutrientsDish);
            }
        }
        Product product = resultNutrientsAllDishes(nutrientsAllDishes);
        return product;
    }

    public Product getNutrientsDish(UserDishEating userDishEating) {
        List<Product> resultProducts = resultDefaultNutrientsDish(userDishEating.getDish().getProductMap());
        Product product = resultNutrientsDish(resultProducts, userDishEating.getAmountDish());
        return product;
    }

    public List<Product> resultDefaultNutrientsDish(Map<Product, Float> products) {
        MathContext mc = new MathContext(3);
        List<Product> productList = new ArrayList<>();
        Search.printTitleTable();
        for (Map.Entry<Product, Float> pr : products.entrySet()) {
            float multiplier = pr.getValue() / 100;

            Product product = pr.getKey();

            Product.Builder pb = new Product.Builder();
            pb.setName(product.getName());
            pb.setKcal(Math.round(product.getKcal() * multiplier));
            pb.setProtein(Math.round(product.getProtein() * multiplier));
            pb.setCarbohydrate(Math.round(product.getCarbohydrate() * multiplier));
            pb.setAFat(Math.round(product.getAFat() * multiplier));
            pb.setVFat(Math.round(product.getVFat() * multiplier));

            for (Map.Entry<VitaminType, Float> vitamin : product.getVitamins().entrySet()) {
                VitaminType vt = vitamin.getKey();
                BigDecimal vv = new BigDecimal(vitamin.getValue() * multiplier, mc);
                pb.addVitamin(vt, Float.parseFloat(String.valueOf(vv)));
            }

            for (Map.Entry<ElementType, Float> element : product.getElements().entrySet()) {
                ElementType et = element.getKey();
                BigDecimal ev = new BigDecimal(element.getValue() * multiplier, mc);
                pb.addElement(et, Float.parseFloat(String.valueOf(ev)));
            }
            product = pb.build();

            productList.add(product);
        }
        return productList;
    }

    public Product resultNutrientsAllDishes(List<Product> products) {
        Product product = new Product();
        for (Product pr : products) {
            Product.Builder b = new Product.Builder();
            b.setKcal(pr.getKcal() + product.getKcal());
            b.setProtein(pr.getProtein() + product.getProtein());
            b.setCarbohydrate(pr.getCarbohydrate() + product.getCarbohydrate());
            b.setAFat(pr.getAFat() + product.getAFat());
            b.setVFat(pr.getVFat() + product.getVFat());

            for (Map.Entry<VitaminType, Float> vitamin : pr.getVitamins().entrySet()) {
                float vitaminValue = 0;
                if (!product.getVitamins().isEmpty()) {
                    vitaminValue = product.getVitaminValue(vitamin.getKey());
                }
                b.addVitamin(vitamin.getKey(), vitamin.getValue() + vitaminValue);
            }

            for (Map.Entry<ElementType, Float> element : pr.getElements().entrySet()) {
                float elementValue = 0;
                if (!product.getElements().isEmpty()) {
                    elementValue = product.getElementValue(element.getKey());
                }
                b.addElement(element.getKey(), element.getValue() + elementValue);
            }

            product = b.build();
        }
        return product;
    }

    public Product resultNutrientsDish(List<Product> products, float amount) {
        final float MULTIPLIER = amount / 100;
        Product product = new Product();
        for (Product pr : products) {
            Product.Builder b = new Product.Builder();
            b.setKcal((int) (pr.getKcal() * MULTIPLIER + product.getKcal()));
            b.setProtein(pr.getProtein() * MULTIPLIER + product.getProtein());
            b.setCarbohydrate(pr.getCarbohydrate() * MULTIPLIER + product.getCarbohydrate());
            b.setAFat(pr.getAFat() * MULTIPLIER + product.getAFat());
            b.setVFat(pr.getVFat() * MULTIPLIER + product.getVFat());

            for (Map.Entry<VitaminType, Float> vitamin : pr.getVitamins().entrySet()) {
                float vitaminValue = 0;
                if (!product.getVitamins().isEmpty()) {
                    vitaminValue = product.getVitaminValue(vitamin.getKey()) * MULTIPLIER;
                }
                b.addVitamin(vitamin.getKey(), vitamin.getValue() * MULTIPLIER + vitaminValue);
            }

            for (Map.Entry<ElementType, Float> element : pr.getElements().entrySet()) {
                float elementValue = 0;
                if (!product.getElements().isEmpty()) {
                    elementValue = product.getElementValue(element.getKey()) * MULTIPLIER;
                }
                b.addElement(element.getKey(), element.getValue() * MULTIPLIER + elementValue);
            }

            product = b.build();
        }
        return product;
    }
}