package com.alexmanwell.calorium.product;

import com.alexmanwell.calorium.Command;
import com.alexmanwell.calorium.CommandContext;
import com.alexmanwell.calorium.Search;

public class PrintConcreteProductCommand implements Command {
    private String productName;

    public PrintConcreteProductCommand(String productName) {
        this.productName = productName;
    }

    @Override
    public void execute(CommandContext context) throws Exception {
        Product product = context.getProductDao().selectProduct(productName);
        if (product != null) {
            Search.printTitleTable();
            Search.printProduct(product);
        } else {
            System.out.println("Продукта с таким названием нету.");
        }
    }
}
