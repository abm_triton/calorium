package com.alexmanwell.calorium.product;

import com.alexmanwell.calorium.Command;
import com.alexmanwell.calorium.CommandContext;
import com.alexmanwell.calorium.Search;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.EnumMap;
import java.util.Scanner;

public class EditProductCommand implements Command {
    final static Logger logger = LoggerFactory.getLogger(EditProductCommand.class);
    private String productName;

    public EditProductCommand(String productName) {
        this.productName = productName;
    }

    @Override
    public void execute(CommandContext context) throws Exception {
        logger.debug("begin edit product");
        ProductDao productDao = context.getProductDao();
        Product product = productDao.selectProduct(productName);
        logger.debug(" {}.", product);
        InputDataProduct input = new InputDataProduct();
        Scanner scanner = new Scanner(System.in);

        if (product == null) {
            throw new IllegalArgumentException("Такого продукта нет в базе " + productName);
        }

        int kcal = input.writeKcal(scanner);
        float protein = input.writeProtein(scanner);
        float carbohydrate = input.writeCarbohydrate(scanner);
        float aFat = input.writeAFat(scanner);
        float vFat = input.writeVFat(scanner);

        Product.Builder pb = new Product.Builder();
        pb.setName(productName).setKcal(kcal).setProtein(protein).setCarbohydrate(carbohydrate).setAFat(aFat).setVFat(vFat);

        EnumMap<VitaminType, Float> vitamins = new EnumMap<>(VitaminType.class);
        for (VitaminType vt : VitaminType.values()) {
            float vv = input.writeVitamin(scanner, vt.name());
            vitamins.put(vt, vv);

            pb.addVitamin(vt, vv);
        }

        EnumMap<ElementType, Float> elements = new EnumMap<>(ElementType.class);
        for (ElementType et : ElementType.values()) {
            float ev = input.writeElement(scanner, et.getElement());
            elements.put(et, ev);

            pb.addElement(et, ev);
        }

        String group = input.writeGroup(scanner);
        String description = input.writeDescription(scanner);


        pb.setGroup(group);
        pb.setDescription(description);

        product = pb.build();

        productDao.editProduct(product);
        Search.printTitleTable();
        Search.printProduct(product);

        logger.debug("end edit product {}.", product);
    }
}
