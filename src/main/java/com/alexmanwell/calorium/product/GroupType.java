package com.alexmanwell.calorium.product;

public enum GroupType {
    Овощь,
    Фрукт,
    Крупа,
    Мясо,
    Рыба,
    Ягода,
    Птица,
    Молочные
    }
