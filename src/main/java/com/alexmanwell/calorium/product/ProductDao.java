package com.alexmanwell.calorium.product;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface ProductDao {
    Product selectProduct(String productName) throws Exception;
    void insertProduct(Product product) throws Exception;
    void deleteProduct(String productName) throws Exception;
    Collection<Product> findAllProducts() throws Exception;
    void editProduct(Product product) throws Exception;
    Map<Product, Float> getProductsInDish(long dishId) throws Exception;
    String imgProduct(String productName) throws Exception;

    List<String> searchProduct(String productName) throws Exception;
}
