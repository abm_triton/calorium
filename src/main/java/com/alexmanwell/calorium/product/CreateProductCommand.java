package com.alexmanwell.calorium.product;

import com.alexmanwell.calorium.Command;
import com.alexmanwell.calorium.CommandContext;
import com.alexmanwell.calorium.Search;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.EnumMap;
import java.util.Map;
import java.util.Scanner;

public class CreateProductCommand implements Command {

    final static Logger logger = LoggerFactory.getLogger(CreateProductCommand.class);

    private String productName;

    public CreateProductCommand(String productName) {
        this.productName = productName;
    }

    @Override
    public void execute(CommandContext context) throws Exception {
        logger.debug("begin execute create product {}.", context);
        ProductDao productDao = context.getProductDao();
        Product product = productDao.selectProduct(productName);
        InputDataProduct input = new InputDataProduct();
        Scanner scanner = new Scanner(System.in);

        if (product != null) {
            throw new IllegalArgumentException("Такое блюдо уже есть в базе: " + productName);
        }

        int kcal = input.writeKcal(scanner);
        float protein = input.writeProtein(scanner);
        float carbohydrate = input.writeCarbohydrate(scanner);
        float aFat = input.writeAFat(scanner);
        float vFat = input.writeVFat(scanner);

        Product.Builder pb = new Product.Builder();
        pb.setName(productName).setKcal(kcal).setProtein(protein).setCarbohydrate(carbohydrate).setAFat(aFat).setVFat(vFat);
        logger.debug("successful create instance of pb {}.", pb);

        EnumMap<VitaminType, Float> vitamins = new EnumMap<>(VitaminType.class);
        for (VitaminType vt : VitaminType.values()) {
            float vv = input.writeVitamin(scanner, vt.name());
            vitamins.put(vt, vv);

            pb.addVitamin(vt, vv);
        }

        Map<ElementType, Float> elements = new EnumMap<>(ElementType.class);
        for (ElementType et : ElementType.values()) {
            float ev = input.writeElement(scanner, et.getElement());
            elements.put(et, ev);

            pb.addElement(et, ev);

        }

        String group = input.writeGroup(scanner);
        String description = input.writeDescription(scanner);

        pb.setGroup(group);
        pb.setDescription(description);

        product = pb.build();

        logger.debug("successful create instance of product.class {}.", product);
        productDao.insertProduct(product);
        Search.printTitleTable();
        Search.printProduct(product);
        logger.debug("begin execute create product");
    }
}
