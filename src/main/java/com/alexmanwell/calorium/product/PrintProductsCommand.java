package com.alexmanwell.calorium.product;

import com.alexmanwell.calorium.Command;
import com.alexmanwell.calorium.CommandContext;
import com.alexmanwell.calorium.Search;

public class PrintProductsCommand implements Command {
    @Override
    public void execute(CommandContext context) throws Exception {
        Search.printTitleTable();
        Search.printProducts(context.getProductDao().findAllProducts());
    }
}
