package com.alexmanwell.calorium.product;

import com.alexmanwell.calorium.Command;
import com.alexmanwell.calorium.CommandContext;

public class DeleteProductCommand implements Command {
    private String productName;

    public DeleteProductCommand(String productName) {
        this.productName = productName;
    }

    @Override
    public void execute(CommandContext context) throws Exception {
        ProductDao productDao = context.getProductDao();
        Product product = productDao.selectProduct(productName);
        if (product != null) {
            productDao.deleteProduct(productName);
            productDao.findAllProducts();
        } else {
            System.out.println("Продукта с таким названием нету.");
        }
    }
}
