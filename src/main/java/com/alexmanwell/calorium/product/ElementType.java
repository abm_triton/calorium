package com.alexmanwell.calorium.product;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum ElementType {
    SULFUR("Сера"),
    POTASSIUM("Калий"),
    CHLORINE("Хлор"),
    SODIUM("Натрий"),
    CALCIUM("Кальций"),
    PHOSPHORUS("Фосфор"),
    MAGNESIUM("Магний"),
    ZINC("Цинк"),
    IRON("Железо"),
    MANGANESE("Марганец"),
    COPPER("Медь"),
    IODINE("Йод"),
    SELENIUM("Селен"),
    MOLYBDENUM("Молибден"),
    COBALT("Кобальт"),
    BROMINE("Бром"),
    BORON("Бор"),
    FLUORIDE("Фтор");

    final static Logger logger = LoggerFactory.getLogger(ElementType.class);

    private String element;

    ElementType(String element) {
        this.element = element;
    }

    public String getElement() {
        return element;
    }

    public static ElementType getElementType(String element) {
        switch (element) {
            case "Сера" : return ElementType.SULFUR;
            case "Калий" : return ElementType.POTASSIUM;
            case "Хлор" : return ElementType.CHLORINE;
            case "Натрий" : return ElementType.SODIUM;
            case "Кальций" : return ElementType.CALCIUM;
            case "Фосфор" : return ElementType.PHOSPHORUS;
            case "Магний" : return ElementType.MAGNESIUM;
            case "Цинк" : return ElementType.ZINC;
            case "Железо" : return ElementType.IRON;
            case "Марганец" : return ElementType.MANGANESE;
            case "Медь" : return ElementType.COPPER;
            case "Йод" : return ElementType.IODINE;
            case "Селен" : return ElementType.SELENIUM;
            case "Молибден" : return ElementType.MOLYBDENUM;
            case "Кобальт" : return ElementType.COBALT;
            case "Бром" : return ElementType.BROMINE;
            case "Бор" : return ElementType.BORON;
            case "Фтор" : return ElementType.FLUORIDE;
            default: throw new IllegalArgumentException("Такого элемента не существует: " + element);
        }
    }
}
