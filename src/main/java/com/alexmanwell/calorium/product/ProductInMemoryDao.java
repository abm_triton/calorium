package com.alexmanwell.calorium.product;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

public class ProductInMemoryDao implements ProductDao {

    final static Logger logger = LoggerFactory.getLogger(ProductInMemoryDao.class);
    private Collection<Product> products = new LinkedHashSet<>();

    @Override
    public Product selectProduct(String productName) throws Exception {
        logger.debug("begin search product: {}.", productName);
        Product product = null;
        for (Product pr : products) {
            if ( pr.getName().contains(productName)) {
                product = pr;
                break;
            }
        }

        logger.debug("Search this {}. product is successful", product);
        return product;
    }

    @Override
    public void insertProduct(Product product) throws Exception {
        logger.debug("begin insert product: {}.", product);
        if (!products.contains(product)) {
            products.add(product);
            logger.debug("Insert product is successful: {}.", product);
        }
    }

    @Override
    public void deleteProduct(String productName) throws Exception {
        logger.debug("begin delete product: {}.", productName);
        for (Product product : products) {
            if (product.getName().contains(productName)) {
                products.remove(product);
                logger.debug("product is successful remove {}.", product);
                break;
            }
        }
    }

    @Override
    public Collection<Product> findAllProducts() throws Exception {
        logger.debug("begin find all products: {}.", products);
        return products;
    }

    @Override
    public void editProduct(Product product) throws Exception {
        logger.debug("begin edit product: {}.", product);
        for (Product pr : products) {
            if (pr.getName().contains(product.getName())) {
                products.remove(pr);
                products.add(product);
                logger.debug("This product {}. is successful edit", product);
                break;
            }
        }
    }

    @Override
    public Map<Product, Float> getProductsInDish(long dishName) throws Exception {
        return null;
    }

    @Override
    public String imgProduct(String productName) throws Exception {
        return null;
    }

    @Override
    public List<String> searchProduct(String productName) throws Exception {
        return null;
    }
}
