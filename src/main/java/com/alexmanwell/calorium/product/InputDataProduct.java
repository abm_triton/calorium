package com.alexmanwell.calorium.product;

import java.util.Scanner;

public class InputDataProduct {

    public int writeKcal(Scanner scanner) {
        return readInt(scanner, "Введите количество килокалорий: ");
    }
    public float writeProtein(Scanner scanner) {
        return readFloat(scanner, "Введите количество белка: ");
    }
    public float writeCarbohydrate(Scanner scanner) { return readFloat(scanner, "Введите количество углеводов: "); }
    public float writeAFat(Scanner scanner) {
        return readFloat(scanner, "Введите количество животных жиров: ");
    }
    public float writeVFat(Scanner scanner) { return readFloat(scanner, "Введите количество растительных жиров: "); }

    public float writeVitamin(Scanner scanner, String name) {
        return readFloat(scanner, "Введите количество витамина " + name + ": ");
    }

    public float writeElement(Scanner scanner, String name) {
        return readFloat(scanner, "Введите количество элемента " + name.toLowerCase() + ": ");
    }
    public String writeGroup(Scanner scanner) {
        return readString(scanner, "Введите группу продукта: ");
    }

    public String writeDescription(Scanner scanner) {
        System.out.println("Введите описание продукта: ");
        scanner.useDelimiter("\\n");
        return scanner.next();
    }

    private String readString(Scanner scanner, String prompt) {
        System.out.print(prompt);
        boolean validInput = scanner.hasNext();
        while (!validInput) {
            scanner.next();
            System.out.println("Некорректный ввод данных. Введите заново");
            System.out.print(prompt);
            validInput = scanner.hasNext();
        }
        return scanner.next();
    }

    private int readInt(Scanner scanner, String prompt) {
        System.out.print(prompt);
        boolean validInput = scanner.hasNextInt();
        while (!validInput) {
            scanner.next();
            System.out.println("Некорректный ввод данных. Введите заново");
            System.out.print(prompt);
            validInput = scanner.hasNextInt();
        }
        return scanner.nextInt();
    }

    public static float readFloat(Scanner scanner, String prompt) {
        System.out.print(prompt);
        boolean validInput = scanner.hasNextFloat();
        while (!validInput) {
            scanner.next();
            System.out.println("Некорректный ввод данных. Введите заново");
            System.out.print(prompt);
            validInput = scanner.hasNextFloat();
        }
        return scanner.nextFloat();
    }
}
