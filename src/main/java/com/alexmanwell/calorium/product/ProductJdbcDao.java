package com.alexmanwell.calorium.product;

import com.alexmanwell.calorium.JdbcUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.*;

public class ProductJdbcDao implements ProductDao {

    final static Logger logger = LoggerFactory.getLogger(ProductJdbcDao.class);

    private Connection connection;
    public ProductJdbcDao(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Product selectProduct(String productName) throws Exception {
        logger.debug("begin search product");
        PreparedStatement stmt = null;
        PreparedStatement stmtFat = null;
        PreparedStatement stmtVitamins = null;
        PreparedStatement stmtElements = null;
        PreparedStatement stmtGroup = null;
        PreparedStatement stmtDescription = null;

        ResultSet resultSet = null;
        ResultSet resultSetFat = null;
        ResultSet resultSetVitamins = null;
        ResultSet resultSetElements = null;
        ResultSet resultSetGroup = null;
        ResultSet resultSetDescription = null;
        try {

            stmt = connection.prepareStatement("SELECT * FROM product WHERE name = ?");
            stmt.setString(1, productName);
            resultSet = stmt.executeQuery();

            logger.debug("execute query in table product {}.", resultSet);
            Product.Builder pb = new Product.Builder();

            String name = null;
            while (resultSet.next()) {
                name = resultSet.getString("name");
                pb.setName(name);
                pb.setKcal(resultSet.getInt("kcal"));
                pb.setProtein(resultSet.getFloat("protein"));
                pb.setCarbohydrate(resultSet.getFloat("carbohydrate"));
            }

            if (!Objects.equals(name, productName)) {
                return null;
            }

            stmtFat = connection.prepareStatement("SELECT * FROM product_fat WHERE name = ?");
            stmtFat.setString(1, productName);
            resultSetFat = stmtFat.executeQuery();

            logger.debug("execute query in table product_fat {}.", resultSetFat);

            while (resultSetFat.next()) {
                pb.setAFat(resultSetFat.getFloat("animal_fat"));
                pb.setVFat(resultSetFat.getFloat("vegetable_fat"));
            }

            stmtVitamins = connection.prepareStatement("SELECT * FROM product_vitamin WHERE name = ?");
            stmtVitamins.setString(1, productName);
            resultSetVitamins = stmtVitamins.executeQuery();

            logger.debug("execute query in table product_vitamin {}.", resultSetVitamins);

            while (resultSetVitamins.next()) {
                for (VitaminType vt : VitaminType.values()) {
                    float vv = resultSetVitamins.getFloat(vt.name());
                    pb.addVitamin(vt, vv);
                }
            }

            stmtElements = connection.prepareStatement("SELECT * FROM product_element WHERE product_name = ?");
            stmtElements.setString(1, productName);
            resultSetElements = stmtElements.executeQuery();

            logger.debug("execute query in table product_element {}.", resultSetElements);

            while (resultSetElements.next()) {
                String et = resultSetElements.getString("element_name");
                float ev = resultSetElements.getFloat("amount_elem");
                pb.addElement(ElementType.getElementType(et.trim()), ev);
            }

            stmtGroup = connection.prepareStatement("SELECT * FROM product_group WHERE product_name = ?");
            stmtGroup.setString(1, productName);
            resultSetGroup = stmtGroup.executeQuery();

            logger.debug("execute query in table product_group {}.", resultSetGroup);

            while (resultSetGroup.next()) {
                pb.setGroup(resultSetGroup.getString("group_name"));
            }

            stmtDescription = connection.prepareStatement("SELECT * FROM product_description WHERE product_name = ?");
            stmtDescription.setString(1, productName);
            resultSetDescription = stmtDescription.executeQuery();

            logger.debug("execute query in table product_group {}.", resultSetDescription);

            while (resultSetDescription.next()) {
                pb.setDescription(resultSetDescription.getString("description"));
            }

            return pb.build();
        } finally {
            JdbcUtils.closeSilently(resultSet);
            JdbcUtils.closeSilently(stmt);

            JdbcUtils.closeSilently(resultSetFat);
            JdbcUtils.closeSilently(stmtFat);

            JdbcUtils.closeSilently(resultSetVitamins);
            JdbcUtils.closeSilently(stmtVitamins);

            JdbcUtils.closeSilently(resultSetElements);
            JdbcUtils.closeSilently(stmtElements);

            JdbcUtils.closeSilently(resultSetGroup);
            JdbcUtils.closeSilently(stmtGroup);

            JdbcUtils.closeSilently(resultSetDescription);
            JdbcUtils.closeSilently(stmtDescription);

            logger.debug("end search product");
        }
    }

    @Override
    public void insertProduct(Product product) throws Exception {
        logger.debug("begin create product");
        try (
                PreparedStatement stmt = connection.prepareStatement("INSERT INTO product (name, kcal, protein, carbohydrate) VALUES (?, ?, ?, ?)");
                PreparedStatement stmtFat = connection.prepareStatement("INSERT INTO product_fat (name, animal_fat, vegetable_fat) VALUES (?, ?, ?)");
                PreparedStatement stmtVitamins = connection.prepareStatement("INSERT INTO product_vitamin (name, a1, a2, b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13, b15, c, d1, d2, d3, d4, d5, e, k1, k2, n, p, u) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                PreparedStatement stmtElements = connection.prepareStatement("INSERT INTO product_element (product_name, element_name, amount_elem) VALUES (?, ?, ?)");
                PreparedStatement stmtGroup = connection.prepareStatement("INSERT INTO product_group (product_name, group_name) VALUES (?, ?)");
                PreparedStatement stmtDescription = connection.prepareStatement("INSERT INTO product_description (product_name, description) VALUES (?, ?)");
                PreparedStatement stmtImage = connection.prepareStatement("INSERT INTO product_image (name, path) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);
        ) {
            stmt.setString(1, product.getName());
            stmt.setInt(2, product.getKcal());
            stmt.setFloat(3, product.getProtein());
            stmt.setFloat(4, product.getCarbohydrate());
            stmt.executeUpdate();

            logger.debug("successful insert in table product");

            stmtFat.setString(1, product.getName());
            stmtFat.setFloat(2, product.getAFat());
            stmtFat.setFloat(3, product.getVFat());
            stmtFat.executeUpdate();

            logger.debug("successful insert in table product_fat");

            if ( !product.getVitamins().isEmpty()) {
                stmtVitamins.setString(1, product.getName());
                int i = 2;
                for (VitaminType vt : VitaminType.values()) {
                    stmtVitamins.setFloat(i, product.getVitaminValue(vt));
                    i++;
                }
                stmtVitamins.executeUpdate();
                logger.debug("successful insert in table product_vitamin");
            }

            if (!product.getElements().isEmpty()) {
                for (Map.Entry<ElementType, Float> element : product.getElements().entrySet()) {
                    stmtElements.setString(1, product.getName());
                    stmtElements.setString(2, element.getKey().getElement());
                    stmtElements.setFloat(3, element.getValue());
                    stmtElements.executeUpdate();
                }
                logger.debug("successful insert in table product_element");
            }

            stmtGroup.setString(1, product.getName());
            stmtGroup.setString(2, product.getGroup());
            stmtGroup.executeUpdate();

            logger.debug("successful insert in table product_group");

            stmtDescription.setString(1, product.getName());
            stmtDescription.setString(2, product.getDescription());
            stmtDescription.executeUpdate();

            logger.info("successful insert in table product_description");

            stmtImage.setString(1, product.getName());

            final String PATH_DIRECTORY = "/image/";
            stmtImage.setString(2, PATH_DIRECTORY + product.getImageName());
            stmtImage.executeUpdate();

            logger.debug("successful insert in table product_description");
        }
        logger.debug("end create product");
    }

    @Override
    public void deleteProduct(String productName) throws Exception {
        try (
                PreparedStatement stmt = connection.prepareStatement("DELETE FROM product WHERE name = ?");
                PreparedStatement stmtFat = connection.prepareStatement("DELETE FROM product_fat WHERE name = ?");
                PreparedStatement stmtVitamins = connection.prepareStatement("DELETE FROM product_vitamin WHERE name = ?");
                PreparedStatement stmtElements = connection.prepareStatement("DELETE FROM product_element WHERE product_name = ?");
                PreparedStatement stmtGroup = connection.prepareStatement("DELETE FROM product_group WHERE product_name = ?");
                PreparedStatement stmtDescription = connection.prepareStatement("DELETE FROM product_description WHERE product_name = ?");
        ) {
            stmtDescription.setString(1, productName);
            stmtDescription.executeUpdate();

            stmtGroup.setString(1, productName);
            stmtGroup.executeUpdate();

            stmtElements.setString(1, productName);
            stmtElements.executeUpdate();

            stmtVitamins.setString(1, productName);
            stmtVitamins.executeUpdate();

            stmtFat.setString(1, productName);
            stmtFat.executeUpdate();

            stmt.setString(1, productName);
            stmt.executeUpdate();
        }
    }

    @Override
    public Collection<Product> findAllProducts() throws Exception {
        logger.debug("begin find all products");
        Statement stmt = null;
        ResultSet resultSet = null;

        Statement stmtElement = null;
        ResultSet rsElement = null;

        Collection<Product> products = new ArrayList<>();
        try {
            logger.debug("start loading product elements");
            stmtElement = connection.createStatement();
            rsElement = stmtElement.executeQuery("SELECT * FROM product_element");
            Map<String, Map<ElementType, Float>> elements = new HashMap<>();

            Product.Builder pb = new Product.Builder();

            while (rsElement.next()) {
                String productName = rsElement.getString("product_name");
                Map<ElementType, Float> productElements = elements.get(productName);
                if (productElements == null) {
                    productElements = new EnumMap<>(ElementType.class);
                    elements.put(productName, productElements);
                }
                String et = rsElement.getString("element_name").trim();
                float ev = rsElement.getFloat("amount_elem");
                productElements.put(ElementType.getElementType(et), ev);
            }
            logger.debug("finish loading product elements");

            stmt = connection.createStatement();
            resultSet = stmt.executeQuery("SELECT product.*, product_fat.animal_fat, product_fat.vegetable_fat, product_vitamin.*, product_group.*, product_description.* FROM product LEFT JOIN product_fat ON product.name = product_fat.name LEFT JOIN product_vitamin ON product.name = product_vitamin.name LEFT JOIN product_group ON product.name = product_group.product_name LEFT JOIN product_description ON product.name = product_description.product_name");

            logger.debug("result execute query {}.", resultSet);

            while (resultSet.next()) {
                String productName = resultSet.getString("name");

                pb.setName(productName);
                pb.setKcal(resultSet.getInt("kcal"));
                pb.setProtein(resultSet.getFloat("protein"));
                pb.setCarbohydrate(resultSet.getFloat("carbohydrate"));
                pb.setAFat(resultSet.getFloat("animal_fat"));
                pb.setVFat(resultSet.getFloat("vegetable_fat"));

                for (VitaminType vt : VitaminType.values()) {
                    float vv = resultSet.getFloat(vt.name());
                    pb.addVitamin(vt, vv);
                }

                Map<ElementType, Float> productElements = elements.get(productName);
                if (productElements != null) {
                    for (Map.Entry<ElementType, Float> pair : productElements.entrySet()) {
                        pb.addElement(ElementType.getElementType(pair.getKey().getElement()), pair.getValue());
                    }
                }

                pb.setGroup(resultSet.getString("group_name"));
                pb.setDescription(resultSet.getString("description"));

                Product product = pb.build();
                products.add(product);
                logger.debug("add product in list {}.", product);
            }
            return products;
        } finally {
            JdbcUtils.closeSilently(resultSet);
            JdbcUtils.closeSilently(stmt);

            JdbcUtils.closeSilently(rsElement);
            JdbcUtils.closeSilently(stmtElement);
        }
    }

    @Override
    public void editProduct(Product product) throws Exception {
        logger.debug("begin edit database {}.", product);
        try (
                PreparedStatement statement = connection.prepareStatement("UPDATE product SET kcal = ?, protein = ?, carbohydrate = ? WHERE name = ?");
                PreparedStatement statementFat = connection.prepareStatement("UPDATE product_fat SET animal_fat = ?, vegetable_fat = ? WHERE name = ?");
                PreparedStatement statementVitamins = connection.prepareStatement("UPDATE product_vitamin SET a1 = ?, a2 = ?, b1 = ?, b2 = ?, b3 = ?, b4 = ?, b5 = ?, b6 = ?, b7 = ?, b8 = ?, b9 = ?, b10 = ?, b11 = ?, b12 = ?, b13 = ?, b15 = ?, c = ?, d1 = ?, d2 = ?, d3 = ?, d4 = ?, d5 = ?, e = ?, k1 = ?, k2 = ?, n = ?, p = ?, u = ? WHERE name = ?");
                PreparedStatement statementElements = connection.prepareStatement("UPDATE product_element SET amount_elem = ? WHERE product_name = ? AND element_name = ?");
                PreparedStatement statementGroup = connection.prepareStatement("UPDATE product_group SET group_name = ? WHERE product_name = ?");
                PreparedStatement statementDescription = connection.prepareStatement("UPDATE product_description SET description = ? WHERE product_name = ?");

        ) {
            statement.setInt(1, product.getKcal());
            statement.setFloat(2, product.getProtein());
            statement.setFloat(3, product.getCarbohydrate());
            statement.setString(4, product.getName());
            statement.executeUpdate();

            statementFat.setFloat(1, product.getAFat());
            statementFat.setFloat(2, product.getVFat());
            statementFat.setString(3, product.getName());
            statementFat.executeUpdate();

            String vitaminProductName = selectVitaminsInEditProduct(product);

            if (!Objects.equals(vitaminProductName, product.getName())) {
                insertVitaminsInEditProduct(product);
            }

            int i = 1;
            for (VitaminType vt : VitaminType.values()) {
                statementVitamins.setFloat(i, product.getVitaminValue(vt));
                i++;
            }
            statementVitamins.setString(VitaminType.values().length + 1, product.getName());
            statementVitamins.executeUpdate();

            String elementProductName = selectElementsInEditProduct(product);

            if (!Objects.equals(elementProductName, product.getName())) {
                insertElementsInEditProduct(product);
            }

            for (Map.Entry<ElementType, Float> et : product.getElements().entrySet()) {
                logger.debug("begin edit product");
                statementElements.setFloat(1, et.getValue());
                statementElements.setString(2, product.getName());
                statementElements.setString(3, et.getKey().getElement());
                logger.debug("stmtInsertElements {}.", statementElements);
                statementElements.executeUpdate();
                logger.debug("end edit product");
            }

            statementGroup.setString(1, product.getGroup());
            statementGroup.setString(2, product.getName());
            statementGroup.executeUpdate();

            statementDescription.setString(1, product.getDescription());
            statementDescription.setString(2, product.getName());
            statementDescription.executeUpdate();
        }
        logger.debug("end edit product {}.", product);
    }

    @Override
    public Map<Product, Float> getProductsInDish(long dishId) throws Exception {
        logger.debug("begin search dish");
        ResultSet resultSet = null;
        try (
                PreparedStatement statement = connection.prepareStatement("SELECT * FROM dish_products WHERE dish_id = ?");
        ) {
            statement.setLong(1, dishId);
            resultSet = statement.executeQuery();

            Map<String, Float> products = new HashMap<>();
            while (resultSet.next()) {
                String productName = resultSet.getString("product_name");
                float amountProduct = resultSet.getInt("amount_product");
                products.put(productName, amountProduct);
            }

            Map<Product, Float> productFloatMap = new HashMap<>();
            Collection<Product> productsInDish =  findAllProducts();
            for (Product product : productsInDish) {
                if (products.containsKey(product.getName())) {
                    productFloatMap.put(product, products.get(product.getName()));
                }
            }
            return productFloatMap;
        } finally {
            JdbcUtils.closeSilently(resultSet);
            logger.debug("end search dish");
        }
    }

    @Override
    public String imgProduct(String productName) throws Exception {
        ResultSet rs = null;
        try (
                PreparedStatement statement = connection.prepareStatement("SELECT * FROM product_image WHERE name = ?");
        ) {
            logger.debug("begin select image path in ImgProduct");
            logger.debug("productName {} = ", productName);

            statement.setString(1, productName);
            rs = statement.executeQuery();

            String path = null;
            while (rs.next()) {
                path = rs.getString("path");
            }
            logger.debug("end select image path in ImgProduct {}", path);
            return path;
        }  finally {
            JdbcUtils.closeSilently(rs);
        }

    }

    @Override
    public List<String> searchProduct(String productName) throws Exception {
        ResultSet rs = null;
        try (

                PreparedStatement statement = connection.prepareStatement("SELECT * FROM product WHERE name LIKE ?");
        ) {
            statement.setString(1, "%" + productName);
            rs = statement.executeQuery();
            logger.debug("execute query in table product {}.", rs);
            List<String> names = new ArrayList<>();
            while (rs.next()) {
                names.add(rs.getString("name"));
            }
            return names;
        } finally {
            JdbcUtils.closeSilently(rs);
        }
    }


    private String selectVitaminsInEditProduct(Product product) throws SQLException {
        ResultSet rs = null;
        try (
                PreparedStatement statement = connection.prepareStatement("SELECT name FROM product_vitamin WHERE name = ?");
        ) {
            statement.setString(1, product.getName());
            rs = statement.executeQuery();
            logger.debug("execute query in table product {}.", rs);
            String name = null;
            while (rs.next()) {
                name = rs.getString("name");
            }
            return name;
        } finally {
            JdbcUtils.closeSilently(rs);
        }
    }

    private void insertVitaminsInEditProduct(Product product) throws SQLException {
        try (
                PreparedStatement statement = connection.prepareStatement("INSERT INTO product_vitamin (name, a1, a2, b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13, b15, c, d1, d2, d3, d4, d5, e, k1, k2, n, p, u) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        ) {
            logger.debug("begin create vitamins in Edit Product");
            statement.setString(1, product.getName());
            int j = 2;
            for (VitaminType vt : VitaminType.values()) {
                statement.setFloat(j, 0);
                j++;
            }
            statement.executeUpdate();
            logger.debug("successful insert in table product_vitamin");
        }
    }

    private String selectElementsInEditProduct(Product product) throws SQLException {
        ResultSet rs = null;
        try (
                PreparedStatement statement = connection.prepareStatement("SELECT product_name FROM product_element WHERE product_name = ?");
        ) {
            statement.setString(1, product.getName());
            rs = statement.executeQuery();

            logger.debug("execute query in table product_element {}.", rs);

            String name = null;
            while (rs.next()) {
                name = rs.getString("product_name");
            }
            return name;
        } finally {
            JdbcUtils.closeSilently(rs);
        }
    }

    private void insertElementsInEditProduct(Product product) throws SQLException {
        try (
                PreparedStatement statement = connection.prepareStatement("INSERT INTO product_element (product_name, element_name, amount_elem) VALUES (?, ?, ?)");
        ) {
            logger.debug("begin create elements if they don't in the table");
            for (ElementType element : ElementType.values()) {
                statement.setString(1, product.getName());
                statement.setString(2, element.getElement());
                statement.setFloat(3, 0);
                logger.debug("statement {}.", statement);

                statement.executeUpdate();
                logger.debug("successful insert element in table product_element");
            }
        }
    }
}
