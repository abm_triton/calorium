package com.alexmanwell.calorium;

import com.alexmanwell.calorium.dish.*;
import com.alexmanwell.calorium.product.*;
import com.alexmanwell.calorium.profile.Profile;
import com.alexmanwell.calorium.profile.ProfileDao;
import com.alexmanwell.calorium.profile.ProfileJdbcDao;
import com.alexmanwell.calorium.eating.UserEating;
import com.alexmanwell.calorium.eating.UserEatingDao;
import com.alexmanwell.calorium.eating.UserEatingJdbcDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.*;

public class Search {

    final static Logger logger = LoggerFactory.getLogger(Search.class);

    public static void printProducts(Collection<Product> products) {
        for (Product product : products) {
            Search.printProduct(product);
        }
    }

    public static void printProduct(Product product) {
        logger.debug("begin print product {}.", product);
        System.out.printf("%-25s", product.getName());
        System.out.printf("%-16s", product.getKcal());
        System.out.printf("%-12s", product.getProtein());
        System.out.printf("%-10s", product.getCarbohydrate());
        System.out.printf("%-15s", product.getAFat());
        System.out.printf("%-15s", product.getVFat());

        if (!product.getVitamins().isEmpty()) {
            for (VitaminType vt : VitaminType.values()) {
                System.out.printf("%-8s", product.getVitaminValue(vt));
            }
        }

        for (ElementType et : ElementType.values()) {
            System.out.printf("%-12s", product.getElementValue(et));
        }

        System.out.printf("%-12s", product.getGroup());
        System.out.printf("%-12s", product.getDescription());

        System.out.println();
        logger.debug("end print product {}.", product);
    }

    public static void printTitleTable() {
        System.out.printf("%-25s", "Название продукта");
        System.out.printf("%-16s", "Килокалории");
        System.out.printf("%-12s", "Белки");
        System.out.printf("%-10s", "Углеводы");
        System.out.printf("%-15s", "Раст. жиры");
        System.out.printf("%-15s", "Живот. жиры");

        for (VitaminType vt : VitaminType.values()) {
            System.out.printf("%-8s", vt.name());
        }

        for (ElementType et : ElementType.values()) {
            System.out.printf("%-12s", et.getElement());
        }

        System.out.printf("%-12s", "Группа продукта");
        System.out.printf("%-12s", "Описание");

        System.out.println();

        for (int i = 0; i < 125 + VitaminType.values().length * 8 + ElementType.values().length * 12; i++) {
            System.out.print("-");
        }
        System.out.println();
    }

    public static void printAllDish(Collection<Dish> allDish) {
        for (Dish dish : allDish) {
            Search.printDish(dish);
        }
    }

    public static void printDish(Dish dish) {
        logger.debug("begin print dish {}.", dish);
        System.out.printf("%-25s", dish.getName());
        System.out.println();

        for (Map.Entry<String, Float> product : dish.getProducts().entrySet()) {
            System.out.printf("%-28s", " ");
            System.out.printf("%-30s", product.getKey());
            System.out.printf("%-12s", product.getValue());
            System.out.println();
        }
        System.out.println();
        logger.debug("end print dish {}.", dish);
    }

    public static void printDishTitleTable() {
        System.out.printf("%-25s", "Название блюда");
        System.out.printf("%-25s", "Название ингридиента( продукта)");
        System.out.printf("%-12s", "Количество в граммах");
        System.out.println();
    }

    public static void printDishTotalBJU(List<Product> products) {
        CalculateNutients calculate = new CalculateNutients();
        Product product = calculate.resultNutrientsAllDishes(products);
        float kcal = product.getKcal();
        float protein = product.getProtein();
        float carbohydrate = product.getCarbohydrate();
        float aFat = product.getAFat();
        float vFat = product.getVFat();
        Map<VitaminType, Float> vitamins = product.getVitamins();
        Map<ElementType, Float> elements = product.getElements();

        System.out.printf("%-25s", "Итого:");
        System.out.printf("%-16s", kcal);
        System.out.printf("%-12s", protein);
        System.out.printf("%-10s", carbohydrate);
        System.out.printf("%-15s", aFat);
        System.out.printf("%-15s", vFat);

        for (VitaminType vt : VitaminType.values()) {
            System.out.printf("%-8s", vitamins.get(vt));
        }

        for (ElementType et : ElementType.values()) {
            System.out.printf("%-12s", elements.get(et));
        }
    }

    public static void main(String[] args) throws Exception {
        try {
            Connection connection = ConnectionFactory.getConnection();
            ProductDao productJdbcDao = new ProductJdbcDao(connection);
            DishDao dishJdbcDao = new DishJdbcDao(connection);
            ProfileDao profileJdbcDao = new ProfileJdbcDao(connection);
            UserEatingDao userEatingJdbcDao = new UserEatingJdbcDao(connection);
            ProductDao productInMemoryDao = new ProductInMemoryDao();
            CommandContext commandContext = new CommandContext(productJdbcDao, dishJdbcDao, profileJdbcDao, userEatingJdbcDao);
            Scanner scanner = new Scanner(System.in);
            while (true) {
                String input = scanner.nextLine();
                Command command = CommandFactory.createCommand(input);
                if (command != null) {
                    logger.debug("begin execute command");
                    command.execute(commandContext);
                } else {
                    System.out.println("Введите коректную команду, узнать больше о командах можно при помоще команды help");
                }
                System.out.println();
                logger.debug("end command {}.", input);
            }
        } catch (Exception ex) {
            logger.error("Error: ", ex);
        }
    }

    public static void printProfileTitleTable() {
        System.out.printf("%-20s", "Id записи");
        System.out.printf("%-20s", "Ник юзера");
        System.out.printf("%-12s", "Электропочта");
        System.out.println();
        for (int i = 0; i < 75; i++) {
            System.out.print("-");
        }
        System.out.println();
    }

    public static void printProfile(Profile profile) {
        System.out.printf("%-20s", profile.getId());
        System.out.printf("%-20s", profile.getNickname());
        System.out.printf("%-12s", profile.getMail());
        System.out.println();
    }

    public static void printProfiles(Collection<Profile> profiles) {
        for (Profile profile : profiles) {
            Search.printProfile(profile);
        }
    }

    public static void printUserEatingTitleTable() {

    }

    public static void printUserEating(Profile profile, UserEating userEating) {
        System.out.printf("%-20s", profile.getNickname());
        System.out.println();
        for (Map.Entry<Dish, Float> e : userEating.getUserDish().entrySet()) {
            System.out.printf("%-20s", e.getKey().getName());
            System.out.printf("%-20s", e.getValue());
            System.out.println();
        }
    }
}