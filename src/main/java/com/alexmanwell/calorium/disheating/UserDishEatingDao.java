package com.alexmanwell.calorium.disheating;

/**
 * Created by qw on 17.10.2016.
 */
public interface UserDishEatingDao {

    UserDishEating add(UserDishEating userDishEating);
    void delete(long dishEatingId);
    void update(final UserDishEating ude) throws Exception;
    UserDishEating select(final UserDishEating ude);
    UserDishEating select(final long dishEatingId) throws Exception;
}
