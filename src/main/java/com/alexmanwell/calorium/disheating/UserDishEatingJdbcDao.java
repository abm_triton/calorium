package com.alexmanwell.calorium.disheating;

import com.alexmanwell.calorium.dish.Dish;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class UserDishEatingJdbcDao implements UserDishEatingDao {


    final static Logger logger = LoggerFactory.getLogger(UserDishEatingJdbcDao.class);

    private Connection connection;

    public UserDishEatingJdbcDao(Connection connection) {
        this.connection = connection;
    }

    @Override
    public UserDishEating add(UserDishEating userDishEating) {
        return null;
    }

    @Override
    public void delete(long dishEatingId) {

    }

    @Override
    public void update(UserDishEating ude) throws Exception {
        logger.debug("begin update dish in mealtime {}", ude.getDish().getName());
        try (
                PreparedStatement stmt = connection.prepareStatement("UPDATE user_dish_eating SET amount_dish = ? WHERE dish_eating_id = ? AND eating_id = ? AND user_dish_eating.dish_id = ?");
        ) {
            stmt.setFloat(1, ude.getAmountDish());
            stmt.setLong(2, ude.getDishEatingId());
            stmt.setLong(3, ude.getEatingId());
            stmt.setLong(4, ude.getDishId());
            stmt.executeUpdate();
        }
        logger.debug("end update dish in mealtime {}", ude.getDish().getName());
    }


    @Override
    public UserDishEating select(UserDishEating ude) {
        return null;
    }

    @Override
    public UserDishEating select(long dishEatingId) throws Exception {
        try (PreparedStatement stmt = connection.prepareStatement("SELECT * FROM user_dish_eating ude LEFT JOIN dish d ON ude.dish_id = d.dish_id LEFT JOIN dish_products dp ON dp.dish_id = d.dish_id WHERE ude.dish_eating_id = ?")) {
            stmt.setLong(1, dishEatingId);
            try (ResultSet rs = stmt.executeQuery()) {
                UserDishEating.Builder udeBuilder = new UserDishEating.Builder();
                Dish.Builder dishBuilder = new Dish.Builder();
                while (rs.next()) {
                    dishEatingId = rs.getLong("dish_eating_id");
                    long eatingId = rs.getLong("eating_id");
                    long dishId = rs.getLong("dish_id");
                    float amountDish = rs.getFloat("amount_dish");
                    udeBuilder.setDishEatingId(dishEatingId).setEatingId(eatingId).setDishId(dishId).setAmountDish(amountDish);

                    String name = rs.getString("name");
                    dishBuilder.setName(name);

                    String productName = rs.getString("product_name");
                    float amountProduct = rs.getFloat("amount_product");
                    dishBuilder.addProducts(productName, amountProduct);
                }
                Dish dish = dishBuilder.build();
                udeBuilder.setDish(dish);
                return udeBuilder.build();
            }
        }
    }
}
