package com.alexmanwell.calorium.disheating;

import com.alexmanwell.calorium.dish.Dish;
import com.alexmanwell.calorium.eating.UserEating;

public class UserDishEating {

    private long dishEatingId;
    private long eatingId;
    private long dishId;
    private float amountDish;
    private Dish dish;
    private UserEating userEating;

    public UserDishEating() {

    }

    public UserDishEating(Dish dish, float amountDish, UserEating userEating) {
        this.dish = dish;
        this.amountDish = amountDish;
        this.userEating = userEating;
    }

    public UserDishEating(long dishId, float amountDish, UserEating userEating) {
        this.dishId = dishId;
        this.amountDish = amountDish;
        this.userEating = userEating;
    }

    public UserDishEating(long dishEatingId, Dish dish, float amountDish) {
        this.dish = dish;
        this.dishEatingId = dishEatingId;
        this.amountDish = amountDish;
    }


    public UserDishEating(long dishEatingId) {
        this.dishEatingId = dishEatingId;
    }

    public UserDishEating(UserDishEating.Builder ueb) {
        this.dishEatingId = ueb.dishEatingId;
        this.eatingId = ueb.eatingId;
        this.dishId = ueb.dishId;
        this.dish = ueb.dish;
        this.amountDish = ueb.amountDish;

    }

    public UserDishEating(long dishEatingId, long eatingId, long dishId, String dishName, float amountDish) {
        this.dishEatingId = dishEatingId;
        this.eatingId = eatingId;
        this.amountDish = amountDish;
        this.dishId = dishId;
    }

    public long getDishEatingId() {
        return dishEatingId;
    }

    public void setDishEatingId(long dishEatingId) {
        this.dishEatingId = dishEatingId;
    }

    public long getDishId() {
        return dishId;
    }

    public void setDishId(long dishId) {
        this.dishId = dishId;
    }

    public float getAmountDish() {
        return amountDish;
    }

    public void setAmountDish(float amountDish) {
        this.amountDish = amountDish;
    }

    public UserEating getUserEating() {
        return userEating;
    }

    public void setUserEating(UserEating userEating) {
        this.userEating = userEating;
    }

    @Override
    public String toString() {
        return String.format("(dishEatingId = %s, eatingId = %s, dish = %s, dishId = %s, amountDish = %s)", dishEatingId, eatingId, dish, dishId, amountDish);
    }

    public long getEatingId() {
        return eatingId;
    }

    public void setEatingId(long eatingId) {
        this.eatingId = eatingId;
    }

    public Dish getDish() {
        return dish;
    }

    public void setDish(Dish dish) {
        this.dish = dish;
    }


    public static class Builder {
        private long dishEatingId;
        private long eatingId;
        private long dishId;
        private float amountDish;
        private Dish dish;

        public Builder() {
        }

        public Builder(UserDishEating ude) {
            this.dishEatingId = ude.getDishEatingId();
            this.eatingId = ude.getEatingId();
            this.dish = ude.dish;
            this.amountDish = ude.getAmountDish();
            this.dishId = ude.getDishId();
        }

        public UserDishEating.Builder setDishEatingId(long dishEatingId) {
            this.dishEatingId = dishEatingId;
            return this;
        }

        public UserDishEating.Builder setEatingId(long eatingId) {
            this.eatingId = eatingId;
            return this;
        }

        public UserDishEating.Builder setAmountDish(float amountDish) {
            this.amountDish = amountDish;
            return this;
        }

        public UserDishEating.Builder setDishId(long dishId) {
            this.dishId = dishId;
            return this;
        }

        public UserDishEating.Builder setDish(Dish dish) {
            this.dish = dish;
            return this;
        }

        public UserDishEating build() {
            return new UserDishEating(this);
        }
    }
}
