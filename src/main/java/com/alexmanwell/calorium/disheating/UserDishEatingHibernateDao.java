package com.alexmanwell.calorium.disheating;

import com.alexmanwell.calorium.HibernateUtils;
import org.hibernate.Session;

public class UserDishEatingHibernateDao implements UserDishEatingDao {

    @Override
    public UserDishEating add(UserDishEating ude) {
        try (final Session session = HibernateUtils.getSessionFactory().openSession()) {
            session.getTransaction().begin();
            session.save(ude);
            session.getTransaction().commit();
            return ude;
        }
    }

    @Override
    public void delete(long dishEatingId) {
        try ( final Session session = HibernateUtils.getSessionFactory().openSession()) {
            session.getTransaction().begin();
            session.delete(new UserDishEating(dishEatingId));
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(UserDishEating ude) {
        try ( final Session session = HibernateUtils.getSessionFactory().openSession()) {
            session.getTransaction().begin();
            session.update(ude);
            session.getTransaction().commit();
        }
    }

    @Override
    public UserDishEating select(final UserDishEating ude) {
        try (final Session session = HibernateUtils.getSessionFactory().openSession()) {
            session.getTransaction().begin();
            final UserDishEating userDishEating = session.get(UserDishEating.class, ude.getDishEatingId());

            if (userDishEating == null) {
                throw new IllegalStateException("Такого приема пищи нет: " + ude);
            }
            session.getTransaction().commit();
            return userDishEating;
        }
    }

    @Override
    public UserDishEating select(final long dishEatingId) throws Exception {
        try (final Session session = HibernateUtils.getSessionFactory().openSession()) {
            session.getTransaction().begin();
            final UserDishEating userDishEating = session.get(UserDishEating.class, dishEatingId);

            if (userDishEating == null) {
                throw new IllegalStateException("Такого приема пищи нет: " + dishEatingId);
            }
            session.getTransaction().commit();
            return userDishEating;
        }
    }
}
