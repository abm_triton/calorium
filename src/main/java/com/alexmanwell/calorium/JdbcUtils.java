package com.alexmanwell.calorium;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JdbcUtils {
    private final static Logger logger = LoggerFactory.getLogger(JdbcUtils.class);
    public static void closeSilently(AutoCloseable stmt) {
        if (stmt != null) {
            logger.debug("close {}.", stmt);
            try {
                stmt.close();
            } catch (Exception e) {
                logger.warn("failed to close {}.", stmt, e);
            }
        }
    }
}
