package com.alexmanwell.calorium.profile;

import com.alexmanwell.calorium.Command;
import com.alexmanwell.calorium.CommandContext;
import com.alexmanwell.calorium.Search;

import java.util.Objects;
import java.util.Scanner;

public class EditProfileCommand implements Command {

    private String nickname;

    public EditProfileCommand(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public void execute(CommandContext context) throws Exception {
        ProfileDao profileDao = context.getProfileDao();
        InputDataProfile input = new InputDataProfile();
        Scanner scanner = new Scanner(System.in);

        Profile profile = profileDao.searchProfile(nickname);
        if (profile == null) {
            throw new IllegalArgumentException("Такого продукта нет в базе " + nickname);
        }

        String password = input.readPassword(scanner);
        String confirmPassword = input.readConfirmPassword(scanner);
        while (!Objects.equals(password, confirmPassword)) {
            System.out.println("Не правильно введен пароль.");
            confirmPassword = input.readConfirmPassword(scanner);
        }

        String mail = input.readMail(scanner);

        profile = new Profile(nickname, mail);

        profileDao.editProfile(profile, password);
        Search.printProfileTitleTable();
        Search.printProfile(profile);
    }
}
