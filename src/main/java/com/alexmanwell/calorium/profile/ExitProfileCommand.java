package com.alexmanwell.calorium.profile;

import com.alexmanwell.calorium.Command;
import com.alexmanwell.calorium.CommandContext;

public class ExitProfileCommand implements Command {

    @Override
    public void execute(CommandContext context) throws Exception {
        context.setProfile(null);
    }
}
