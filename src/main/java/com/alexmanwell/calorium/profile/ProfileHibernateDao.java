package com.alexmanwell.calorium.profile;

import com.alexmanwell.calorium.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.Collection;
import java.util.List;

public class ProfileHibernateDao implements ProfileDao {

    @Override
    public Profile loginProfile(String nickname, String password) throws Exception {
        try (final Session session = HibernateUtils.getSessionFactory().openSession()) {
            session.getTransaction().begin();
            Profile profile = session.createNativeQuery("SELECT * FROM users WHERE nickname = ? AND password = ?", Profile.class)
                    .setParameter(1, nickname)
                    .setParameter(2, password)
                    .getSingleResult();

            if (profile == null) {
                throw new IllegalStateException("Профиль с таким ником не существует" + nickname);
            }
            session.getTransaction().commit();
            return profile;
        }
    }

    @Override
    public void createProfile(Profile profile, String password) throws Exception {
        try (final Session session = HibernateUtils.getSessionFactory().openSession()) {
            session.getTransaction().begin();
            final Query query = session.createNativeQuery("INSERT INTO users(nickname, password, mail) VALUES (?, ?, ?)");
            query.setParameter(1, profile.getNickname());
            query.setParameter(2, password);
            query.setParameter(3, profile.getMail());
            query.executeUpdate();
            session.getTransaction().commit();
        }
    }

    @Override
    public void deleteProfile(String nickname) throws Exception {
        try (final Session session = HibernateUtils.getSessionFactory().openSession()) {
            session.getTransaction().begin();
            final Query query = session.createNativeQuery("DELETE FROM users WHERE nickname = ?", Profile.class);
            query.setParameter(1, nickname);
            query.executeUpdate();
            session.getTransaction().commit();
        }
    }

    @Override
    public void editProfile(Profile profile, String password) throws Exception {
        try (final Session session = HibernateUtils.getSessionFactory().openSession()) {
            session.getTransaction().begin();
            final Query query = session.createNativeQuery("UPDATE users SET password = ?, mail = ? WHERE nickname = ?", Profile.class);
            query.setParameter(1, password);
            query.setParameter(2, profile.getMail());
            query.setParameter(3, profile.getNickname());
            query.executeUpdate();
            session.getTransaction().commit();
        }
    }

    @Override
    public Profile searchProfile(String nickname) throws Exception {
        try (final Session session = HibernateUtils.getSessionFactory().openSession()) {
            session.getTransaction().begin();
            final Profile profile = (Profile) session.createQuery("FROM Profile AS pr WHERE pr.nickname=:nickname")
                    .setParameter("nickname", nickname)
                    .getSingleResult();

            if (profile == null) {
                throw new IllegalStateException("Профиль с таким ником не существует" + nickname);
            }
            session.getTransaction().commit();
            return profile;
        }
    }

    @Override
    public Profile searchProfile(String nickname, String password) throws Exception {
        try (final Session session = HibernateUtils.getSessionFactory().openSession()) {
            session.getTransaction().begin();
            Profile profile = session.createNativeQuery("SELECT * FROM users WHERE nickname = ? AND password = ?", Profile.class)
                    .setParameter(1, nickname)
                    .setParameter(2, password)
                    .getSingleResult();

            if (profile == null) {
                throw new IllegalStateException("Профиль с таким ником не существует" + nickname);
            }
            session.getTransaction().commit();
            return profile;
        }
    }

    @Override
    public Collection<Profile> searchAllProfiles() throws Exception {
        List<Profile> list;
        try (Session session = HibernateUtils.getSessionFactory().openSession()) {
            session.getTransaction().begin();
            list = session.createQuery("FROM Profile").list();
            session.getTransaction().commit();
        }
        return list;
    }
}