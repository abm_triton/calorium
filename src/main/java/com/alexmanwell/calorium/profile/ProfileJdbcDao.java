package com.alexmanwell.calorium.profile;

import com.alexmanwell.calorium.JdbcUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProfileJdbcDao implements ProfileDao {

    private final static Logger logger = LoggerFactory.getLogger(ProfileJdbcDao.class);

    private Connection connection;

    public ProfileJdbcDao(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Profile loginProfile(String nickname, String password) throws Exception {
        logger.debug("begin log in profile {}", nickname);
        PreparedStatement stmtUser = null;
        ResultSet rs = null;
        try {
            stmtUser = connection.prepareStatement("SELECT * FROM users WHERE nickname = ? AND password = ?");
            stmtUser.setString(1, nickname);
            stmtUser.setString(2, password);
            rs = stmtUser.executeQuery();

            Profile profile = null;
            while( rs.next()) {
                long id = rs.getLong("user_id");
                nickname = rs.getString("nickname");
                String mail = rs.getString("mail");
                profile = new Profile(id, nickname, mail);
            }

            logger.debug("end log in profile {}", nickname);
            return profile;
        } finally {
            JdbcUtils.closeSilently(stmtUser);
            JdbcUtils.closeSilently(rs);
        }
    }

    @Override
    public void createProfile(Profile profile, String password) throws Exception {
        logger.debug("begin create profile");
        try (
                PreparedStatement stmtUser = connection.prepareStatement("INSERT INTO users (nickname, password, mail) VALUES (?, ?, ?)");
        ) {
            stmtUser.setString(1, profile.getNickname());
            stmtUser.setString(2, password);
            stmtUser.setString(3, profile.getMail());
            stmtUser.getGeneratedKeys();
            stmtUser.executeUpdate();
        }
        logger.debug("end create profile");
    }

    @Override
    public void deleteProfile(String nickname) throws Exception {
        try (
                PreparedStatement stmt = connection.prepareStatement("DELETE FROM users WHERE nickname = ?");
        ) {
            stmt.setString(1, nickname);
            stmt.executeUpdate();
        }
    }

    @Override
    public void editProfile(Profile profile, String password) throws Exception {
        logger.debug("begin edit profile");
        try (
                PreparedStatement stmtUser = connection.prepareStatement("UPDATE users SET password = ?, mail = ? WHERE nickname = ? ");
        ) {
            stmtUser.setString(1, password);
            stmtUser.setString(2, profile.getMail());
            stmtUser.setString(3, profile.getNickname());
            stmtUser.executeUpdate();
            logger.debug("Successful edit profile {}, {}, {}, {}", profile.getId(), profile.getNickname(), hidePassword(password), profile.getMail());
        }
        logger.debug("end edit profile");
    }

    @Override
    public Profile searchProfile(String nickname) throws Exception {
        logger.debug("begin search profile {}", nickname);
        PreparedStatement stmtUser = null;
        ResultSet rs = null;
        try {
            stmtUser = connection.prepareStatement("SELECT * FROM users WHERE nickname = ?");
            stmtUser.setString(1, nickname);
            rs = stmtUser.executeQuery();

            Profile profile = null;
            while( rs.next()) {
                long id = rs.getLong("user_id");
                nickname = rs.getString("nickname");
                String mail = rs.getString("mail");
                profile = new Profile(id, nickname, mail);
            }

            logger.debug("end search profile");
            return profile;
        } finally {
            JdbcUtils.closeSilently(stmtUser);
            JdbcUtils.closeSilently(rs);
        }
    }

    @Override
    public Collection<Profile> searchAllProfiles() throws Exception {
        Statement stmtUser = null;
        ResultSet rs = null;
        List<Profile> profiles = new ArrayList<>();
        try {
            stmtUser = connection.createStatement();
            rs = stmtUser.executeQuery("SELECT * FROM users");

            while (rs.next()) {
                long id = rs.getLong("user_id");
                String nickname = rs.getString("nickname");
                String mail = rs.getString("mail");
                Profile profile = new Profile(id, nickname, mail);
                profiles.add(profile);
            }
            return profiles;
        } finally {
            JdbcUtils.closeSilently(rs);
            JdbcUtils.closeSilently(stmtUser);
        }
    }

    @Override
    public Profile searchProfile(String nickname, String password) throws Exception {
        logger.debug("begin confirm profile {}", nickname);
        PreparedStatement stmtUser = null;
        ResultSet rs = null;
        try {

            stmtUser = connection.prepareStatement("SELECT * FROM users WHERE nickname = ? AND password = ?");
            stmtUser.setString(1, nickname);
            stmtUser.setString(2, CryptPassword.cryptPassword(password));
            rs = stmtUser.executeQuery();

            Profile profile = null;
            while( rs.next()) {
                long id = rs.getLong("user_id");
                nickname = rs.getString("nickname");
                String mail = rs.getString("mail");
                profile = new Profile(id, nickname, mail);
            }

            logger.debug("profile = {}", profile);
            logger.debug("end confirm profile");
            return profile;
        } finally {
            JdbcUtils.closeSilently(stmtUser);
            JdbcUtils.closeSilently(rs);
        }
    }

    private String hidePassword(String password) {
        final Pattern pattern = Pattern.compile("\\w");
        final Matcher matcher = pattern.matcher(password);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, "*");
        }
        return sb.toString();
    }
}
