package com.alexmanwell.calorium.profile;

import com.alexmanwell.calorium.Command;
import com.alexmanwell.calorium.CommandContext;
import com.alexmanwell.calorium.Search;

public class PrintAllProfileCommand implements Command {
    @Override
    public void execute(CommandContext context) throws Exception {
        Search.printProfileTitleTable();
        Search.printProfiles(context.getProfileDao().searchAllProfiles());
    }
}
