package com.alexmanwell.calorium.profile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;

public class CryptPassword {

    final static Logger logger = LoggerFactory.getLogger(CryptPassword.class);

    public static String cryptPassword(String password) {
        String md5 = null;
        MessageDigest crypt;
        final String salt = "boom";
        try {
            crypt = MessageDigest.getInstance("MD5");
            password += salt;
            logger.debug("Password + salt: {}", password);
            crypt.update(password.getBytes());
            md5 = byteToHex(crypt.digest());
        } catch(NoSuchAlgorithmException e) {
            logger.warn("Error to enconding password: ", e);
        }
        return md5;
    }

    private static String byteToHex(byte[] hash) {
        Formatter formatter = new Formatter();
        for (byte b : hash) {
            formatter.format("%02x", b);
        }
        String result = formatter.toString();
        formatter.close();
        return result;
    }
}
