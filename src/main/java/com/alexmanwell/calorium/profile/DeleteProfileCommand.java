package com.alexmanwell.calorium.profile;

import com.alexmanwell.calorium.Command;
import com.alexmanwell.calorium.CommandContext;

public class DeleteProfileCommand implements Command {
    private String nickname;

    public DeleteProfileCommand(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public void execute(CommandContext context) throws Exception {
        ProfileDao profileDao = context.getProfileDao();
        Profile profile = profileDao.searchProfile(nickname);
        if (profile != null) {
            profileDao.deleteProfile(nickname);
            profileDao.searchAllProfiles();
        } else {
            System.out.println("Профиля с таким названием нет.");
        }
    }
}
