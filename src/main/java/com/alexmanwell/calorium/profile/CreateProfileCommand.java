package com.alexmanwell.calorium.profile;

import com.alexmanwell.calorium.Command;
import com.alexmanwell.calorium.CommandContext;
import com.alexmanwell.calorium.Search;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;
import java.util.Scanner;

public class CreateProfileCommand implements Command {


    final static Logger logger = LoggerFactory.getLogger(CreateProfileCommand.class);

    private String nickname;

    public CreateProfileCommand(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public void execute(CommandContext context) throws Exception {
        logger.debug("begin create profile {}", nickname);
        ProfileDao profileDao = context.getProfileDao();
        InputDataProfile input = new InputDataProfile();
        Scanner scanner = new Scanner(System.in);

        Profile profile = profileDao.searchProfile(nickname);

        if (profile != null) {
            throw new IllegalArgumentException("Профиль с таким ником уже есть в базе: " + nickname);
        }

        String password = input.readPassword(scanner);
        String confirmPassword = input.readConfirmPassword(scanner);
        while (!Objects.equals(password, confirmPassword)) {
            System.out.println("Не правильно введен пароль.");
            confirmPassword = input.readConfirmPassword(scanner);
        }

        String mail = input.readMail(scanner);
        profile = new Profile(nickname, mail);

        profileDao.createProfile(profile, CryptPassword.cryptPassword(password));

        profile = profileDao.searchProfile(nickname);
        context.setProfile(profile);
        Search.printProfileTitleTable();
        Search.printProfile(profile);
        logger.debug("end create profile {}", nickname);
    }
}
