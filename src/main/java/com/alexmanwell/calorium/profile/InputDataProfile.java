package com.alexmanwell.calorium.profile;

import java.util.Scanner;

public class InputDataProfile {
    public String readPassword(Scanner scanner) {
        System.out.println("Введите пароль: ");
        return scanner.next();
    }

    public String readConfirmPassword(Scanner scanner) {
        System.out.println("Повторите пароль: ");
        return scanner.next();
    }

    public String readMail(Scanner scanner) {
        System.out.println("Введите электропочту: ");
        return scanner.next();
    }
}
