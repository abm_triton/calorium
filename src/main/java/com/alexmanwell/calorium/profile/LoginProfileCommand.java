package com.alexmanwell.calorium.profile;

import com.alexmanwell.calorium.Command;
import com.alexmanwell.calorium.CommandContext;
import com.alexmanwell.calorium.Search;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Scanner;

public class LoginProfileCommand implements Command{

    private final static Logger logger = LoggerFactory.getLogger(LoginProfileCommand.class);

    private final String nickname;

    public LoginProfileCommand(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public void execute(CommandContext context) throws Exception {
        logger.debug("begin log in profile {}", nickname);
        ProfileDao profileDao = context.getProfileDao();
        InputDataProfile input = new InputDataProfile();
        Scanner scanner = new Scanner(System.in);

        Profile profile = profileDao.searchProfile(nickname);

        if (profile == null) {
            throw new IllegalArgumentException("Профиль с таким ником не существует: " + nickname);
        }

        String password = input.readPassword(scanner);

        profile = profileDao.loginProfile(nickname, CryptPassword.cryptPassword(password));

        while (profile == null) {
            System.out.println("Пароль введен не верно. Введите заново пароль:");
            password = scanner.next();
            profile = profileDao.loginProfile(nickname, CryptPassword.cryptPassword(password));
        }

        context.setProfile(profile);
        Search.printProfileTitleTable();
        Search.printProfile(profile);
        logger.debug("end log in profile {}", nickname);
    }
}
