package com.alexmanwell.calorium.profile;

import java.util.Collection;

public interface ProfileDao {
    Profile loginProfile(String nickname, String password) throws Exception;
    void createProfile(Profile profile, String password) throws Exception;
    void deleteProfile(String nickname) throws Exception;
    void editProfile(Profile profile, String password) throws Exception;

    Profile searchProfile(String nickname) throws Exception;
    Profile searchProfile(String nickname, String password) throws Exception;
    Collection<Profile> searchAllProfiles() throws Exception;

}
