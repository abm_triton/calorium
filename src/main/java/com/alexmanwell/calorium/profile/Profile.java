package com.alexmanwell.calorium.profile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Profile {

    final static Logger logger = LoggerFactory.getLogger(Profile.class);

    private long id;
    private String nickname;
    private String mail;

    public Profile() {
    }

    public Profile(long id, String nickname, String mail) {
        this.id = id;
        this.nickname = nickname;
        this.mail = mail;
    }

    public Profile(String nickname, String mail) {
        this.nickname = nickname;
        this.mail = mail;
    }

    public long getId() {
        return id;
    }

    public String getNickname() {
        return nickname;
    }

    public String getMail() {
        return mail;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Profile profile = (Profile) o;

        if (id != profile.id) return false;
        if (nickname != null ? !nickname.equals(profile.nickname) : profile.nickname != null) return false;
        return mail != null ? mail.equals(profile.mail) : profile.mail == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (nickname != null ? nickname.hashCode() : 0);
        result = 31 * result + (mail != null ? mail.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return String.format("(Профиль| id: %s, Никнейм: %s, электропочта: %s)", id, nickname, mail);
    }
}
