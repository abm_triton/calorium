package com.alexmanwell.calorium;

public interface Command {
    void execute(CommandContext context) throws Exception;
}
