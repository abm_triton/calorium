package com.alexmanwell.calorium;

import com.alexmanwell.calorium.dish.*;
import com.alexmanwell.calorium.product.*;
import com.alexmanwell.calorium.profile.*;
import com.alexmanwell.calorium.eating.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CommandFactory {

    final static Logger logger = LoggerFactory.getLogger(CommandFactory.class);

    private static final Command HELP_COMMAND = new Command() {
        @Override
        public void execute(CommandContext context) throws Exception {
            System.out.println("Commands: " + PRODUCT_COMMANDS.keySet().stream().collect(Collectors.joining(", ")) + DISH_COMMANDS.keySet().stream().collect(Collectors.joining(", ")));
        }
    };

    private static final HashMap<String, Function<String, Command>> PRODUCT_COMMANDS = new HashMap<>();
    private static final HashMap<String, Function<String, Command>> DISH_COMMANDS = new HashMap<>();
    private static final HashMap<String, Function<String, Command>> PROFILE_COMMANDS = new HashMap<>();
    private static final HashMap<String, Function<String, Command>> EATING_COMMANDS = new HashMap<>();

    private static final Map<String, Command>  COMMANDS = new HashMap<>();

    static {
        PRODUCT_COMMANDS.put("create product", CreateProductCommand::new);
        PRODUCT_COMMANDS.put("edit product", EditProductCommand::new);
        PRODUCT_COMMANDS.put("delete product", DeleteProductCommand::new);
        PRODUCT_COMMANDS.put("print product", PrintConcreteProductCommand::new);

        DISH_COMMANDS.put("create dish", CreateDishCommand::new);
        DISH_COMMANDS.put("edit dish", EditDishCommand::new);
        DISH_COMMANDS.put("delete dish", DeleteDishCommand::new);
        DISH_COMMANDS.put("print dish", PrintDishCommand::new);
        DISH_COMMANDS.put("delete ingridient dish", DeleteIngridientDishCommand::new);
        DISH_COMMANDS.put("add ingridient dish", AddIngridientDishCommand::new);
        DISH_COMMANDS.put("print dish all parametrs", PrintDishAllParametrsCommand::new);

        PROFILE_COMMANDS.put("create profile", CreateProfileCommand::new);
        PROFILE_COMMANDS.put("print profile", PrintProfileCommand::new);
        PROFILE_COMMANDS.put("delete profile", DeleteProfileCommand::new);
        PROFILE_COMMANDS.put("edit profile", EditProfileCommand::new);
        PROFILE_COMMANDS.put("login profile", LoginProfileCommand::new);

        EATING_COMMANDS.put("add dish", AddDishUserEating::new);
        EATING_COMMANDS.put("open mealtime", OpenMealtimeUserEating::new);
        EATING_COMMANDS.put("remove dish", RemoveDishUserEating::new);
        EATING_COMMANDS.put("create mealtime", CreateMealtimeUserEating::new);

        COMMANDS.put("help", HELP_COMMAND);
        COMMANDS.put("print products",  new PrintProductsCommand());
        COMMANDS.put("print dishes",  new PrintAllDishCommand());
        COMMANDS.put("print profiles",  new PrintAllProfileCommand());
        COMMANDS.put("exit profile", new ExitProfileCommand());
        COMMANDS.put("end", new EndAddDishUserEating());
        COMMANDS.put("print", new PrintMealtimeUserEating());
    }

    public static Command createCommand(String input) throws Exception {
        Command command = COMMANDS.get(input);
        if (command != null) {
            logger.debug("find command: {}.", command);
            return command;
        }

        for (Map.Entry<String, Function<String, Command>> entry : PRODUCT_COMMANDS.entrySet()) {
            if (input.startsWith(entry.getKey())) {
                String suffix = input.substring(entry.getKey().length()).trim();
                command = entry.getValue().apply(suffix);
            }
        }

        for (Map.Entry<String, Function<String, Command>> entry : DISH_COMMANDS.entrySet()) {
            if (input.startsWith(entry.getKey())) {
                String suffix = input.substring(entry.getKey().length()).trim();
                command = entry.getValue().apply(suffix);
            }
        }

        for (Map.Entry<String, Function<String, Command>> entry : PROFILE_COMMANDS.entrySet()) {
            if (input.startsWith(entry.getKey())) {
                String suffix = input.substring(entry.getKey().length()).trim();
                command = entry.getValue().apply(suffix);
            }
        }

        for (Map.Entry<String, Function<String, Command>> entry : EATING_COMMANDS.entrySet()) {
            if (input.startsWith(entry.getKey())) {
                String suffix = input.substring(entry.getKey().length()).trim();
                command = entry.getValue().apply(suffix);
            }
        }

        logger.debug("find command: {}.", command);
        return command;
    }
}
