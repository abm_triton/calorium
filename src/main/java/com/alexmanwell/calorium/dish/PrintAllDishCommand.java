package com.alexmanwell.calorium.dish;

import com.alexmanwell.calorium.Command;
import com.alexmanwell.calorium.CommandContext;
import com.alexmanwell.calorium.Search;

public class PrintAllDishCommand implements Command {
    @Override
    public void execute(CommandContext context) throws Exception {
        Search.printDishTitleTable();
        Search.printAllDish(context.getDishDao().findAllDish());
    }
}
