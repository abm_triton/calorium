package com.alexmanwell.calorium.dish;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface DishDao {

    void insertDish(Dish dish) throws Exception;
    void deleteDish(long dishId) throws Exception;
    void editDish(Dish dish, long dishId) throws Exception;

    Dish searchDish(String dishName) throws Exception;
    Collection<Dish> findAllDish() throws Exception;

    void deleteProductDish(long dishId, List<String> products) throws Exception;
    void addIngridientDish(long dishId, String dishName, Map<String, Float> products) throws Exception;
}
