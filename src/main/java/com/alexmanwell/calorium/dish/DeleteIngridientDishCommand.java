package com.alexmanwell.calorium.dish;

import com.alexmanwell.calorium.Command;
import com.alexmanwell.calorium.CommandContext;
import com.alexmanwell.calorium.Search;

import java.util.List;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

public class DeleteIngridientDishCommand implements Command {

    private final String dishName;

    public DeleteIngridientDishCommand(String dishName) {
        this.dishName = dishName;
    }

    @Override
    public void execute(CommandContext context) throws Exception {
        DishDao dishDao = context.getDishDao();
        Dish dish = dishDao.searchDish(dishName);
        InputDataDish input = new InputDataDish();
        Scanner scanner = new Scanner(System.in);

        if (dish == null) {
            throw new IllegalArgumentException("Продукта с таким названием нету " + dishName);
        }

        Search.printDish(dish);
        List<String> products = new ArrayList<>();
        while (true) {
            System.out.println("Удалить ингридиент? ");
            String end = scanner.next();
            if (Objects.equals(end, "нет")) {
                break;
            }
            String productName = input.writeProductName(scanner);
            products.add(productName);
        }
        dishDao.deleteProductDish(dish.getId(), products);
        Search.printAllDish(dishDao.findAllDish());
    }
}
