package com.alexmanwell.calorium.dish;

import com.alexmanwell.calorium.Command;
import com.alexmanwell.calorium.CommandContext;
import com.alexmanwell.calorium.Search;

public class PrintDishCommand implements Command {

    private final String dishName;

    public PrintDishCommand(String dishName) {
        this.dishName = dishName;
    }

    @Override
    public void execute(CommandContext context) throws Exception {
        Dish dish = context.getDishDao().searchDish(dishName);

        if (dish == null) {
            throw new IllegalArgumentException("Блюда с таким названием нету." + dishName);
        }
        Search.printDishTitleTable();
        Search.printDish(dish);
    }
}
