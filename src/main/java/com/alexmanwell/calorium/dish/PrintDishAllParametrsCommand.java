package com.alexmanwell.calorium.dish;

import com.alexmanwell.calorium.CalculateNutients;
import com.alexmanwell.calorium.Command;
import com.alexmanwell.calorium.CommandContext;
import com.alexmanwell.calorium.Search;
import com.alexmanwell.calorium.product.Product;

import java.util.List;
import java.util.Map;

public class PrintDishAllParametrsCommand implements Command {

    private String dishName;

    public PrintDishAllParametrsCommand(String dishName) {
        this.dishName = dishName;
    }

    @Override
    public void execute(CommandContext context) throws Exception {
        Dish dish = context.getDishDao().searchDish(dishName);
        if (dish == null) {
            throw new IllegalArgumentException("Нету блюда " + dishName);
        }

        Search.printDishTitleTable();
        Search.printDish(dish);

        Map<Product, Float> map = context.getProductDao().getProductsInDish(dish.getId());

        CalculateNutients calculate = new CalculateNutients();
        List<Product> productList = calculate.resultDefaultNutrientsDish(map);

        for (Product product : productList) {
            Search.printProduct(product);
        }
        Search.printDishTotalBJU(productList);
    }
}
