package com.alexmanwell.calorium.dish;

import com.alexmanwell.calorium.product.InputDataProduct;

import java.util.Scanner;

public class InputDataDish {

    public String writeProductName(Scanner scanner) {
        return readString(scanner, "Введите название ингридиента в блюде: ");
    }

    public float writeAmountProduct(Scanner scanner) {
        return InputDataProduct.readFloat(scanner, "Введите количество ингридиента: "); }

    public float writeEditAmountProduct(Scanner scanner, String productName) {
        return readFloat(scanner, "Введите количество ингридиента ", productName + ": ");
    }

    private float readFloat(Scanner scanner, String prompt, String productName) {
        System.out.print(prompt + productName);
        boolean validInput = scanner.hasNextFloat();
        while (!validInput) {
            scanner.next();
            System.out.println("Некорректный ввод данных. Введите заново");
            System.out.print(prompt + productName);
            validInput = scanner.hasNextFloat();
        }
        return scanner.nextFloat();
    }

    private String readString(Scanner scanner, String prompt) {
        System.out.print(prompt);
        boolean validInput = scanner.hasNext();
        while (!validInput) {
            scanner.next();
            System.out.println("Некорректный ввод данных. Введите заново");
            System.out.print(prompt);
            validInput = scanner.hasNext();
        }
        return scanner.next();
    }

}
