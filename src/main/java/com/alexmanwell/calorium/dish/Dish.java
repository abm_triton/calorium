package com.alexmanwell.calorium.dish;

import com.alexmanwell.calorium.product.Product;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Dish {

    private long id;
    private String name;
    private Map<String, Float> products = new HashMap<>();
    private Map<Product, Float> productMap = new HashMap<>();

    public Dish() {
    }

    public Dish(long id, String name, Map<String, Float> products) {
        this.id = id;
        this.name = name.trim();
        this.products = products;
    }

    public Dish(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Dish(String name, Map<String, Float> products) {
        this.name = name;
        this.products = products;
    }

    public Dish(Dish.Builder b) {
        this.id = b.id;
        this.name = b.name;
        this.products = b.products;
        this.productMap = b.productMap;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void  setName(String name) {
        this. name = name;
    }

    public Map<String, Float> getProducts() {
        return products;
    }

    public void setProducts(Map<String, Float> products) {
        this.products = products;
    }

    public void addProducts(String productName, float amountProduct) {
        this.products.put(productName, amountProduct);
    }

    public void setProductMap(Product product, float amountProduct) {
        this.productMap.put(product, amountProduct);
    }

    public Map<Product, Float> getProductMap() {
        return this.productMap;
    }

    public Product getProduct(String productName) {
        Product product = null;
        for (Map.Entry<Product, Float> map : this.productMap.entrySet()) {
            if (Objects.equals(productName, map.getKey().getName())) {
                product = map.getKey();
            }
        }
        return product;
    }

    public static class Builder {
        private long id;
        private String name;
        private Map<String, Float> products = new HashMap<>();
        private Map<Product, Float> productMap = new HashMap<>();

        public Builder() {
        }

        public Builder(Dish dish) {
            this.id = dish.getId();
            this.name = dish.getName();
            this.products = dish.getProducts();
        }

        public Builder setId(long id) {
            this.id = id;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder addProducts(String productName, float productAmount) {
            this.products.put(productName, productAmount);
            return this;
        }

        public Builder setProductMap(Product product, float productAmount) {
            this.productMap.put(product, productAmount);
            return this;
        }

        public Dish build() {
            return new Dish(this);
        }
    }

    @Override
    public String toString() {
        return String.format("(dishId = %d, name = %s, products = %s, productMap = %s)", id, name, products, productMap);
    }
}
