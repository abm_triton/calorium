package com.alexmanwell.calorium.dish;

import com.alexmanwell.calorium.Command;
import com.alexmanwell.calorium.CommandContext;
import com.alexmanwell.calorium.Search;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;

public class AddIngridientDishCommand implements Command {

    final static Logger logger = LoggerFactory.getLogger(AddIngridientDishCommand.class);

    private String dishName;

    public AddIngridientDishCommand(String dishName) {
        this.dishName = dishName;
    }

    @Override
    public void execute(CommandContext context) throws Exception {
        logger.debug("begin execute add ingridient in dish {}.", context);
        DishDao dishDao = context.getDishDao();
        Dish dish = dishDao.searchDish(dishName);
        InputDataDish input = new InputDataDish();
        Scanner scanner = new Scanner(System.in);

        if (dish == null) {
            throw new IllegalArgumentException("Такого блюдо нет в базе: " + dishName);
        }

        Map<String, Float> products = new HashMap<>();
        while (true) {
            System.out.println("Добавить новый ингридиент? ");
            String end = scanner.next();
            if (Objects.equals(end, "нет")) {
                break;
            }
            String productName = input.writeProductName(scanner);
            float amountProduct = input.writeAmountProduct(scanner);
            products.put(productName, amountProduct);
        }
        dishDao.addIngridientDish(dish.getId(), dishName, products);

        for (Map.Entry<String, Float> product : dish.getProducts().entrySet()) {
            products.put(product.getKey(), product.getValue());
        }
        dish = new Dish(dish.getId(), dish.getName(), products);
        Search.printDishTitleTable();
        Search.printDish(dish);
    }
}
