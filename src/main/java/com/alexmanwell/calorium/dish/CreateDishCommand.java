package com.alexmanwell.calorium.dish;

import com.alexmanwell.calorium.Command;
import com.alexmanwell.calorium.CommandContext;
import com.alexmanwell.calorium.Search;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;

public class CreateDishCommand implements Command {

    private final static Logger logger = LoggerFactory.getLogger(CreateDishCommand.class);

    private String dishName;

    public CreateDishCommand(String dishName) {
        this.dishName = dishName;
    }

    @Override
    public void execute(CommandContext context) throws Exception {
        logger.debug("begin execute create dish {}.", context);
        DishDao dishDao = context.getDishDao();
        Dish dish = dishDao.searchDish(dishName);
        InputDataDish input = new InputDataDish();
        Scanner scanner = new Scanner(System.in);

        logger.debug("dish {} = ", dish);

        if (dish != null) {
            throw new IllegalArgumentException("Такое блюдо уже есть в базе: " + dishName);
        }

        Map<String, Float> products = new HashMap<>();
        while (true) {
            System.out.println("Добавить новый ингридиент? ");
            String end = scanner.next();
            if (Objects.equals(end, "нет")) {
                break;
            }
            String productName = input.writeProductName(scanner);
            float amountProduct = input.writeAmountProduct(scanner);
            products.put(productName, amountProduct);
        }
        dish = new Dish(dishName, products);
        dishDao.insertDish(dish);
        Search.printDishTitleTable();
        Search.printDish(dish);
    }
}
