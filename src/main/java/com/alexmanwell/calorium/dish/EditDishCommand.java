package com.alexmanwell.calorium.dish;

import com.alexmanwell.calorium.Command;
import com.alexmanwell.calorium.CommandContext;
import com.alexmanwell.calorium.Search;
import com.alexmanwell.calorium.product.EditProductCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class EditDishCommand implements Command {

    private final static Logger logger = LoggerFactory.getLogger(EditProductCommand.class);
    private String dishName;

    public EditDishCommand(String dishName) {
        this.dishName = dishName;
    }

    @Override
    public void execute(CommandContext context) throws Exception {
        logger.debug("begin edit dish");
        DishDao dishDao = context.getDishDao();
        Dish dish = dishDao.searchDish(dishName);
        logger.debug(" {}.", dish);
        InputDataDish input = new InputDataDish();
        Scanner scanner = new Scanner(System.in);
        if (dish == null) {
            throw new IllegalArgumentException("Такого продукта нет в базе " + dishName);
        }

        Map<String, Float> products = new HashMap<>();
        for (Map.Entry<String, Float> product : dish.getProducts().entrySet()) {
            float value = input.writeEditAmountProduct(scanner, product.getKey());
            products.put(product.getKey(), value);
        }

        dish = new Dish(dish.getId(), dishName, products);
        dishDao.editDish(dish, dish.getId());
        Search.printTitleTable();
        Search.printDish(dish);

        logger.debug("end edit dish {}.", dish);
    }
}
