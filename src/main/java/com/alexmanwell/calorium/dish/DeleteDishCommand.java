package com.alexmanwell.calorium.dish;

import com.alexmanwell.calorium.Command;
import com.alexmanwell.calorium.CommandContext;

public class DeleteDishCommand implements Command {

    private String dishName;

    public DeleteDishCommand(String dishName) {
        this.dishName = dishName;
    }

    @Override
    public void execute(CommandContext context) throws Exception {
        DishDao dishDao = context.getDishDao();
        Dish dish = dishDao.searchDish(dishName);

        if (dish == null) {
            throw new IllegalArgumentException("Блюда с таким названием нету " + dishName);
        }

        dishDao.deleteDish(dish.getId());
        dishDao.findAllDish();

    }
}
