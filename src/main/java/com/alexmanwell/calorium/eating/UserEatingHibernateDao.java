package com.alexmanwell.calorium.eating;

import com.alexmanwell.calorium.HibernateUtils;
import com.alexmanwell.calorium.dish.Dish;
import com.alexmanwell.calorium.profile.Profile;
import org.hibernate.Session;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class UserEatingHibernateDao implements UserEatingDao {
    @Override
    public void addEating(UserEating userEating) throws Exception {

    }

    @Override
    public void addDishEating(UserEating userEating) throws Exception {

    }

    @Override
    public void removeEating(String nickname) throws Exception {

    }

    @Override
    public void editEating(Profile profile) throws Exception {

    }

    @Override
    public UserEating searchUserEating(long profile, Date date, String mealtime) throws Exception {
        return null;
    }

    @Override
    public List<UserEating> searchUserEatingToday(long profileId, Date date) throws Exception {
        return null;
    }

    @Override
    public UserEating selectMealtime(UserEating ue) throws Exception {
        try (final Session session = HibernateUtils.getSessionFactory().openSession()) {
            session.getTransaction().begin();
            final UserEating userEating = session.get(UserEating.class, ue.getEatingId());

            if (userEating == null) {
                throw new IllegalStateException("Такого приема пищи нет: " + ue);
            }
            session.getTransaction().commit();
            return userEating;
        }
    }

    @Override
    public Set<String> mealtimes() throws Exception {
        return null;
    }

    @Override
    public void removeDishEating(UserEating userEating) throws Exception {

    }

    @Override
    public void deleteMealtimeEating(long eatingId) throws Exception {
        try ( final Session session = HibernateUtils.getSessionFactory().openSession()) {
            session.getTransaction().begin();
            session.delete(new UserEating(eatingId));
            session.getTransaction().commit();
        }
    }

    @Override
    public UserEating createMealtime(UserEating ue) throws Exception {
        try (final Session session = HibernateUtils.getSessionFactory().openSession()) {
            session.getTransaction().begin();
            session.save(ue);
            session.getTransaction().commit();
            return ue;
        }
    }

    @Override
    public void editEating(long eatingId, Map<Dish, Float> dishes) throws Exception {
    }

    @Override
    public void removeDishEating(long dishEatingId) throws Exception {

    }

    @Override
    public UserEating selectMealtime(long eatingId) throws Exception {
        return null;
    }
}
