package com.alexmanwell.calorium.eating;

import com.alexmanwell.calorium.JdbcUtils;
import com.alexmanwell.calorium.dish.Dish;
import com.alexmanwell.calorium.disheating.UserDishEating;
import com.alexmanwell.calorium.product.ElementType;
import com.alexmanwell.calorium.product.Product;
import com.alexmanwell.calorium.product.VitaminType;
import com.alexmanwell.calorium.profile.Profile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.*;
import java.util.Date;

public class UserEatingJdbcDao implements UserEatingDao {

    final static Logger logger = LoggerFactory.getLogger(UserEatingJdbcDao.class);

    private Connection connection;

    public UserEatingJdbcDao(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void addEating(UserEating userEating) throws Exception {
        logger.debug("begin add dish and amount in mealtime {} {}", userEating.getMealtime(), userEating.getDate());
        try (
                PreparedStatement stmtDishEating = connection.prepareStatement("INSERT INTO user_dish_eating (eating_id, dish_id, amount_dish) VALUES (?, ?, ?)");
        ) {
            for (Map.Entry<Dish, Float> entry : userEating.getUserDish().entrySet()) {
                long dishId = entry.getKey().getId();
                float dishAmount = entry.getValue();
                stmtDishEating.setLong(1, userEating.getEatingId());
                stmtDishEating.setLong(2, dishId);
                stmtDishEating.setFloat(3, dishAmount);
                stmtDishEating.executeUpdate();
            }
        }
        logger.debug("end add dish and amount in mealtime {} {}", userEating.getMealtime(), userEating.getDate());
    }

    @Override
    public void addDishEating(UserEating userEating) throws Exception {
        try (
                PreparedStatement stmtDishEating = connection.prepareStatement("INSERT INTO user_dish_eating (eating_id, dish_id, amount_dish) VALUES (?, ?, ?)");
        ) {
            for (Map.Entry<Dish, Float> entry : userEating.getUserDish().entrySet()) {
                long dishId = entry.getKey().getId();
                float dishAmount = entry.getValue();
                stmtDishEating.setLong(1, userEating.getEatingId());
                stmtDishEating.setLong(2, dishId);
                stmtDishEating.setFloat(3, dishAmount);
                stmtDishEating.getGeneratedKeys();
                stmtDishEating.executeUpdate();
            }
        }
    }

    @Override
    public void removeEating(String nickname) throws Exception {

    }

    @Override
    public void editEating(Profile profile) throws Exception {

    }

    @Override
    public UserEating searchUserEating(long profileId, Date date, String mealtime) throws Exception {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.prepareStatement("SELECT * FROM user_eating ue LEFT JOIN user_dish_eating ude ON ue.eating_id = ude.eating_id LEFT JOIN dish d ON ude.dish_id = d.dish_id WHERE ue.user_id = ? AND ue.mealtime = ? AND ue.date_eating BETWEEN ? AND ?");
            stmt.setLong(1, profileId);
            stmt.setString(2, mealtime);
            stmt.setTimestamp(3, makeBeginTS(date));
            stmt.setTimestamp(4, makeEndTS(date));
            rs = stmt.executeQuery();

            UserEating.Builder builder = new UserEating.Builder();
            while (rs.next()) {
                builder.setProfileId(rs.getLong("user_id"));
                builder.setEatingId(rs.getLong("eating_id"));
                builder.setDateEating(rs.getTimestamp("date_eating"));
                builder.setMealtime(rs.getString("mealtime"));

                long dishId = rs.getLong("dish_id");
                String dishName = rs.getString("name");
                Dish dish = new Dish(dishId, dishName);
                float dishAmount = rs.getFloat("amount_dish");
                builder.addDish(dish, dishAmount);

                Map<Dish, Float> dishEatings = new HashMap<>();
                dishEatings.put(dish, dishAmount);
                builder.setDish(rs.getLong("dish_eating_id"), dishEatings);
            }

            UserEating userEating = builder.build();
            logger.debug("end search userEating {}", userEating);
            return userEating;
        } finally {
            JdbcUtils.closeSilently(rs);
            JdbcUtils.closeSilently(stmt);
        }
    }

    @Override
    public List<UserEating> searchUserEatingToday(long profileId, Date date) throws Exception {
        logger.info("begin search userEating mealtime today. profileId: {}, date: {}", profileId, date);
        try (PreparedStatement stmt = connection.prepareStatement("SELECT * FROM user_eating ue LEFT JOIN user_dish_eating ude ON ue.eating_id = ude.eating_id LEFT JOIN dish d ON ude.dish_id = d.dish_id LEFT JOIN dish_products dp ON dp.dish_id = ude.dish_id LEFT JOIN product p ON dp.product_name = p.name LEFT JOIN product_fat pf ON p.name = pf.name LEFT JOIN product_element pe ON p.name = pe.product_name LEFT JOIN product_vitamin pv ON p.name = pv.name LEFT JOIN product_group pg ON p.name = pg.product_name LEFT JOIN product_image pi ON p.name = pi.name LEFT JOIN product_description pd ON p.name = pd.product_name WHERE ue.user_id = ? AND ue.date_eating BETWEEN ? AND ?");) {
            stmt.setLong(1, profileId);
            stmt.setTimestamp(2, makeBeginTS(date));
            stmt.setTimestamp(3, makeEndTS(date));

            try (ResultSet rs = stmt.executeQuery();) {
                Map<Long, UserEating> userEatings = new HashMap<>();
                Map<Long, UserDishEating> dishEating = new HashMap<>();
                while (rs.next()) {
                    UserEating.Builder builder = new UserEating.Builder();
                    builder.setProfileId(rs.getLong("user_id")).setDateEating(rs.getTimestamp("date_eating")).setMealtime(rs.getString("mealtime"));

                    long eatingId = rs.getLong("eating_id");
                    builder.setEatingId(eatingId);
                    UserEating userEating = userEatings.get(eatingId);
                    if (userEating == null) {
                        userEating = builder.build();
                    }
                    builder = new UserEating.Builder(userEating);

                    long dishEatingId = rs.getLong("dish_eating_id");
                    UserDishEating ude = dishEating.get(dishEatingId);
                    Dish dish;
                    if (ude != null) {
                        dish = ude.getDish();
                    } else {
                        long dishId = rs.getLong("dish_id");
                        dish = getDish(rs, dishId);
                        ude = getUserDishEating(rs, eatingId, dishEatingId, dish, dishId);
                        builder.setDishEating(ude);
                    }

                    String productName = rs.getString("product_name");
                    if (!dish.getProducts().containsKey(productName)) {
                        float amountProduct = rs.getFloat("amount_product");
                        Product product = getProduct(rs, productName);
                        dish.setProductMap(product, amountProduct);
                        dish.addProducts(productName, amountProduct);
                    }

                    Product product = dish.getProduct(productName);
                    String elementName = rs.getString("element_name");
                    if (elementName != null) {
                        float amountElem = rs.getFloat("amount_elem");
                        product.putElements(ElementType.getElementType(elementName.trim()), amountElem);
                    }

                    dishEating.put(dishEatingId, ude);
                    userEating = builder.build();
                    userEatings.put(eatingId, userEating);
                }

                List<UserEating> eatings = new ArrayList<>();
                for (Map.Entry<Long, UserEating> pair : userEatings.entrySet()) {
                    eatings.add(pair.getValue());
                }
                logger.debug("end search userEating mealtime today {}", eatings);
                return eatings;
            }
        }
    }

    private UserDishEating getUserDishEating(ResultSet rs, long eatingId, long dishEatingId, Dish dish, long dishId) throws Exception {
        UserDishEating.Builder b = new UserDishEating.Builder();
        b.setDish(dish).setDishId(dishId).setEatingId(eatingId).setAmountDish(rs.getFloat("amount_dish")).setDishEatingId(dishEatingId);
        return b.build();
    }

    private Dish getDish(ResultSet rs, long dishId) throws Exception {
        Dish.Builder b = new Dish.Builder();
        b.setId(dishId).setName(rs.getString("name"));
        return b.build();
    }

    private Product getProduct(ResultSet rs, String productName) throws Exception {
        int kcal = rs.getInt("kcal");
        float protein = rs.getFloat("protein");
        float carbohydrate = rs.getFloat("carbohydrate");
        float vegetableFat = rs.getFloat("vegetable_fat");
        float animalFat = rs.getFloat("animal_fat");
        String groupName = rs.getString("group_name");
        String description = rs.getString("description");
        String path = rs.getString("path");

        Product.Builder b = new Product.Builder();
        b.setName(productName).setKcal(kcal).setProtein(protein).setCarbohydrate(carbohydrate).setVFat(vegetableFat).setAFat(animalFat).setGroup(groupName).setDescription(description).setImageName(path);
        for (VitaminType vt : VitaminType.values()) {
            float vv = rs.getFloat(vt.name());
            b.addVitamin(vt, vv);
        }
        return b.build();
    }

    @Override
    public UserEating selectMealtime(UserEating ue) throws Exception {
        logger.debug("begin select mealtime {}", ue);
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.prepareStatement("SELECT * FROM user_eating WHERE user_id = ? AND mealtime = ? AND date_eating = ?");
            stmt.setLong(1, ue.getProfileId());
            stmt.setString(2, ue.getMealtime());
            stmt.setTimestamp(3, new Timestamp(ue.getDate().getTime()));
            rs = stmt.executeQuery();

            UserEating.Builder builder = new UserEating.Builder();
            while (rs.next()) {
                builder.setProfileId(rs.getLong("user_id"));
                builder.setEatingId(rs.getLong("eating_id"));
                builder.setDateEating(rs.getTimestamp("date_eating"));
                builder.setMealtime(rs.getString("mealtime"));
            }
            UserEating userEating = builder.build();
            if (userEating.getProfileId() == 0 && userEating.getDate() == null && userEating.getMealtime() == null) {
                return null;
            }

            logger.debug("end select mealtime {}", userEating);
            return userEating;
        } finally {
            JdbcUtils.closeSilently(rs);
            JdbcUtils.closeSilently(stmt);
        }

    }

    @Override
    public Set<String> mealtimes() throws Exception {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.prepareStatement("SELECT mealtime FROM mealtime");
            rs = stmt.executeQuery();

            Set<String> set = new HashSet<>();
            while (rs.next()) {
                String mealtime = rs.getString("mealtime");
                set.add(mealtime);
            }

            return set;
        } finally {
            JdbcUtils.closeSilently(rs);
            JdbcUtils.closeSilently(stmt);
        }
    }

    @Override
    public void removeDishEating(UserEating userEating) throws Exception {
        logger.debug("begin remove dish in mealtime {}", userEating);
        PreparedStatement stmt = null;
        ResultSet rs = null;
        PreparedStatement stmtRemoveDish = null;
        PreparedStatement stmtRemoveUserEating = null;

        try {
            long idDish = 0;
            float amountDish = 0;
            for (Map.Entry<Dish, Float> entry : userEating.getUserDish().entrySet()) {
                idDish = entry.getKey().getId();
                amountDish = entry.getValue();
            }

            stmt = connection.prepareStatement("SELECT ude.eating_id FROM user_eating ue LEFT JOIN user_dish_eating ude ON ue.eating_id = ude.eating_id WHERE ue.user_id = ? AND ue.mealtime = ? AND ue.date_eating = ? AND ude.dish_id = ? AND ude.amount_dish = ?");
            stmt.setLong(1, userEating.getProfileId());
            stmt.setString(2, userEating.getMealtime());
            stmt.setTimestamp(3, new Timestamp(userEating.getDate().getTime()));
            stmt.setLong(4, idDish);
            stmt.setFloat(5, amountDish);
            logger.debug("userEating = {}, idDish = {}, amountDish = {}", userEating, idDish, amountDish);
            rs = stmt.executeQuery();

            long eatingId = 0;
            while (rs.next()) {
                eatingId = rs.getLong("eating_id");
                logger.debug("eatingId = {}", eatingId);
            }

            stmtRemoveDish = connection.prepareStatement("DELETE FROM user_dish_eating WHERE eating_id = ?");
            stmtRemoveDish.setLong(1, eatingId);
            stmtRemoveDish.executeUpdate();
            logger.debug("remove row user_dish_eating with eatingId = {}", eatingId);

            stmtRemoveUserEating = connection.prepareStatement("DELETE FROM user_eating WHERE eating_id = ?");
            stmtRemoveUserEating.setLong(1, eatingId);
            stmtRemoveUserEating.executeUpdate();
            logger.debug("remove row user_eating with eatingId = {}", eatingId);

        } finally {
            JdbcUtils.closeSilently(rs);
            JdbcUtils.closeSilently(stmt);
            JdbcUtils.closeSilently(stmtRemoveDish);
            JdbcUtils.closeSilently(stmtRemoveUserEating);
        }
        logger.debug("end remove dish in mealtime {}", userEating);
    }

    @Override
    public void deleteMealtimeEating(long eatingId) throws Exception {
        logger.debug("begin delete mealtime in {}", eatingId);
        PreparedStatement stmtUserDishEating = null;
        PreparedStatement userEating = null;
        try {
            stmtUserDishEating = connection.prepareStatement("DELETE FROM user_dish_eating WHERE eating_id = ?");
            stmtUserDishEating.setLong(1, eatingId);
            stmtUserDishEating.executeUpdate();

            userEating = connection.prepareStatement("DELETE FROM user_eating WHERE eating_id = ?");
            userEating.setLong(1, eatingId);
            userEating.executeUpdate();
        } finally {
            JdbcUtils.closeSilently(stmtUserDishEating);
            JdbcUtils.closeSilently(userEating);
        }
        logger.debug("end delete mealtime in {}", eatingId);
    }

    @Override
    public UserEating createMealtime(UserEating ue) throws Exception {
        logger.debug("begin insert in user_eating table new data = {}", ue);
        try (
                PreparedStatement stmtEating = connection.prepareStatement("INSERT INTO user_eating (user_id, date_eating, mealtime) VALUES (?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
        ) {
            stmtEating.setLong(1, ue.getProfileId());
            stmtEating.setTimestamp(2, new Timestamp(ue.getDate().getTime()));
            stmtEating.setString(3, ue.getMealtime());
            stmtEating.executeUpdate();

            ResultSet generateKeys = stmtEating.getGeneratedKeys();
            generateKeys.next();
            long eatingId = generateKeys.getLong("eating_id");

            JdbcUtils.closeSilently(generateKeys);

            UserEating.Builder b = new UserEating.Builder(ue);
            b.setEatingId(eatingId);
            UserEating newUE = b.build();
            logger.debug("end insert in user_eating table new data = {}", newUE);
            return newUE;
        }
    }

    @Override
    public void editEating(long eatingId, Map<Dish, Float> dishes) throws Exception {
        logger.debug("begin edit dishes in mealtime {}", eatingId);
        try (
                PreparedStatement stmt = connection.prepareStatement("UPDATE user_dish_eating SET amount_dish = ? WHERE eating_id = ? AND dish_id = ?");
        ) {
            for (Map.Entry<Dish, Float> dish : dishes.entrySet()) {
                stmt.setFloat(1, dish.getValue());
                stmt.setLong(2, eatingId);
                stmt.setLong(3, dish.getKey().getId());
                stmt.executeUpdate();
            }
        }
    }

    @Override
    public void removeDishEating(long dishEatingId) throws Exception {
        logger.debug("begin remove dish in mealtime in {}", dishEatingId);
        try (PreparedStatement stmt = connection.prepareStatement("DELETE FROM user_dish_eating WHERE dish_eating_id = ?")) {
            stmt.setLong(1, dishEatingId);
            stmt.executeUpdate();
        }
        logger.debug("begin remove dish in mealtime in {}", dishEatingId);
    }

    @Override
    public UserEating selectMealtime(long eatingId) throws Exception {
        logger.debug("begin select mealtime {}", eatingId);
        try (final PreparedStatement stmt = connection.prepareStatement("SELECT * FROM user_eating ue LEFT JOIN user_dish_eating ude ON ue.eating_id = ude.eating_id LEFT JOIN dish d ON ude.dish_id = d.dish_id WHERE ue.eating_id = ?")) {
            stmt.setLong(1, eatingId);

            try (final ResultSet rs = stmt.executeQuery()) {
                UserEating.Builder builder = new UserEating.Builder();
                while (rs.next()) {
                    builder.setProfileId(rs.getLong("user_id"));
                    builder.setEatingId(rs.getLong("eating_id"));
                    builder.setDateEating(rs.getTimestamp("date_eating"));
                    builder.setMealtime(rs.getString("mealtime"));

                    long dishEatingId = rs.getLong("dish_eating_id");
                    long dishId = rs.getLong("dish_id");
                    String dishName = rs.getString("name");
                    Dish dish = new Dish(dishId, dishName);
                    float dishAmount = rs.getFloat("amount_dish");
                    builder.addDish(dish, dishAmount);

                    Map<Dish, Float> dishEatings = new HashMap<>();
                    dishEatings.put(dish, dishAmount);
                    builder.setDish(dishEatingId, dishEatings);
                    builder.setDishEating(new UserDishEating(dishEatingId, eatingId, dishId, dishName, dishAmount));
                }

                UserEating userEating = builder.build();
                logger.debug("end select mealtime {}", userEating);
                return userEating;
            }
        }
    }

    private Timestamp makeBeginTS(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.AM_PM, 0);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        Date begin = calendar.getTime();
        logger.debug("begin date: {}", begin);
        return new Timestamp(begin.getTime());
    }

    private Timestamp makeEndTS(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.AM_PM, 0);
        calendar.set(Calendar.HOUR, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 59);

        Date end = calendar.getTime();
        logger.debug("end date: {}", end);
        return new Timestamp(end.getTime());
    }
}
