package com.alexmanwell.calorium.eating;

import com.alexmanwell.calorium.CommandContext;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.MatchResult;

public class ParseUtils {
    static String parseDishName(String input) throws Exception {
        Scanner s = new Scanner(input);
        s.findInLine("(\\D+)");
        MatchResult result = s.match();
        String dishName = result.group().trim();
        s.close();
        return dishName;
    }

    static float parseDishAmount(String input) throws Exception {
        Scanner s = new Scanner(input);
        s.findInLine("(\\d+)");
        MatchResult result = s.match();
        float dishAmount = Float.parseFloat(result.group().trim());
        s.close();
        return dishAmount;
    }

    static String parseMealtime(String input, CommandContext context) throws Exception {
        Scanner s = new Scanner(input);
        s.findInLine("(\\D+)");
        MatchResult result = s.match();
        String mealtime = result.group().trim();
        s.close();

        Set<String> mealtimes = context.getUserEatingDao().mealtimes();
        if (!mealtimes.contains(mealtime)) {
            throw new IllegalArgumentException("Неверно назван прием пищи " + mealtime);
        }

        return mealtime;
    }

    //TODO Исправить parseDate, чтоб корректно работад
    public static Date parseDate(String input) throws Exception {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = dateFormat.parse(input);
        dateFormat.setLenient(input.matches(dateFormat.toPattern()));
        return date;
    }
}
