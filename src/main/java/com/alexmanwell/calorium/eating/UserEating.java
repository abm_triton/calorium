package com.alexmanwell.calorium.eating;

import com.alexmanwell.calorium.dish.Dish;
import com.alexmanwell.calorium.disheating.UserDishEating;

import java.util.*;

public class UserEating {

    private long profileId;
    private long eatingId;
    private Date date;
    private String mealtime;
    private Map<Dish, Float> userDish;
    private Map<Long, Map<Dish, Float>> dishEating;
    private List<UserDishEating> userDishEatings;

    public UserEating() {
    }

    public UserEating(UserEating.Builder ueb) {
        this.profileId = ueb.profileId;
        this.eatingId = ueb.eatingId;
        this.date = ueb.dateEating;
        this.mealtime = ueb.mealtime;
        this.userDish = ueb.userDish;
        this.userDishEatings = ueb.userDishEatings;
        this.dishEating = ueb.dishEating;
    }

    public UserEating(long profileId, Date date, String mealtime, Map<Dish, Float> userDish) {
        this.profileId = profileId;
        this.date = date;
        this.mealtime = mealtime;
        this.userDish = userDish;
    }

    public UserEating(long profileId, String mealtime, Map<Dish, Float> userDish) {
        this.profileId = profileId;
        this.mealtime = mealtime;
        this.userDish = userDish;
    }

    public UserEating(long eatingId) {
        this.eatingId = eatingId;
    }

    public long getProfileId() {
        return profileId;
    }

    public void setProfileId(long profileId) {
        this.profileId = profileId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getMealtime() {
        return mealtime;
    }

    public void setMealtime(String mealtime) {

        this.mealtime = mealtime;
    }

    public Map<Dish, Float> getUserDish() {
        return userDish;
    }

    public void setUserDish(Map<Dish, Float> userDish) {
        this.userDish = userDish;
    }


    @Override
    public String toString() {
        return String.format("(profileId = %d, eatingId = %s, date = %s, mealtime = %s, dishEating = %s, userDishEatings = %s)", profileId, eatingId, date, mealtime, userDish, userDishEatings);
    }

    public long getEatingId() {
        return eatingId;
    }

    public void setEatingId(long eatingId) {
        this.eatingId = eatingId;
    }

    public List<UserDishEating> getUserDishEatings() {
        return userDishEatings;
    }

    public void setUserDishEatings(List<UserDishEating> userDishEating) {
        this.userDishEatings = userDishEating;
    }

    public Map<Long, Map<Dish, Float>> getDishEating() {
        return dishEating;
    }

    public void setDishEating(Map<Long, Map<Dish, Float>> dishEating) {
        this.dishEating = dishEating;
    }

    public static class Builder {
        private long profileId;
        private long eatingId;
        private Date dateEating;
        private String mealtime;
        private Map<Dish, Float> userDish = new HashMap<>();
        private List<UserDishEating> userDishEatings = new ArrayList<>();
        private Map<Long, Map<Dish, Float>> dishEating = new HashMap<>();;

        public Builder() {
        }

        public Builder(UserEating ue) {
            this.profileId = ue.getProfileId();
            this.eatingId = ue.getEatingId();
            this.dateEating = ue.getDate();
            this.mealtime = ue.getMealtime();
            this.userDish = ue.getUserDish();
            this.userDishEatings = ue.getUserDishEatings();
            this.dishEating = ue.getDishEating();
        }

        public Builder setProfileId(long profileId) {
            this.profileId = profileId;
            return this;
        }

        public Builder setEatingId(long eatingId) {
            this.eatingId = eatingId;
            return this;
        }

        public Builder setDateEating(Date dateEating) {
            this.dateEating = dateEating;
            return this;
        }

        public Builder setMealtime(String mealtime) {
            this.mealtime = mealtime;
            return this;
        }

        public Builder addDish(Dish dish, float amountDish) {
            this.userDish.put(dish, amountDish);
            return this;
        }

        public Builder setDishEating(UserDishEating userDishEating) {
            this.userDishEatings.add(userDishEating);
            return this;
        }

        public Builder setDish(long dishEatingId, Map<Dish, Float> userDish) {
            this.dishEating.put(dishEatingId, userDish);
            return this;
        }

        public UserEating build() {
            return new UserEating(this);
        }
    }
}
