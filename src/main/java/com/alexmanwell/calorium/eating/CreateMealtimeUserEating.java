package com.alexmanwell.calorium.eating;

import com.alexmanwell.calorium.Command;
import com.alexmanwell.calorium.CommandContext;
import com.alexmanwell.calorium.profile.Profile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

public class CreateMealtimeUserEating implements Command {
    private final static Logger logger = LoggerFactory.getLogger(CreateMealtimeUserEating.class);

    private String input;

    public CreateMealtimeUserEating(String input) {
        this.input = input;
    }

    @Override
    public void execute(CommandContext context) throws Exception {
        // TODO Разобратся с датой, очень не адекватно создает время в дате.
        Profile profile = context.getProfile();
        logger.debug("begin create mealtime {} for user {}", input, profile);

        if (profile == null) {
            throw new IllegalArgumentException("Вы не залогинились.");
        }

        String mealtime = ParseUtils.parseMealtime(input, context);
        Date date = ParseUtils.parseDate(input.replace(mealtime, "").trim());

        UserEating.Builder b = new UserEating.Builder();
        b.setProfileId(profile.getId());
        b.setDateEating(date);
        b.setMealtime(mealtime);
        UserEating userEating = b.build();

        UserEatingDao userEatingDao = context.getUserEatingDao();
        userEating = userEatingDao.selectMealtime(userEating);
        if (userEating == null) {
            userEating = b.build();
            userEating = userEatingDao.createMealtime(userEating);
        } else {
            logger.info("Такой прием пищи уже существует");
        }
        context.setUserEating(userEating);

        logger.debug("end create mealtime {} for user {}", userEating, profile.getNickname());
    }
}
