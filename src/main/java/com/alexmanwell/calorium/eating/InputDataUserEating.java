package com.alexmanwell.calorium.eating;

import java.util.Scanner;

public class InputDataUserEating {
    public String readMealtime(Scanner scanner) {
        System.out.println("Введите название приема пищи: ");
        return scanner.next();
    }

    public String readDishName(Scanner scanner) {
        System.out.println("Введите блюдо: ");
        return scanner.next();
    }

    public float readDishAmount(Scanner scanner) {
        return readFloat(scanner, "Введите количество съеденной пищи: ");
    }

    public static float readFloat(Scanner scanner, String prompt) {
        System.out.print(prompt);
        boolean validInput = scanner.hasNextFloat();
        while (!validInput) {
            scanner.next();
            System.out.println("Некорректный ввод данных. Введите заново");
            System.out.print(prompt);
            validInput = scanner.hasNextFloat();
        }
        return scanner.nextFloat();
    }
}
