package com.alexmanwell.calorium.eating;

import com.alexmanwell.calorium.dish.Dish;
import com.alexmanwell.calorium.profile.Profile;

import java.util.*;

public class UserEatingInMemoryDao implements UserEatingDao {
    @Override
    public void addEating(UserEating userEating) throws Exception {

    }

    @Override
    public void addDishEating(UserEating userEating) throws Exception {

    }

    @Override
    public void removeEating(String nickname) throws Exception {

    }

    @Override
    public void editEating(Profile profile) throws Exception {

    }

    @Override
    public UserEating searchUserEating(long profile, Date date, String mealtime) throws Exception {
        return null;
    }

    @Override
    public List<UserEating> searchUserEatingToday(long profile, Date date) throws Exception {
        return null;
    }

    @Override
    public UserEating selectMealtime(UserEating ue) throws Exception {
        return null;
    }

    @Override
    public Set<String> mealtimes() throws Exception {
        Set<String> mealtimes = new HashSet<String>();
        mealtimes.add("Завтрак");
        mealtimes.add("Обед");
        mealtimes.add("Ужин");
        return mealtimes;
    }

    @Override
    public void removeDishEating(UserEating userEating) throws Exception {

    }

    @Override
    public void deleteMealtimeEating(long eatingId) throws Exception {

    }

    @Override
    public UserEating createMealtime(UserEating ue) throws Exception {
        return null;
    }

    @Override
    public void editEating(long eatingId, Map<Dish, Float> dishes) {
    }

    @Override
    public void removeDishEating(long dishEatingId) throws Exception {

    }

    @Override
    public UserEating selectMealtime(long eatingId) throws Exception {
        return null;
    }
}
