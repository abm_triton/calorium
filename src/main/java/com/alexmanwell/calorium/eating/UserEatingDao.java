package com.alexmanwell.calorium.eating;

import com.alexmanwell.calorium.dish.Dish;
import com.alexmanwell.calorium.profile.Profile;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface UserEatingDao {
    void addEating(UserEating userEating) throws Exception;
    void addDishEating(UserEating userEating) throws Exception;

    void removeEating(String nickname) throws Exception;
    void editEating(Profile profile) throws Exception;
    UserEating searchUserEating(long profile, Date date, String mealtime) throws Exception;
    List<UserEating> searchUserEatingToday(long profileId, Date date) throws Exception;
    UserEating selectMealtime(UserEating ue) throws Exception;

    Set<String> mealtimes() throws Exception;

    void removeDishEating(UserEating userEating) throws Exception;
    void deleteMealtimeEating(long eatingId) throws Exception;
    UserEating createMealtime(UserEating ue) throws Exception;

    void editEating(long eatingId, Map<Dish, Float> dishes) throws Exception;

    void removeDishEating(long dishEatingId) throws Exception;

    UserEating selectMealtime(long eatingId) throws Exception;
}
