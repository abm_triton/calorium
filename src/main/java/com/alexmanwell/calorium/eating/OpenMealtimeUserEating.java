package com.alexmanwell.calorium.eating;

import com.alexmanwell.calorium.Command;
import com.alexmanwell.calorium.CommandContext;
import com.alexmanwell.calorium.Search;
import com.alexmanwell.calorium.profile.Profile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

public class OpenMealtimeUserEating implements Command {

    private final static Logger logger = LoggerFactory.getLogger(OpenMealtimeUserEating.class);

    private final String input;

    public OpenMealtimeUserEating(String mealtime) {
        this.input = mealtime;
    }

    @Override
    public void execute(CommandContext context) throws Exception {
        //TODO: Команды писать более человеческим языком ( например, дату указывать: вчера, неделю назад.)
        logger.debug("open mealtime {} for user {}", input, context.getProfile().getNickname());

        Profile profile = context.getProfile();

        if (profile == null) {
            throw new IllegalArgumentException("Вы не залогинились.");
        }

        String mealtime = ParseUtils.parseMealtime(input, context);
        logger.debug("mealtime = {}", mealtime);
        Date date = ParseUtils.parseDate(input.replace(mealtime, "").trim());
        logger.debug("date = {}", date);

        UserEatingDao userEatingDao = context.getUserEatingDao();
        UserEating userEating = userEatingDao.searchUserEating(profile.getId(), date, mealtime);
        logger.debug("userEating = {}", userEating);
        context.setUserEating(userEating);

        if (userEating != null) {
            Search.printUserEatingTitleTable();
            Search.printUserEating(profile, userEating);
        }
        logger.debug("open mealtime {} for user {}", mealtime, profile.getNickname());
    }

}
