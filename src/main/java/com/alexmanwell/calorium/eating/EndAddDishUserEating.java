package com.alexmanwell.calorium.eating;

import com.alexmanwell.calorium.Command;
import com.alexmanwell.calorium.CommandContext;

public class EndAddDishUserEating implements Command {
    @Override
    public void execute(CommandContext context) throws Exception {
        context.setUserEating(null);
    }
}
