package com.alexmanwell.calorium.eating;

import com.alexmanwell.calorium.Command;
import com.alexmanwell.calorium.CommandContext;
import com.alexmanwell.calorium.dish.Dish;
import com.alexmanwell.calorium.dish.DishDao;
import com.alexmanwell.calorium.profile.Profile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class RemoveDishUserEating implements Command {
    private final static Logger logger = LoggerFactory.getLogger(RemoveDishUserEating.class);
    private String input;

    public RemoveDishUserEating(String input) {
        this.input = input;
    }

    @Override
    public void execute(CommandContext context) throws Exception {

        logger.debug("begin remove dish {}", input);
        Profile profile = context.getProfile();

        if (profile == null) {
            throw new IllegalArgumentException("Вы не залогинились.");
        }

        UserEating userEating = context.getUserEating();
        if (userEating == null) {
            System.out.println("Войдите в режим ввода блюд");
            return;
        }

        String dishName = ParseUtils.parseDishName(input);
        logger.debug("dishName = {}", dishName);
        float dishAmount = ParseUtils.parseDishAmount(input.replace(dishName, "").trim());
        logger.debug("dishAmount = {}", dishAmount);

        DishDao dishDao = context.getDishDao();
        Dish dish = dishDao.searchDish(dishName);
        Map<Dish, Float> dishEating = new HashMap<>();
        dishEating.put(dish, dishAmount);

        Date date = userEating.getDate();
        String mealtime = userEating.getMealtime();

        UserEatingDao userEatingDao = context.getUserEatingDao();
        userEating = new UserEating(profile.getId(), date, mealtime, dishEating);
        userEatingDao.removeDishEating(userEating);

        logger.debug("end remove dish {} in specified mealtime {} in {}", input, mealtime, date);
    }
}
