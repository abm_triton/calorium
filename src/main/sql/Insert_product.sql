INSERT INTO product (name, kcal, protein, carbohydrate)
  VALUES ('Морковь', 41, 0.9, 10);

INSERT INTO product_fat (name, animal_fat, vegetable_fat)
  VALUES ('Морковь', 0, 0.1);

INSERT INTO product_vitamin(name,a1, b1, b2, b3, b6, b9, c, e, n, k1)
  VALUES ('Морковь', 0.018, 0.01, 0.02, 1.1, 0.1, 0.009, 5, 0.6, 0.00006, 0.0132);

INSERT INTO product_element (product_name, element_name, amount_elem)
  VALUES ('Морковь', 'Калий', 234);
INSERT INTO product_element (product_name, element_name, amount_elem)
  VALUES ('Морковь', 'Магний', 36);
INSERT INTO product_element (product_name, element_name, amount_elem)
  VALUES ('Морковь', 'Кальций', 46);
INSERT INTO product_element (product_name, element_name, amount_elem)
  VALUES ('Морковь', 'Фосфор', 60);
INSERT INTO product_element (product_name, element_name, amount_elem)
  VALUES ('Морковь', 'Сера', 6);
INSERT INTO product_element (product_name, element_name, amount_elem)
  VALUES ('Морковь', 'Натрий', 65);
INSERT INTO product_element (product_name, element_name, amount_elem)
  VALUES ('Морковь', 'Железо', 1.4);
INSERT INTO product_element (product_name, element_name, amount_elem)
  VALUES ('Морковь', 'Цинк', 0.4);
INSERT INTO product_element (product_name, element_name, amount_elem)
  VALUES ('Морковь', 'Марганец', 0.2);
INSERT INTO product_element (product_name, element_name, amount_elem)
  VALUES ('Морковь', 'Медь', 0.08);
INSERT INTO product_element (product_name, element_name, amount_elem)
  VALUES ('Морковь', 'Бор', 0.2);
INSERT INTO product_element (product_name, element_name, amount_elem)
  VALUES ('Морковь', 'Йод', 0.005);
INSERT INTO product_element (product_name, element_name, amount_elem)
  VALUES ('Морковь', 'Фтор', 0.055);
INSERT INTO product_element (product_name, element_name, amount_elem)
  VALUES ('Морковь', 'Молибден', 0.02);

INSERT INTO product_image (id_image, name, path)
VALUES (1, 'Морковь', '/image/carrot.jpg');

INSERT INTO product_description (product_name, description)
  VALUES ('Морковь', 'Морковь - двулетнее растение (редко одно- или многолетнее), в первый год жизни образует розетку листьев и корнеплод[4], во второй год жизни — семенной куст и семена. Широко распространена, в том числе в средиземноморских странах, Африке, Австралии, Новой Зеландии и Америке. В сельском хозяйстве выращивается морковь посевная (морковь культурная, рассматривается или как самостоятельный вид Daucus sativus, или как подвид моркови дикой — Daucus carota subsp. sativus) — двулетнее растение с грубым деревянистым беловатым или оранжевым корнем. Культурная морковь подразделяется на столовую и кормовую.');

INSERT INTO product_group (product_name, group_name)
  VALUES ('Морковь', 'Овощь');