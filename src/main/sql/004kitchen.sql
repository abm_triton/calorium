CREATE TABLE kitchen_product (
  kitchen_product_id SERIAL PRIMARY KEY NOT NULL,
  name_product CHARACTER VARYING(255) NOT NULL,
  amount_product DECIMAL NOT NULL,
  cost_product DECIMAL NOT NULL,
  date_purchase TIMESTAMP NOT NULL,
  FOREIGN KEY (name_product) REFERENCES product(name)
);

CREATE TABLE kitchen_dish (
  kitchen_dish_id SERIAL PRIMARY KEY NOT NULL,
  dish_id INTEGER NOT NULL,
  amount_dish DECIMAL NOT NULL,
  cost_product DECIMAL NOT NULL,
  date_cooking TIMESTAMP NOT NULL,
  FOREIGN KEY (dish_id) REFERENCES dish(dish_id)
);