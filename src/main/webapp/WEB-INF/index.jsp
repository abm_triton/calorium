<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:if test="${profile != null}">
    <jsp:useBean id="profile" scope="session" type="com.alexmanwell.calorium.profile.Profile"/>
</c:if>
<html>
<head>
    <jsp:include page="head.jsp" flush="true"/>

    <title>Калоризатор</title>
</head>
<body>
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
    <jsp:include page="header.jsp" flush="true"/>

    <main class="mdl-layout__content">
        <c:if test="${profile != null}">
            <div class="mdl-grid mdl-cell mdl-cell--12-col">
                <a href="${pageContext.request.contextPath}/printMealtime" class="mdl-button mdl-js-button mdl-button--raised mdl-button--primary" role="button">Вывести приемы пищи за сегодня</a>
            </div>
            <div class="mdl-grid mdl-cell mdl-cell--12-col">
                <a href="${pageContext.request.contextPath}/printMealtime" class="mdl-button mdl-js-button mdl-button--raised mdl-button--primary" role="button">Вывести приемы пищи за сегодня</a>
            </div>
        </c:if>
        <br/>
        <form>
            Enter Your Name: <input type="text" value="" name="userName" id="userName"/>
        </form>
        <br>
        <br>

        <strong>Ajax Response</strong>:
        <div>
            <ul class="mdl-list" id="ajaxGetUserServletResponse">
            </ul>
        </div>
    </main>
</div>

<script type="text/javascript">
    function getXmlHttp() {
        var xmlhttp;
        try {
            xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (E) {
                xmlhttp = false;
            }
        }
        if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
            xmlhttp = new XMLHttpRequest();
        }
        return xmlhttp;
    }

    function searchQuery() {
        document.addEventListener("DOMContentLoaded", function () {
            document.getElementById('userName').addEventListener("keydown", function () {
                var name = document.getElementById("userName").value;
                if (name.length > 2) {
                    var xmlhttp = getXmlHttp();
                    if (xmlhttp) {
                        xmlhttp.onreadystatechange = function () {
                            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                                var json = JSON.parse(xmlhttp.responseText);
                                for (key in json) {
                                    if (json.hasOwnProperty(key)) {
                                        printSearchQuery(json);
                                    }
                                }
                            }
                        };
                        xmlhttp.open('get', "${pageContext.request.contextPath}/search?userName=" + name, true);
                        xmlhttp.send(null);
                    }
                }
            }, false);
        }, false);
    }

    function printSearchQuery(json) {
        console.log("Значение " + json[key]);
        var li = document.createElement('li');
        li.className = "mdl-list__item";
        var a = document.createElement('a');
        a.href = "${pageContext.request.contextPath}/printProduct?productName=" + json[key];
        a.className = "mdl-list__item-primary-content";
        a.getAttribute('href');
        a.innerText = json[key];
        li.appendChild(a);
        document.getElementById("ajaxGetUserServletResponse").appendChild(li);
    }

    searchQuery();

</script>

</body>
</html>
