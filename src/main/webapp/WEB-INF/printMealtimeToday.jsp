<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:useBean id="profile" scope="session" type="com.alexmanwell.calorium.profile.Profile"/>
<jsp:useBean id="userEatings" scope="request" type="java.util.List<com.alexmanwell.calorium.eating.UserEating>"/>

<jsp:useBean id="resultNutrientsDish" scope="request" type="java.util.Map<java.lang.Long, java.util.Map<com.alexmanwell.calorium.dish.Dish, com.alexmanwell.calorium.product.Product>>"/>
<jsp:useBean id="resultNutrientsMealtimeDishes" scope="request" type="java.util.Map<java.lang.Long, com.alexmanwell.calorium.product.Product>"/>
<jsp:useBean id="resultNutrientsToday" scope="request" type="com.alexmanwell.calorium.product.Product"/>
<jsp:useBean id="currentDay" scope="request" type="java.lang.String"/>
<html>
<head>
    <jsp:include page="head.jsp" flush="true"/>
    <title>Calorizator | Прием пищи ${profile.nickname}</title>
</head>
<body>
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
    <jsp:include page="header.jsp" flush="true"/>

    <main class="mdl-layout__content">
        <div class="mdl-grid mdl-cell mdl-cell--12-col">
            <h1>${profile.nickname} | ${profile.mail}</h1>
        </div>
        <c:if test="${userEatings.size() != 0}">
        <c:forEach var="userEating" items="${userEatings}">
        <div class="mdl-grid mdl-cell mdl-cell--12-col">
            <table class="mdl-cell mdl-cell--12-col mdl-cell--12-col-phone mdl-cell--12-col-tablet mdl-data-table mdl-js-data-table mdl-shadow--2dp">

                <caption>
                    <div class="mdl-card__title mdl-color--teal-500">
                        <div class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone">
                            <h2 class="mdl-card__title-text mdl-color-text--white">${userEating.mealtime} - ${userEating.date}</h2>
                        </div>
                        <div class="mdl-cell mdl-cell--6-col mdl-cell--2-col-tablet mdl-cell--2-col-phone">
                            <button type="button" class="mdl-button mdl-button--raised mdl-js-button mdl-button--primary" style="float: right" onclick="addDishModalWindow(${userEating.eatingId})">Добавить блюдо</button>
                        </div>
                        <div class="icon" style="float:right;">
                            <a href="${pageContext.request.contextPath}/deleteMealtime?profileId=${sessionScope.profile.id}&eatingId=${userEating.eatingId}" title="удалить">
                                <span class="glyphicon glyphicon-remove"></span>
                            </a>
                            <a href="${pageContext.request.contextPath}/editMealtime?eatingId=${userEating.eatingId}" title="редактировать данные">
                                <span class="glyphicon glyphicon-edit"></span>
                            </a>
                        </div>
                    </div>
                </caption>

                <thead>
                <tr>
                    <th class="mdl-typography--text-center">Название блюда</th>
                    <th class="mdl-typography--text-center">количество</th>
                    <th class="mdl-typography--text-center">Калории</th>
                    <th class="mdl-typography--text-center">Белки</th>
                    <th class="mdl-typography--text-center">Углеводы</th>
                    <th class="mdl-typography--text-center">Жиры</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="ude" items="${userEating.userDishEatings}">
                    <tr>
                        <td class="mdl-typography--text-left mdl-data-table__cell--non-numeric">${ude.dish.name}</td>
                        <td class="mdl-typography--text-right"> ${ude.amountDish}</td>
                        <td class="mdl-typography--text-right"> ${resultNutrientsDish.get(userEating.eatingId).get(ude.dish).kcal}</td>
                        <td class="mdl-typography--text-right"> ${resultNutrientsDish.get(userEating.eatingId).get(ude.dish).protein}</td>
                        <td class="mdl-typography--text-right"> ${resultNutrientsDish.get(userEating.eatingId).get(ude.dish).carbohydrate}</td>
                        <td class="mdl-typography--text-right"> ${resultNutrientsDish.get(userEating.eatingId).get(ude.dish).fat}</td>
                        <td>
                            <a href="${pageContext.request.contextPath}/removeDishMealtime?eatingId=${userEating.eatingId}&dishEatingId=${ude.dishEatingId}" title="удалить">
                                <span class="glyphicon glyphicon-remove"></span>
                            </a>
                            <button type="button" onclick="editDishMealtime(${ude.eatingId}, ${ude.dishEatingId}, '${ude.dish.name}', ${ude.amountDish})" class="mdl-button mdl-button--raised mdl-js-button mdl-button--primary">
                                Редактировать
                            </button>
                        </td>
                    </tr>
                </c:forEach>
                <tr id="finalResult">
                    <td class="mdl-typography--text-left mdl-data-table__cell--non-numeric">Итого</td>
                    <td></td>
                    <td class="mdl-typography--text-right"> ${resultNutrientsMealtimeDishes.get(userEating.eatingId).kcal}</td>
                    <td class="mdl-typography--text-right"> ${resultNutrientsMealtimeDishes.get(userEating.eatingId).protein}</td>
                    <td class="mdl-typography--text-right"> ${resultNutrientsMealtimeDishes.get(userEating.eatingId).carbohydrate}</td>
                    <td class="mdl-typography--text-right"> ${resultNutrientsMealtimeDishes.get(userEating.eatingId).fat}</td>
                    <td></td>
                </tr>
                </tbody>
            </table>
        </div>
        </c:forEach>

        <div class="mdl-grid mdl-cell mdl-cell--12-col">
            <table class="mdl-cell mdl-cell--12-col mdl-cell--12-col-phone mdl-cell--12-col-tablet mdl-data-table mdl-js-data-table mdl-shadow--2dp">

                <caption>
                    <div class="mdl-card__title mdl-color--teal-500">
                        <div class="mdl-cell mdl-cell--12-col mdl-cell--12-col-tablet mdl-cell--12-col-phone">
                            <h2 class="mdl-card__title-text mdl-color-text--white">Итоговая таблица за сегодня ( ${currentDay})</h2>
                        </div>
                    </div>
                </caption>

                <thead>
                <tr>
                    <th class="mdl-typography--text-left">Калории</th>
                    <th class="mdl-typography--text-left">Белки</th>
                    <th class="mdl-typography--text-left">Углеводы</th>
                    <th class="mdl-typography--text-left">Жиры</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="mdl-typography--text-right"> ${resultNutrientsToday.kcal}</td>
                    <td class="mdl-typography--text-right"> ${resultNutrientsToday.protein}</td>
                    <td class="mdl-typography--text-right"> ${resultNutrientsToday.carbohydrate}</td>
                    <td class="mdl-typography--text-right"> ${resultNutrientsToday.fat}</td>
                </tr>
                </tbody>
            </table>
        </div>

        </c:if>
        <c:if test="${userEatings.size() == 0}">
        <p class="mdl-grid mdl-cell mdl-cell--12-col mdl-typography--title">Сегодня нет никаких приемов пищи</p>
        <div class="mdl-grid mdl-cell mdl-cell--12-col">
            <a href="${pageContext.request.contextPath}/createMealtime?nickname=${sessionScope.profile.nickname}" class="mdl-button mdl-js-button mdl-button--raised mdl-button--primary" role="button">Создать прием пищи</a>
        </div>
        </c:if>

        <table>
            <tbody id="load-data-here">

            </tbody>
        </table>
</div>

<dialog id="insert" class="mdl-dialog">
    <h5 class="mdl-dialog__title">Добавить блюдо</h5>
    <div class="mdl-dialog__content">
        <form id="insertDishMealtime" action="${pageContext.request.contextPath}/addDishMealtime" method="post">
            <div class="mdl-grid">
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--12-col">
                    <input class="mdl-textfield__input" type="text" name="dishName" value="" id="dishName">
                    <label class="mdl-textfield__label mdl-color-text--grey" for="dishName">Название блюда</label>
                </div>
            </div>
            <div class="mdl-grid">

                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--12-col">
                    <input class="mdl-textfield__input" type="text" name="amountDish" value="" pattern="-?[0-9]*(\.[0-9]+)?" id="amountDish">
                    <label class="mdl-textfield__label mdl-color-text--grey" for="amountDish">Количество блюда</label>
                    <span class="mdl-textfield__error">Некорректный ввод. Только цифры можно вводить!</span>
                </div>
            </div>
            <input type="submit" form="insertDishMealtime" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" style="float: left"/>
            <input type="reset" class="mdl-button mdl-js-button mdl-button--accent">
        </form>

    </div>
</dialog>

<dialog id="edit" class="mdl-dialog">
    <h3 class="mdl-dialog__title">Редактировать блюдо</h3>
    <div class="mdl-dialog__content">
        <form id="editDishMealtime" action="${pageContext.request.contextPath}/editDishMealtime" method="post">
            <div class="mdl-grid">
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--12-col">
                    <input class="mdl-textfield__input" type="text" name="dishName" id="editDishName" value=" ">
                    <label class="mdl-textfield__label mdl-color-text--grey" for="editDishName">Название блюда</label>
                </div>
            </div>
            <div class="mdl-grid">
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--12-col">
                    <input class="mdl-textfield__input" type="text" name="amountDish" id="editAmountDish" pattern="-?[0-9]*(\.[0-9]+)?" value="0">
                    <label class="mdl-textfield__label mdl-color-text--grey" for="editAmountDish">Количество блюда</label>
                    <span class="mdl-textfield__error">Некорректный ввод. Только цифры можно вводить!</span>
                </div>
            </div>
            <input type="submit" form="editDishMealtime" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" style="float: left"/>
            <input type="reset" class="mdl-button mdl-js-button mdl-button--accent">
        </form>
    </div>
</dialog>

<script>
    function addDishModalWindow(eatingId) {
        'use strict';

        if (!document.querySelector('#insert').showModal) {
            dialogPolyfill.registerDialog(document.querySelector('#insert'));
        }
        document.querySelector('#insert').showModal();
        document.querySelector('#insert').querySelector("input[type='reset']").addEventListener('click', function () {
            document.querySelector('#insert').close();
        });

        document.querySelector('#insert').querySelector("input[type='submit']").addEventListener('click', function () {
            var inputEatingId = document.createElement('input');
            inputEatingId.setAttribute("form", 'insertDishMealtime');
            inputEatingId.setAttribute("type", "hidden");
            inputEatingId.setAttribute("name", "eatingId");
            inputEatingId.setAttribute("value", eatingId);
            var form = document.getElementById("insertDishMealtime");
            form.appendChild(inputEatingId);
            document.querySelector('#insert').close();
        });
    }

    function editDishMealtime(eatingId, dishEatingId, dishName, dishAmount) {
        'use strict';

        var inputDishName = document.getElementById("editDishName");
        inputDishName.setAttribute("value", dishName);
        var inputDishAmount = document.getElementById("editAmountDish");
        inputDishAmount.setAttribute("value", dishAmount);

        if (!document.querySelector('#edit').showModal) {
            dialogPolyfill.registerDialog(document.querySelector('#edit'));
        }
        document.querySelector('#edit').showModal();
        document.querySelector('#edit').querySelector("input[type='reset']").addEventListener('click', function () {
            document.querySelector('#edit').close();
        });

        document.querySelector('#edit').querySelector("input[type='submit']").addEventListener('click', function () {
            var inputDishEatingId = document.createElement('input');
            inputDishEatingId.setAttribute("form", 'editDishMealtime');
            inputDishEatingId.setAttribute("type", "hidden");
            inputDishEatingId.setAttribute("name", "dishEatingId");
            inputDishEatingId.setAttribute("value", dishEatingId);
            var form = document.getElementById("editDishMealtime");
            form.appendChild(inputDishEatingId);

            var inputEatingId = document.createElement('input');
            inputEatingId.setAttribute("form", 'editDishMealtime');
            inputEatingId.setAttribute("type", "hidden");
            inputEatingId.setAttribute("name", "eatingId");
            inputEatingId.setAttribute("value", eatingId);
            form.appendChild(inputEatingId);
            document.querySelector('#edit').close();
        });
    }
</script>

</body>
</html>