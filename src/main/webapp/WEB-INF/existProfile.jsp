<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="profile" scope="request" type="com.alexmanwell.calorium.profile.Profile"/>
<html>
<head>
    <jsp:include page="head.jsp" flush="true"/>
    <title>Title</title>
</head>
<body>
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
    <jsp:include page="header.jsp" flush="true"/>

    <main class="mdl-layout__content">

        <h1>Профайл с таким ником ${profile.nickname} уже существует</h1>
    </main>
</div>
</body>
</html>
