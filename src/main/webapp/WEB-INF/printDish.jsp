<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <jsp:include page="head.jsp" flush="true"/>

    <title>Вывод блюда </title>
</head>
<body>
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
    <jsp:include page="header.jsp" flush="true"/>

    <jsp:useBean id="dish" scope="request" type="com.alexmanwell.calorium.dish.Dish"/>
    <jsp:useBean id="result" scope="request" type="com.alexmanwell.calorium.product.Product"/>

    <main class="mdl-layout__content">
        <c:if test="${dish != null}">
            <div class="mdl-grid">
                <table class="mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                    <thead>
                    <tr>
                        <th class="mdl-data-table__cell--non-numeric">Блюдо</th>
                        <th class="mdl-data-table__cell--non-numeric">Ингридиенты</th>
                        <th class="mdl-data-table__cell--non-numeric">Кол-во</th>
                        <th class="grid-gradient"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="mdl-typography--text-left mdl-data-table__cell--non-numeric">${dish.name}</td>
                        <td class="mdl-typography--text-right mdl-data-table__cell--non-numeric"></td>
                        <td class="mdl-typography--text-right mdl-data-table__cell--non-numeric"></td>
                        <td class="icon">
                            <a href="${pageContext.request.contextPath}/deleteDish?dishName=${dish.name}" title="удалить"><span class="glyphicon glyphicon-remove"></span></a>
                            <a href="${pageContext.request.contextPath}/editDish?dishName=${dish.name}" title="редактировать данные"><span class="glyphicon glyphicon-edit"></span></a>
                        </td>
                    </tr>
                    <c:forEach var="product" items="${dish.products}">
                        <tr>
                            <td></td>
                            <td class="mdl-typography--text-left mdl-data-table__cell--non-numeric"> ${product.key} </td>
                            <td class="mdl-typography--text-right mdl-data-table__cell--non-numeric"> ${product.value} </td>
                            <td></td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>

                <table class="mdl-cell mdl-cell--12-col mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                    <thead>
                    <tr>
                        <th class="grid-gradient"></th>
                        <th class="mdl-typography--text-left mdl-data-table__cell--non-numeric">Калории</th>
                        <th class="mdl-typography--text-left mdl-data-table__cell--non-numeric">Белки</th>
                        <th class="mdl-typography--text-left mdl-data-table__cell--non-numeric">Углеводы</th>
                        <th class="mdl-typography--text-left mdl-data-table__cell--non-numeric">Жиры</th>
                        <th class="mdl-typography--text-left mdl-data-table__cell--non-numeric">Животные жиры</th>
                        <th class="mdl-typography--text-left mdl-data-table__cell--non-numeric">Растительные жиры</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="mdl-typography--text-left mdl-data-table__cell--non-numeric mdl-typography--font-bold">Итого</td>
                        <td class="mdl-typography--text-right mdl-data-table__cell--non-numeric"><span class="mdl-typography--text-right"> ${result.kcal}</span></td>
                        <td class="mdl-typography--text-right mdl-data-table__cell--non-numeric"> ${result.protein}</td>
                        <td class="mdl-typography--text-right mdl-data-table__cell--non-numeric"> ${result.carbohydrate}</td>
                        <td class="mdl-typography--text-right mdl-data-table__cell--non-numeric"> ${result.fat}</td>
                        <td class="mdl-typography--text-right mdl-data-table__cell--non-numeric"> ${result.AFat}</td>
                        <td class="mdl-typography--text-right mdl-data-table__cell--non-numeric"><span class="mdl-typography--text-right"> ${result.VFat} </span></td>
                    </tr>
                    </tbody>
                </table>

                <c:if test="${result.vitamins.isEmpty() == false}">
                    <table class="mdl-cell mdl-cell--12-col mdl-cell--12-col-phone mdl-cell--12-col-tablet mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                        <caption class="vitaminTitle">Витамины</caption>
                        <thead>
                        <tr>
                            <c:forEach var="vitamin" items="${result.vitamins}">
                                <c:if test="${vitamin.value != 0}">
                                    <th class="mdl-data-table__cell--non-numeric">${vitamin.key}</th>
                                </c:if>
                            </c:forEach>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <c:forEach items="${result.vitamins}" var="vitamin">
                                <c:if test="${vitamin.value != 0}">
                                    <td class="mdl-data-table__cell--non-numeric">${vitamin.value}</td>
                                </c:if>
                            </c:forEach>
                        </tr>
                        </tbody>
                    </table>
                </c:if>

                <c:if test="${result.vitamins.isEmpty() == true}">
                    <div class="mdl-grid mdl-cell mdl-cell--12-col">
                        <p><b>Витаминов в базе нету.</b></p>
                    </div>
                </c:if>

                <c:if test="${result.elements.isEmpty() == false}">
                    <table class="mdl-cell mdl-cell--12-col mdl-cell--12-col-phone mdl-cell--12-col-tablet mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                        <caption class="elementTitle">Элементы</caption>
                        <thead>
                        <tr>
                            <c:forEach items="${result.elements}" var="element">
                                <c:if test="${element.value != 0}">
                                    <th class="mdl-data-table__cell--non-numeric">${element.key.getElement()}</th>
                                </c:if>
                            </c:forEach>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <c:forEach items="${result.elements}" var="element">
                                <c:if test="${element.value != 0}">
                                    <td class="mdl-data-table__cell--non-numeric">${element.value}</td>
                                </c:if>
                            </c:forEach>
                        </tr>
                        </tbody>
                    </table>
                </c:if>
            </div>
            <c:if test="${result.elements.isEmpty() == true}">
                <div class="mdl-grid mdl-cell mdl-cell--12-col">
                    <p><b>Элементов в базе нету.</b></p>
                </div>
            </c:if>
        </c:if>

        <c:if test="${dish == null}">
            <div class="mdl-grid mdl-cell mdl-cell--12-col">
                <p><b>Блюда ${dishName} нету в базе</b></p>
            </div>
        </c:if>

        <div class="mdl-grid mdl-cell mdl-cell--12-col">
            <a href="${pageContext.request.contextPath}/printDishes" class="mdl-button mdl-js-button mdl-button--raised" role="button">Вернуться назад</a>
        </div>

    </main>
</div>
</body>
</html>
