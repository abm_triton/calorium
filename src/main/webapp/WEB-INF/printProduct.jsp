<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <jsp:include page="head.jsp" flush="true"/>

    <title>Вывод конкретного продукта</title>
</head>
<body>
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
    <jsp:include page="header.jsp" flush="true"/>

    <c:if test="${product != null}">
        <jsp:useBean id="product" scope="request" type="com.alexmanwell.calorium.product.Product"/>
    </c:if>
    <main class="mdl-layout__content">
        <c:if test="${product != null}">
            <div class="mdl-grid">
                <table class="mdl-cell mdl-cell--12-col mdl-cell--12-col-phone mdl-cell--12-col-tablet mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                    <thead>
                    <tr>
                        <th class="mdl-data-table__cell--non-numeric">Название продукта</th>
                        <th class="mdl-data-table__cell--non-numeric">Калории</th>
                        <th class="mdl-data-table__cell--non-numeric">Белки</th>
                        <th class="mdl-data-table__cell--non-numeric">Углеводы</th>
                        <th class="mdl-data-table__cell--non-numeric">Животные жиры</th>
                        <th class="mdl-data-table__cell--non-numeric">Растительные жиры</th>
                        <th class="mdl-data-table__cell--non-numeric">Жиры</th>
                        <th class="mdl-data-table__cell--non-numeric">Группа продукта</th>
                        <th class="mdl-data-table__cell--non-numeric"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="text-left mdl-data-table__cell--non-numeric">${product.name}</td>
                        <td class="text-right mdl-data-table__cell--non-numeric"> ${product.kcal}</td>
                        <td class="text-right mdl-data-table__cell--non-numeric"> ${product.protein}</td>
                        <td class="text-right mdl-data-table__cell--non-numeric"> ${product.carbohydrate}</td>
                        <td class="text-right mdl-data-table__cell--non-numeric"> ${product.AFat}</td>
                        <td class="text-right mdl-data-table__cell--non-numeric"> ${product.VFat}</td>
                        <td class="text-right mdl-data-table__cell--non-numeric"> ${product.fat}</td>
                        <td class="text-right mdl-data-table__cell--non-numeric"> ${product.group}</td>
                        <td class="icon">
                            <a href="${pageContext.request.contextPath}/deleteProduct?productName=${product.name}"
                               title="удалить">
                                <span class="glyphicon glyphicon-remove"></span>
                            </a>
                            <a href="${pageContext.request.contextPath}/editProduct?productName=${product.name}"
                               title="редактировать данные">
                                <span class="glyphicon glyphicon-edit"></span>
                            </a>
                        </td>
                    </tr>
                    </tbody>
                </table>

                <c:if test="${product.vitamins.isEmpty() == false}">
                    <table class="mdl-cell mdl-cell--12-col mdl-cell--12-col-phone mdl-cell--12-col-tablet mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                        <caption class="vitaminTitle">Витамины</caption>
                        <thead>
                        <tr>
                            <c:forEach items="${product.vitamins}" var="vitamin">
                                <c:if test="${vitamin.value != 0}">
                                    <th class="mdl-data-table__cell--non-numeric">${vitamin.key}</th>
                                </c:if>
                            </c:forEach>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <c:forEach items="${product.vitamins}" var="vitamin">
                                <c:if test="${vitamin.value != 0}">
                                    <td class="mdl-data-table__cell--non-numeric">${vitamin.value}</td>
                                </c:if>
                            </c:forEach>
                        </tr>
                        </tbody>
                    </table>
                </c:if>

                <c:if test="${product.vitamins.isEmpty() == true}">
                    <div>
                        <div>
                            <p><b>Витаминов в базе нету.</b></p>
                        </div>
                    </div>
                </c:if>

                <c:if test="${product.elements.isEmpty() == false}">
                    <table class="mdl-cell mdl-cell--12-col mdl-cell--12-col-phone mdl-cell--12-col-tablet mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                        <caption class="elementTitle">Элементы</caption>
                        <thead>
                        <tr>
                            <c:forEach items="${product.elements}" var="element">
                                <c:if test="${element.value != 0}">
                                    <th class="mdl-data-table__cell--non-numeric">${element.key.element}</th>
                                </c:if>
                            </c:forEach>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <c:forEach items="${product.elements}" var="element">
                                <c:if test="${element.value != 0}">
                                    <td class="mdl-data-table__cell--non-numeric">${element.value}</td>
                                </c:if>
                            </c:forEach>
                        </tr>
                        </tbody>
                    </table>
                </c:if>

                <c:if test="${product.elements.isEmpty() == true}">
                    <div class="mdl-cell mdl-cell--12-col">
                        <p><b>Элементов в базе нету.</b></p>
                    </div>
                </c:if>


                <div class="mdl-cell mdl-cell--12-col">
                    <img style="float: right; padding: 7px;" src="${pageContext.request.contextPath}/imgProduct?productName=${product.name}"
                         alt="${product.name}">
                    <p>${product.description}</p>
                </div>
            </div>
        </c:if>


        <c:if test="${product == null}">
            <div class="mdl-cell mdl-cell--12-col">
                <p><b>Продукта ${productName} нету в базе</b></p>
            </div>
        </c:if>

        <div class="mdl-grid mdl-cell mdl-cell--12-col">
            <a href="${pageContext.request.contextPath}/printProducts" class="mdl-button mdl-js-button mdl-button--raised" role="button">Вернуться назад</a>
        </div>
    </main>
</div>
</body>
</html>