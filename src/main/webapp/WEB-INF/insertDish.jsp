<%--suppress ALL --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <jsp:include page="head.jsp" flush="true"/>

    <title>Добавление нового блюда</title>
</head>
<body>
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
    <jsp:include page="header.jsp" flush="true"/>
    <main class="mdl-layout__content">
        <div class="mdl-grid">
            <div class="mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-form mdl-shadow--16dp" style="margin: 10px auto;">
                <div class="mdl-card__title mdl-color--teal-500">
                    <h2 class="mdl-card__title-text mdl-color-text--white">Добавить новое блюдо</h2>
                </div>
                <form class="mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" action="${pageContext.request.contextPath}/insertDish" id="insert" method="post">
                    <div class="mdl-grid">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--6-col mdl-cell--12-col-tablet mdl-cell--12-col-phone">
                            <input class="mdl-textfield__input" form="insert" type="text" name="dishName" pattern="[^0-9]{3,35}$" id="dishName">
                            <label class="mdl-textfield__label mdl-color-text--grey" for="dishName">Название блюда</label>
                            <span class="mdl-textfield__error">Обязательное поле ввода!</span>
                        </div>
                    </div>

                    <div class="mdl-grid">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--6-col mdl-cell--12-col-tablet mdl-cell--12-col-phone">
                            <input class="mdl-textfield__input" form="insert" type="text" name="products.key" id="productName">
                            <label class="mdl-textfield__label mdl-color-text--grey" for="productName">Название продукта</label>
                        </div>


                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--6-col mdl-cell--12-col-tablet mdl-cell--12-col-phone">
                            <input class="mdl-textfield__input" form="insert" type="text" pattern="-?[0-9]*(\.[0-9]+)?" name="products.value" id="amountProduct">
                            <label class="mdl-textfield__label mdl-color-text--grey" for="amountProduct">Количество продукта</label>
                            <span class="mdl-textfield__error">Некорректный ввод. Только цифры можно вводить!</span>
                        </div>
                    </div>

                    <div id="newProduct"></div>

                    <div class="mdl-grid mdl-grid--no-spacing">
                        <div class="mdl-cell mdl-cell--12-col mdl-cell--12-col-tablet mdl-cell--12-col-phone">
                            <button id="buttonAddProduct" type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-button--primary" style="float: right" onclick="addProduct()">Добавить продукт</button>
                        </div>
                    </div>

                    <div class="mdl-grid"></div>

                    <div class="mdl-grid mdl-grid--no-spacing">
                        <div class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone">
                            <a href="${pageContext.request.contextPath}/printDishes" class="mdl-button mdl-js-button mdl-button--raised" role="button" style="float: left">Вернуться назад</a>
                        </div>
                        <div class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone">
                            <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--primary" style="float: right">Добавить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </main>

    <script src="${pageContext.request.contextPath}/js/addEntity.js"></script>
</div>
</body>
</html>
