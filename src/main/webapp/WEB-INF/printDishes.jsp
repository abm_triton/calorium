<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html;" charset=UTF-8"/>

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/style.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/material.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/mdl-form-select.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/mdl-form-upload-file.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/mdl-form.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/mdl-modal-window.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/mdl-profile-drawer-header.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/glyphicon.css">

    <script src="${pageContext.request.contextPath}/js/material.js"></script>
    <script src="${pageContext.request.contextPath}/js/mdl-form-select.js"></script>
    <script src="${pageContext.request.contextPath}/js/mdl-form-upload-file.js"></script>
    <script src="${pageContext.request.contextPath}/js/hide.js"></script>

    <title>Выво все блюд</title>
</head>
<body>
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
    <jsp:include page="header.jsp" flush="true"/>

    <main class="mdl-layout__content">

        <div class="mdl-grid mdl-cell mdl-cell--12-col">
            <a href="${pageContext.request.contextPath}/insertDish" class="mdl-button mdl-js-button mdl-button--raised" role="button">Добавить новое блюдо</a>
        </div>

        <div class="mdl-grid">
            <table class="mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                <caption>Спсиок всех блюд</caption>
                <thead>
                <tr>
                    <th class="mdl-data-table__cell--non-numeric">Блюдо</th>
                    <th class="mdl-data-table__cell--non-numeric">Ингридиенты</th>
                    <th class="mdl-data-table__cell--non-numeric">Кол-во</th>
                    <th class="mdl-data-table__cell--non-numeric"></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="dish" items="${dishes}">
                    <tr>
                        <td class="mdl-typography--text-left mdl-data-table__cell--non-numeric"> ${dish.getName()}
                            <span class="visible">
                                <a class="mdl-navigation__link mdl-color-text--blue" href="${pageContext.request.contextPath}/printDish?dishName=${dish.getName()}">Подробнее</a>
                            </span>
                        </td>
                        <td class="mdl-typography--text-right mdl-data-table__cell--non-numeric"></td>
                        <td class="mdl-typography--text-right mdl-data-table__cell--non-numeric"></td>
                        <td class="icon">
                            <a href="${pageContext.request.contextPath}/deleteDish?dishName=${dish.getName()}"
                               title="удалить">
                                <span class="glyphicon glyphicon-remove">
                                </span>
                            </a>
                            <a href="${pageContext.request.contextPath}/editDish?dishName=${dish.getName()}"
                               title="редактировать данные">
                                <span class="glyphicon glyphicon-edit">
                                </span>
                            </a>
                        </td>
                    </tr>
                    <c:forEach var="product" items="${dish.getProducts()}">
                        <tr>
                            <td class="mdl-typography--text-right mdl-data-table__cell--non-numeric"></td>
                            <td class="mdl-typography--text-right mdl-data-table__cell--non-numeric"> ${product.key} </td>
                            <td class="mdl-typography--text-right mdl-data-table__cell--non-numeric"> ${product.value} </td>
                            <td></td>
                        </tr>
                    </c:forEach>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </main>
</div>
</body>
</html>
