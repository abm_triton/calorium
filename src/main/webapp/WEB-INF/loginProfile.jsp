<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="profile" scope="session" type="com.alexmanwell.calorium.profile.Profile"/>
<html>
<head>
    <jsp:include page="head.jsp" flush="true"/>

    <title>Calorizator | Здравствуйте ${profile.nickname}</title>
</head>
<body>
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
    <jsp:include page="header.jsp" flush="true"/>
    <main class="mdl-layout__content">
        <div class="mdl-grid mdl-cell mdl-cell--12-col">
            <a href="${pageContext.request.contextPath}/printMealtime?profileId=${sessionScope.profile.id}" class="mdl-button mdl-js-button mdl-button--raised mdl-button--primary" role="button">Вывести приемы пищи за сегодня</a>
        </div>
    </main>

</div>
</body>
</html>
