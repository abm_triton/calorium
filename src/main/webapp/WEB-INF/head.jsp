<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<meta http-equiv="Content-Type" content="text/html;" charset=UTF-8"/>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/style.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/material.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/mdl-form-select.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/mdl-form-upload-file.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/mdl-form.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/mdl-modal-window.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/mdl-profile-drawer-header.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/glyphicon.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style/mdl-dialog.css">

<script src="${pageContext.request.contextPath}/js/material.js"></script>
<script src="${pageContext.request.contextPath}/js/mdl-form-select.js"></script>
<script src="${pageContext.request.contextPath}/js/mdl-form-upload-file.js"></script>
<script src="${pageContext.request.contextPath}/js/hide.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery-3.1.1.js"></script>
<script src="${pageContext.request.contextPath}/js/dynamic-table.js"></script>


