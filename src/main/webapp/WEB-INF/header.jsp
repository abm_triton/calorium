<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<header class="mdl-layout__header">
    <div class="mdl-layout__header-row">
        <div class="mdl-layout-spacer"></div>
        <nav class="mdl-navigation">
            <form class="mdl-grid" id="product" method="get" action="${pageContext.request.contextPath}/printProduct">
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable">
                    <label class="mdl-button mdl-js-button mdl-button--icon" for="productName">
                        <span class="glyphicon glyphicon-search"></span>
                    </label>
                    <div class="mdl-textfield__expandable-holder">
                        <input class="mdl-textfield__input" type="text" id="productName" form="product" name="productName">
                        <label class="mdl-textfield__label" for="product">Найти</label>
                    </div>
                </div>
            </form>
            <c:if test="${profile == null}">
                <div class="mdl-grid">
                    <label class="mdl-navigation__link" for="window-registration">Регистрация</label>
                    <label class="glyphicon glyphicon-log-in tooltip-login mdl-navigation__link" data-tool="Вход" for="window-login"></label>
                </div>
            </c:if>
        </nav>
    </div>
</header>
<div class="mdl-layout__drawer">
    <span class="mdl-layout__title mdl-layout__header"><a href="${pageContext.request.contextPath}/" class="mdl-color-text--white">Calorizator</a></span>
    <c:if test="${profile != null}">
        <header class="mdl-profile-drawer-header">
            <img src="#" class="mdl-profile-avatar mdl-profile-avatar--circle" alt="${profile.nickname}"/>
            <div class="mdl-profile-dropdown">
                <span>${profile.mail}</span>
                <div class="mdl-layout-spacer"></div>
                <button id="profileSettings" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon">
                    <i class="material-icons" role="presentation"><span class=" glyphicon glyphicon-triangle-bottom"></span></i>
                    <span class="visuallyhidden">Аккаунт</span>
                </button>
                <nav class="mdl-navigation mdl-menu mdl-menu--broad mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" for="profileSettings">
                    <a href="${pageContext.request.contextPath}/logoutProfile" class="mdl-navigation__link mdl-navigation__link--small-padding">Выход из профиля</a>
                </nav>
            </div>
        </header>
    </c:if>
    <nav class="mdl-navigation">
        <a href="${pageContext.request.contextPath}/printProducts" class="mdl-navigation__link" role="button">Вывести список всех продуктов</a>
        <a href="${pageContext.request.contextPath}/printDishes" class="mdl-navigation__link" role="button">Вывести список всех блюд</a>
        <c:if test="${profile != null}">
            <a href="${pageContext.request.contextPath}/createMealtime?nickname=${profile.nickname}" class="mdl-navigation__link" role="button">Создать прием пищи</a>
        </c:if>
    </nav>
</div>

<div class="mdl-modal-window">
    <input class="mdl-modal-window__open" id="window-login" type="checkbox" hidden>
    <div class="mdl-modal-window__wrap" aria-hidden="true" role="dialog">
        <div class="mdl-modal-window__dialog">
            <div class="mdl-card">
                <div class="mdl-card__title mdl-color--teal-500">
                    <h2 class="mdl-card__title-text mdl-color-text--white">Вход в профиль</h2>
                    <label class="mdl-button mdl-js-button mdl-modal-window__button mdl-button--icon" for="window-login" aria-hidden="true">&times;</label>
                </div>
                <form class="mdl-cell mdl-cell--12-col" id="login" method="post" action="${pageContext.request.contextPath}/loginProfile">
                    <div class="mdl-grid">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell mdl-cell--12-col">
                            <input class="mdl-textfield__input" type="text" id="nicknameLogin" name="nickname" form="login"/>
                            <label class="mdl-textfield__label mdl-color-text--grey" for="nicknameLogin">Никнейм пользователя</label>
                        </div>
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell mdl-cell--12-col">
                            <input class="mdl-textfield__input" type="password" id="passwordLogin" name="password" form="login"/>
                            <label class="mdl-textfield__label mdl-color-text--grey" for="passwordLogin">Пароль пользователя</label>
                        </div>
                        <div class="mdl-cell mdl-cell--12-col">
                            <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--primary" style="float: right">Войти</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="mdl-modal-window">
    <input class="mdl-modal-window__open" id="window-registration" type="checkbox" hidden>
    <div class="mdl-modal-window__wrap" aria-hidden="true" role="dialog">
        <div class="mdl-modal-window__dialog">
            <div class="mdl-card">
                <div class="mdl-card__title mdl-color--teal-500">
                    <h2 class="mdl-card__title-text mdl-color-text--white">Регистрация пользователя</h2>
                    <label class="mdl-button mdl-js-button mdl-modal-window__button mdl-button--icon" for="window-registration" aria-hidden="true">&times;</label>
                </div>
                <form class="mdl-cell mdl-cell--12-col" id="registration" method="post" action="${pageContext.request.contextPath}/createProfile">
                    <div class="mdl-grid">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell mdl-cell--12-col">
                            <input class="mdl-textfield__input" type="text" id="nicknameRegistration" name="nickname" form="registration"/>
                            <label class="mdl-textfield__label mdl-color-text--grey" for="nicknameRegistration">Никнейм пользователя</label>
                        </div>
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell mdl-cell--12-col">
                            <input class="mdl-textfield__input" type="password" id="passwordRegistration" name="password" form="registration"/>
                            <label class="mdl-textfield__label mdl-color-text--grey" for="passwordRegistration">Пароль</label>
                        </div>
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell mdl-cell--12-col">
                            <input class="mdl-textfield__input" type="password" id="confirmPassword" name="confirmPassword" form="registration"/>
                            <label class="mdl-textfield__label mdl-color-text--grey" for="confirmPassword">Повторите пароль</label>
                        </div>
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell mdl-cell--12-col">
                            <input class="mdl-textfield__input" type="email" id="mail" name="mail" form="registration"/>
                            <label class="mdl-textfield__label mdl-color-text--grey" for="mail">Введите почту</label>
                            <span class="mdl-textfield__error">Некорректный ввод!</span>
                        </div>
                        <div class="mdl-cell mdl-cell--12-col">
                            <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--primary" style="float: right">Зарегистрироваться</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
