<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <jsp:include page="head.jsp" flush="true"/>
    <title>Добавление нового продукта</title>
</head>
<body>
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
    <jsp:include page="header.jsp" flush="true"/>

    <main class="mdl-layout__content">
        <div class="mdl-grid">
            <div class="mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-form mdl-shadow--16dp" style="margin: 10px auto;">
                <div class="mdl-card__title mdl-color--teal-500">
                    <h2 class="mdl-card__title-text mdl-color-text--white">Добавить новый продукт</h2>
                </div>
                <form class="mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" action="${pageContext.request.contextPath}/insertProduct" id="insert" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                    <div class="mdl-grid">

                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--6-col mdl-cell--12-col-tablet mdl-cell--12-col-phone">
                            <input class="mdl-textfield__input" form="insert" type="text" name="productName" id="name">
                            <label class="mdl-textfield__label mdl-color-text--grey" for="name">Название продукта</label>
                        </div>
                        <div class="mdl-select mdl-js-select mdl-select--floating-label mdl-cell mdl-cell--12-col-tablet mdl-cell--12-col-phone">
                            <select class="mdl-select__input" id="group" form="insert" name="productGroup">
                                <option value=""></option>
                                <c:forEach var="productGroup" items="${groupType}">
                                    <option value="${productGroup}">${productGroup}</option>
                                </c:forEach>
                            </select>
                            <label class="mdl-textfield__label mdl-select__label mdl-color-text--grey" for="group">Группа продукта</label>
                        </div>
                    </div>

                    <div class="mdl-grid">

                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--4-col mdl-cell--12-col-tablet mdl-cell--12-col-phone">
                            <input class="mdl-textfield__input" form="insert" pattern="-?[0-9]*(\.[0-9]+)?" type="text" name="productKcal" id="productKcal">
                            <label class="mdl-textfield__label mdl-color-text--grey" for="productKcal">Ккалории</label>
                            <span class="mdl-textfield__error">Некорректный ввод. Только цифры можно вводить!</span>
                        </div>
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--4-col mdl-cell--12-col-tablet mdl-cell--12-col-phone">
                            <input class="mdl-textfield__input" form="insert" pattern="-?[0-9]*(\.[0-9]+)?" type="text" name="productProtein" id="productProtein">
                            <label class="mdl-textfield__label mdl-color-text--grey" for="productProtein">Белки</label>
                            <span class="mdl-textfield__error">Некорректный ввод. Только цифры можно вводить!</span>
                        </div>
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--4-col mdl-cell--12-col-tablet mdl-cell--12-col-phone">
                            <input class="mdl-textfield__input" form="insert" pattern="-?[0-9]*(\.[0-9]+)?" type="text" name="productCarbohydrate" id="productCarbohydrate">
                            <label class="mdl-textfield__label mdl-color-text--grey" for="productCarbohydrate">Углеводы</label>
                            <span class="mdl-textfield__error">Некорректный ввод. Только цифры можно вводить!</span>
                        </div>
                    </div>

                    <div class="mdl-grid">

                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--6-col mdl-cell--12-col-tablet mdl-cell--12-col-phone">
                            <input class="mdl-textfield__input" form="insert" pattern="-?[0-9]*(\.[0-9]+)?" type="text" name="productAFat" id="productAFat">
                            <label class="mdl-textfield__label mdl-color-text--grey" for="productAFat">Животный жир</label>
                            <span class="mdl-textfield__error">Некорректный ввод. Только цифры можно вводить!</span>
                        </div>

                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--6-col mdl-cell--12-col-tablet mdl-cell--12-col-phone">
                            <input class="mdl-textfield__input" form="insert" pattern="-?[0-9]*(\.[0-9]+)?" type="text" name="productVFat" id="productVFat">
                            <label class="mdl-textfield__label mdl-color-text--grey" for="productVFat">Растительный жир</label>
                            <span class="mdl-textfield__error">Некорректный ввод. Только цифры можно вводить!</span>
                        </div>
                    </div>

                    <button type="button" class="mdl-button mdl-js-button mdl-button--raised" onclick="hide('vitamins')">Витамины</button>
                    <div class="mdl-grid"></div>
                    <div class="mdl-grid" id="vitamins" style="display: none">

                        <c:forEach var="vitamin" items="${vitaminType}">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell mdl-cell--4-col mdl-cell--12-col-tablet mdl-cell--12-col-phone">
                                <input class="mdl-textfield__input" form="insert" type="text" pattern="-?[0-9]*(\.[0-9]+)?" name="${vitamin}" id="vitamin">
                                <label class="mdl-textfield__label mdl-color-text--grey" for="vitamin">${vitamin}</label>
                                <span class="mdl-textfield__error">Некорректный ввод. Только цифры можно вводить!</span>
                            </div>
                        </c:forEach>
                    </div>

                    <button type="button" class="mdl-button mdl-js-button mdl-button--raised" onclick="hide('elements')">Элементы</button>
                    <div class="mdl-grid" id="elements" style="display: none">
                        <c:forEach var="element" items="${elementType}">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell mdl-cell--4-col mdl-cell--12-col-tablet mdl-cell--12-col-phone">
                                <input class="mdl-textfield__input" form="insert" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="element" name="${element.getElementType(element.getElement())}">
                                <label class="mdl-textfield__label mdl-color-text--grey" for="element">${element.getElement()}</label>
                                <span class="mdl-textfield__error">Некорректный ввод. Только цифры можно вводить!</span>
                            </div>
                        </c:forEach>
                    </div>

                    <div class="mdl-grid">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--12-col mdl-cell--12-col-tablet mdl-cell--12-col-phone">
                            <textarea rows="5" class="mdl-textfield__input" form="insert" type="text" name="productDescription" id="productDescription" placeholder="Описание продукта"></textarea>
                        </div>
                    </div>
                    <br>
                    <div class="mdl-grid">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--file mdl-cell--12-col mdl-cell--10-col-tablet mdl-cell--12-col-phone">
                            <input class="mdl-textfield__input" placeholder="Загрузить картинку" type="text" id="uploadFile" readonly/>
                            <div class="mdl-button mdl-button--primary mdl-button--icon mdl-button--file">
                                <span class="glyphicon glyphicon-paperclip"></span><input type="file" name="image" id="uploadBtn">
                            </div>
                        </div>
                    </div>
                    <div class="mdl-grid mdl-grid--no-spacing">
                        <div class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone">
                            <a href="${pageContext.request.contextPath}/printProducts" class="mdl-button mdl-js-button mdl-button--raised" role="button" style="float: left">Вернуться назад</a>
                        </div>
                        <div class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone">
                            <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--primary" style="float: right">Добавить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </main>
</div>

<script src="${pageContext.request.contextPath}/js/mdl-form-upload-file.js"></script>
</body>
</html>
