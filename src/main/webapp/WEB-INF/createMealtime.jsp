<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<jsp:useBean id="profile" scope="session" type="com.alexmanwell.calorium.profile.Profile"/>
<html>
<head>
    <jsp:include page="head.jsp" flush="true"/>

    <title>Calorizator | Здравствуйте ${profile.nickname}</title>
</head>
<body>
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
    <jsp:include page="header.jsp" flush="true"/>

    <main class="mdl-layout__content">
        <div class="mdl-grid">
            <h1>${profile.nickname} | ${profile.mail}</h1>

            <div class="mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-form mdl-shadow--16dp" style="margin: 10px auto;">
                <div class="mdl-card__title mdl-color--teal-500">
                    <h2 class="mdl-card__title-text mdl-color-text--white">Создание приема пищи</h2>
                </div>

                <form class="mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" action="${pageContext.request.contextPath}/createMealtime" id="insert" method="post">
                    <input hidden name="nickname" value="${profile.nickname}"/>

                    <div class="mdl-select mdl-js-select mdl-select--floating-label">
                        <select class="mdl-select__input" form="insert" name="mealtime" id="mealtime">
                            <option value=""></option>
                            <option value="Завтрак">Завтрак</option>
                            <option value="Обед">Обед</option>
                            <option value="Ужин">Ужин</option>
                        </select>
                        <label class="mdl-textfield__label mdl-select__label mdl-color-text--grey" for="mealtime">Прием пищи</label>
                    </div>

                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--4-col mdl-cell--12-col-tablet mdl-cell--12-col-phone">
                        <input class="mdl-textfield__input" form="insert" type="date" name="date" id="date">
                        <label class="mdl-textfield__label mdl-color-text--grey" for="date">Дата приема пищи</label>
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--4-col mdl-cell--12-col-tablet mdl-cell--12-col-phone">
                        <input class="mdl-textfield__input" form="insert" type="time" name="time" id="time">
                        <label class="mdl-textfield__label mdl-color-text--grey" for="time">Время приема пищи</label>
                    </div>

                    <div class="mdl-grid">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--6-col mdl-cell--12-col-tablet mdl-cell--12-col-phone">
                            <input class="mdl-textfield__input" form="insert" type="text" name="dishes.key" id="dishName">
                            <label class="mdl-textfield__label mdl-color-text--grey" for="dishName">Название блюда</label>
                        </div>
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--6-col mdl-cell--12-col-tablet mdl-cell--12-col-phone">
                            <input class="mdl-textfield__input" form="insert" type="text" name="dishes.value" pattern="-?[0-9]*(\.[0-9]+)?" id="dishAmount">
                            <label class="mdl-textfield__label mdl-color-text--grey" for="dishAmount">Количество блюда</label>
                            <span class="mdl-textfield__error">Некорректный ввод. Только цифры можно вводить!</span>
                        </div>
                    </div>
                    <div id="newDish"></div>

                    <div class="mdl-grid"></div>
                    <div class="mdl-grid mdl-grid--no-spacing">
                        <div class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone">
                            <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent">Создать</button>
                        </div>
                        <div class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone">
                            <button id="addDishButton" type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-button--primary" style="float: right" onclick="addDish()">Добавить блюдо</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </main>
</div>

<script src="${pageContext.request.contextPath}/js/addEntity.js"></script>
</body>
</html>
