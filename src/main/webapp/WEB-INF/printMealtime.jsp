<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<jsp:useBean id="profile" scope="session" type="com.alexmanwell.calorium.profile.Profile"/>
<jsp:useBean id="userEating" scope="request" type="com.alexmanwell.calorium.eating.UserEating"/>

<html>
<head>
    <jsp:include page="head.jsp" flush="true"/>

    <title>Calorizator | Прием пищи ${profile.nickname}</title>
</head>
<body>
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
    <jsp:include page="header.jsp" flush="true"/>

    <main class="mdl-layout__content">
        <h1>${profile.nickname} | ${profile.mail}</h1>
        <c:if test="${userEating != null}">
            <div class="mdl-grid">
                <table class="mdl-cell mdl-cell--12-col mdl-cell--12-col-phone mdl-cell--12-col-tablet mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                    <caption>
                        <div class="mdl-card__title mdl-color--teal-500">
                            <div class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone">
                                <h2 class="mdl-card__title-text mdl-color-text--white">${userEating.mealtime} - ${userEating.date}</h2>
                            </div>
                            <div class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone">
                                <button id="addDishButton" type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-button--primary" style="float: right" onclick="addDish()">Добавить блюдо</button>
                            </div>
                        </div>
                    </caption>
                    <thead>
                    <tr>
                        <th class="mdl-data-table__cell--non-numeric">Название блюда</th>
                        <th class="mdl-data-table__cell--non-numeric">количество</th>
                        <th class="mdl-data-table__cell--non-numeric">Калории</th>
                        <th class="mdl-data-table__cell--non-numeric">Белки</th>
                        <th class="mdl-data-table__cell--non-numeric">Углеводы</th>
                        <th class="mdl-data-table__cell--non-numeric">Жиры</th>
                        <th class="mdl-data-table__cell--non-numeric"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="dishes" items="${userEating.dishEating}">
                        <c:forEach var="dish" items="${dishes.value}">
                            <tr>
                                <td class="text-left mdl-data-table__cell--non-numeric">${dish.key.name}</td>
                                <td class="text-left mdl-data-table__cell--non-numeric"> ${dish.value}</td>
                                <td class="text-left mdl-data-table__cell--non-numeric"> ${dish.value}</td>
                                <td class="text-left mdl-data-table__cell--non-numeric"> ${dish.value}</td>
                                <td class="text-left mdl-data-table__cell--non-numeric"> ${dish.value}</td>
                                <td class="text-left mdl-data-table__cell--non-numeric"> ${dish.value}</td>
                                <td class="icon">
                                    <a href="${pageContext.request.contextPath}/removeDishMealtime?eatingId=${userEating.eatingId}&dishEatingId=${dishes.key}"
                                       title="удалить">
                                        <span class="glyphicon glyphicon-remove"></span>
                                    </a>
                                    <a href="${pageContext.request.contextPath}/editMealtime?eatingId=${userEating.eatingId}"
                                       title="редактировать данные">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </c:if>

    </main>
</div>

</body>
</html>
