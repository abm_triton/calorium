<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <jsp:include page="head.jsp" flush="true"/>

    <title>Вывод продуктов</title>
</head>
<body>
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
    <jsp:include page="header.jsp" flush="true"/>

    <main class="mdl-layout__content">
        <div class="mdl-grid mdl-cell mdl-cell--12-col">
            <a href="${pageContext.request.contextPath}/insertProduct" class="mdl-button mdl-js-button mdl-button--raised" role="button">Добавить новый продукт</a>
        </div>
        <div class="mdl-grid">
            <table class="mdl-cell mdl-cell--12-col mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                <caption>Спсиок всех продуктов</caption>
                <thead>
                <tr>
                    <th class="mdl-data-table__cell--non-numeric">Название</th>
                    <th class="mdl-data-table__cell--non-numeric">Ккалории</th>
                    <th class="mdl-data-table__cell--non-numeric">Белки</th>
                    <th class="mdl-data-table__cell--non-numeric">Углеводы</th>
                    <th class="mdl-data-table__cell--non-numeric">Жиры</th>
                    <th class="mdl-data-table__cell--non-numeric">Группа продукта</th>
                    <th class="grid-gradient"></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="product" items="${products}">
                    <tr>
                        <td class="text-left mdl-data-table__cell--non-numeric"> ${product.getName()}
                            <a class="mdl-navigation__link mdl-color-text--blue" href="${pageContext.request.contextPath}/printProduct?productName=${product.getName()}">Подробнее</a>
                        </td>
                        <td class="text-right mdl-data-table__cell--non-numeric"> ${product.getKcal()} </td>
                        <td class="text-right mdl-data-table__cell--non-numeric"> ${product.getProtein()} </td>
                        <td class="text-right mdl-data-table__cell--non-numeric"> ${product.getCarbohydrate()} </td>
                        <td class="text-right mdl-data-table__cell--non-numeric">
                            <span class="show">${product.getFat()}
                                <em>животные: ${product.getAFat()} растительные: ${product.getVFat()}<i></i></em>
                            </span>
                        </td>
                        <td class="text-right mdl-data-table__cell--non-numeric"> ${product.getGroup()} </td>
                        <td class="icon">
                            <a href="${pageContext.request.contextPath}/deleteProduct?productName=${product.getName()}"
                               title="удалить">
                                <span class="glyphicon glyphicon-remove">
                                </span>

                            </a>
                            <a href="${pageContext.request.contextPath}/editProduct?productName=${product.getName()}"
                               title="редактировать данные">
                                <span class="glyphicon glyphicon-edit">
                                </span>
                            </a>
                        </td>
                    </tr>

                </c:forEach>
                </tbody>
            </table>
        </div>
    </main>
</div>
</body>
</html>
