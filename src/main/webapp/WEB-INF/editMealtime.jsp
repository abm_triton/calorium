<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="profile" scope="session" type="com.alexmanwell.calorium.profile.Profile"/>
<jsp:useBean id="userEating" scope="request" type="com.alexmanwell.calorium.eating.UserEating"/>
<html>
<head>
    <jsp:include page="head.jsp" flush="true"/>

    <title>Редакторовать данные приема пищи </title>
</head>
<body>
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
    <jsp:include page="header.jsp" flush="true"/>

    <main class="mdl-layout__content">
        <div class="mdl-grid">
            <h1>${profile.nickname} | ${profile.mail}</h1>

            <div class="mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-form mdl-shadow--16dp" style="margin: 10px auto;">
                <div class="mdl-card__title mdl-color--teal-500">
                    <h2 class="mdl-card__title-text mdl-color-text--white">Редактирование приема пищи ${userEating.mealtime} -- ${userEating.date}</h2>
                </div>

                <form class="mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" action="${pageContext.request.contextPath}/editMealtime" id="edit" method="post">
                    <c:set var="userEating" value="${userEating}" scope="session"/>
                    <c:forEach var="dish" items="${userEating.userDish}">
                        <div class="mdl-grid">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--6-col mdl-cell--12-col-tablet mdl-cell--12-col-phone">
                                <input class="mdl-textfield__input" form="edit" type="text" name="dish.key" value="${dish.key.name}" id="dishName">
                                <label class="mdl-textfield__label mdl-color-text--grey" for="dishName">Название блюда</label>
                            </div>
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--6-col mdl-cell--12-col-tablet mdl-cell--12-col-phone">
                                <input class="mdl-textfield__input" form="edit" type="text" name="dish.value" value="${dish.value}" pattern="-?[0-9]*(\.[0-9]+)?" id="dishAmount">
                                <label class="mdl-textfield__label mdl-color-text--grey" for="dishAmount">Количество блюда</label>
                                <span class="mdl-textfield__error">Некорректный ввод. Только цифры можно вводить!</span>
                            </div>
                        </div>
                    </c:forEach>

                    <div class="mdl-grid mdl-grid--no-spacing">
                        <div class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone">
                            <a href="${pageContext.request.contextPath}/printDishes" class="mdl-button mdl-js-button mdl-button--raised" role="button" style="float: left">Вернуться назад</a>
                        </div>
                        <div class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone">
                            <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--primary" style="float: right">Редактировать</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </main>
</div>

</body>
</html>
