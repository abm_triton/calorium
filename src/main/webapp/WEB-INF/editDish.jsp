<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <jsp:include page="head.jsp" flush="true"/>

    <title>Редактирование данных блюда</title>
</head>
<body>
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">

    <jsp:include page="header.jsp" flush="true"/>

    <jsp:useBean id="dish" scope="request" type="com.alexmanwell.calorium.dish.Dish"/>

    <main class="mdl-layout__content">
        <div class="mdl-grid">
            <div class="mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone mdl-form mdl-shadow--16dp" style="margin: 10px auto;">
                <div class="mdl-card__title mdl-color--teal-500">
                    <h2 class="mdl-card__title-text mdl-color-text--white">Редактирование блюда ${dish.name}</h2>
                </div>
                <form class="mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--4-col-phone" action="${pageContext.request.contextPath}/editDish" id="edit" method="post">
                    <input hidden name="dishName" value="${dish.name}">
                    <c:forEach var="product" items="${dish.products}">
                        <div class="mdl-grid">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--6-col mdl-cell--12-col-tablet mdl-cell--12-col-phone">
                                <input class="mdl-textfield__input" form="edit" type="text" name="product.key" id="productName" value="${product.key}">
                                <label class="mdl-textfield__label mdl-color-text--grey" for="productName">Редактировать продукт </label>
                            </div>

                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--6-col mdl-cell--12-col-tablet mdl-cell--12-col-phone">
                                <input class="mdl-textfield__input" form="edit" type="text" name="product.value" id="amountProduct" value="${product.value}">
                                <label class="mdl-textfield__label mdl-color-text--grey" for="amountProduct">Количество продукта </label>
                            </div>
                        </div>
                    </c:forEach>
                    <div class="mdl-grid mdl-grid--no-spacing">
                        <div class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone">
                            <a href="${pageContext.request.contextPath}/printDishes" class="mdl-button mdl-js-button mdl-button--raised" role="button" style="float: left">Вернуться назад</a>
                        </div>
                        <div class="mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone">
                            <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--primary" style="float: right">Редактировать</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </main>
</div>
</body>
</html>
