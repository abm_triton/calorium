/*
document.addEventListener("DOMContentLoaded", function () {
    loadData();
}, false);
*/
/*

function getXmlHttp() {
    var xmlhttp;
    try {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
            xmlhttp = false;
        }
    }
    if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}

$(document).on("click", ".selectData", function () {
    var dishEatingId = $(this).attr("id");
    console.log(dishEatingId);

    var xmlhttp = getXmlHttp();
    xmlhttp.open('get', "http://localhost:8080/calorium/ajaxSelectConcreteDishMealtime?dishEatingId=" + dishEatingId, true);
    xmlhttp.send(null);
    if (xmlhttp) {
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                var json = JSON.parse(xmlhttp.responseText);

                $("[name='dishEatingId']").val(json.dishEatingId);
                $("[name='eatingId']").val(json.eatingId);
                $("[name='dishId']").val(json.dishId);
                $("[name='dishName']").val(json.dishName);
                $("[name='amountDish']").val(json.amountDish);
            }
        }
    }
});

function deleteData() {
    var dishEatingId = document.getElementById('dishEatingId').value;
    console.log(dishEatingId);
    var xmlhttp = getXmlHttp();
    xmlhttp.open('get', "http://localhost:8080/calorium/ajaxDeleteDishMealtime?dishEatingId=" + dishEatingId, true);
    xmlhttp.send(null);
    if (xmlhttp) {
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                loadData();
            }
        }
    }
}

function updateData() {
    var dishEatingId = document.getElementById('dishEatingId').value;
    var eatingId = document.getElementById('eatingId').value;
    var dishName = document.getElementById('dishName').value;
    var amountDish = document.getElementById('amountDish').value;
    var dishId = document.getElementById('dishId').value;
    console.log(dishEatingId + " | " + eatingId + " | " + dishName + " | " + amountDish + " | " + dishId);
    var xmlhttp = getXmlHttp();
    xmlhttp.open('get', "http://localhost:8080/calorium/ajaxUpdateDishMealtime?dishEatingId=" + dishEatingId + "&dishName=" + dishName + "&amountDish=" + amountDish + "&dishId=" + dishId + "&eatingId=" + eatingId, true);
    xmlhttp.send(null);
    if (xmlhttp) {
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                loadData();
            }
        }
    }
}

document.addEventListener('onclick', [].forEach.call(document.querySelectorAll('.selectData'), function (el) {
    var dishEatingId = el.getAttribute('id');
    console.log(dishEatingId);

    var xmlhttp = getXmlHttp();
    xmlhttp.open('get', "http://localhost:8080/calorium/ajaxSelectConcreteDishMealtime?dishEatingId=" + dishEatingId, true);
    xmlhttp.send(null);
    if (xmlhttp) {
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                var json = JSON.parse(xmlhttp.responseText);

                $("[name='dishEatingId']").val(json.dishEatingId);
                $("[name='eatingId']").val(json.eatingId);
                $("[name='dishId']").val(json.dishId);
                $("[name='dishName']").val(json.dishName);
                $("[name='amountDish']").val(json.amountDish);
            }
        }
    }
}));

function insertData() {

    var eatingId = document.getElementById("createDiv").textContent;
    console.log(" | " + eatingId + "  | ");
    var dishName = "" + document.getElementById('dishName').value;
    var amountDish = "" + document.getElementById('amountDish').value;
    console.log(eatingId + "   " + dishName + "   " + amountDish);

    document.getElementById("eatingId").parentNode.removeChild(document.getElementById("eatingId"));
    document.getElementById("createDiv").parentNode.removeChild(document.getElementById("createDiv"));

    document.getElementById("dialog").close();

    var xmlhttp = getXmlHttp();
    xmlhttp.open('get', "http://localhost:8080/calorium/ajaxAddDishMealtime?eatingId=" + eatingId + "&dishName=" + dishName + "&amountDish=" + amountDish, true);
    xmlhttp.send(null);
    if (xmlhttp) {
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

                loadData();
            }
        }
    }
}

function loadData() {
    var dataHandler = document.getElementById("load-data-here");
    dataHandler.innerHTML = "";
    var xmlhttp = getXmlHttp();
    xmlhttp.open('get', "http://localhost:8080/calorium/ajaxSelectMealtime?eatingId=255", true);
    xmlhttp.send();
    console.log(xmlhttp.statusText);
    if (xmlhttp) {
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                var json = JSON.parse(xmlhttp.responseText);
                for (key in json['userDishEating']) {
                    console.log(json['userDishEating'][key]['dishName'] + " | " + json['userDishEating'][key]['amountDish']);
                    var newRow = document.createElement('tr');

                    $("[name='dishEatingId']").val(json['userDishEating'][key]['dishEatingId']);
                    $("[name='dishId']").val(json['userDishEating'][key]['dishId']);

                    var newColumnNameDish = document.createElement('td');
                    newColumnNameDish.innerText = json['userDishEating'][key]['dishName'];
                    newRow.appendChild(newColumnNameDish);

                    var newColumnAmountDish = document.createElement('td');
                    newColumnAmountDish.innerText = json['userDishEating'][key]['amountDish'];
                    newRow.appendChild(newColumnAmountDish);

                    var button = document.createElement('button');
                    button.className = "selectData";
                    button.innerText = "Button";
                    button.id = json['userDishEating'][key]['dishEatingId'];
                    newRow.appendChild(button);
                    dataHandler.appendChild(newRow);

                }
                createTable(json);
            }
        }
    }
}

function addDishModalWindow(eatingId) {
    'use strict';
    var inputEatingId = document.createElement("input");
    inputEatingId.setAttribute("id", "eatingId");
    inputEatingId.setAttribute("type", "text");
    inputEatingId.setAttribute("name", "eatingId");
    inputEatingId.setAttribute('value', eatingId);
    inputEatingId.hidden = true;

    var createDiv = document.createElement("div");
    createDiv.setAttribute("id", "createDiv");
    createDiv.innerText = eatingId;
    createDiv.style.display = "none";

    document.getElementById("insertDishMealtime").appendChild(createDiv);
    document.getElementById("insertDishMealtime").appendChild(inputEatingId);
}

function editDishMealtime(dishEatingId, dishName, amountDish) {
    'use strict';
    var dEatingId = document.createElement("div");
    dEatingId.setAttribute("id", "dEatingId");
    dEatingId.innerText = dishEatingId;
    dEatingId.style.display = "none";
    document.getElementById("insertDishMealtime").appendChild(dEatingId);

    document.getElementById("dishName").setAttribute("value", dishName);
    document.getElementById("amountDish").setAttribute("value", amountDish);
}

document.addEventListener("DOMContentLoaded", function () {
    'use strict';
    var dialogButton = document.querySelectorAll('.dialog-button');
    var dialog = document.querySelector('#dialog');
    if (!dialog.showModal) {
        dialogPolyfill.registerDialog(dialog);
    }

    for (var i = 0; i < dialogButton.length; i++) {
        dialogButton[i].addEventListener('click', function () {
            dialog.showModal();
        });
    }

    dialog.querySelector('button:not([disabled])').addEventListener('click', function () {
        if (document.getElementById("createDiv")) {
            document.getElementById("createDiv").parentNode.removeChild(document.getElementById("createDiv"));
            document.getElementById("eatingId").parentNode.removeChild(document.getElementById("eatingId"));
        }
        if (document.getElementById("dEatingId")) {
            document.getElementById("dEatingId").parentNode.removeChild(document.getElementById("dEatingId"));
        }
        dialog.close();
    });
}, false);


function createTable(json) {
    'use strict';

    for (var keyJson in json) {
        console.log(json[keyJson]);
    }

    var div = document.createElement("div");
    div.className = "mdl-grid mdl-cell mdl-cell--12-col";

    var table = document.createElement("table");
    table.className = "mdl-cell mdl-cell--12-col mdl-cell--12-col-phone mdl-cell--12-col-tablet mdl-data-table mdl-js-data-table mdl-shadow--2dp";
    div.appendChild(table);

    var thead = document.createElement("thead");
    table.appendChild(thead);
    var tr = document.createElement("tr");
    thead.appendChild(tr);

    var titleNames = ["Название блюда", "количество", "Калории", "Белки", "Углеводы", "Жиры", "          "];
    for (var i = 0; i < titleNames.length; i++) {
        var th = document.createElement("th");
        th.className = "mdl-typography--text-center";
        th.innerText = titleNames[i];
        tr.appendChild(th);
    }

    var tbody = document.createElement("tbody");
    table.appendChild(tbody);
    for (var key in json['userDishEating']) {
        var newRow = document.createElement('tr');

        var newColumnNameDish = document.createElement('td');
        newColumnNameDish.innerText = json['userDishEating'][key]['dishName'];
        newRow.appendChild(newColumnNameDish);

        var newColumnAmountDish = document.createElement('td');
        newColumnAmountDish.innerText = json['userDishEating'][key]['amountDish'];
        newRow.appendChild(newColumnAmountDish);

        tbody.appendChild(newRow);
    }

    document.getElementsByTagName("dialog")[0].parentNode.insertBefore(div, document.getElementsByTagName("dialog")[0]);
}*/
