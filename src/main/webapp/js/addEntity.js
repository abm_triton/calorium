var i = 1;
function addProduct() {
    var divProduct = document.createElement('div');
    divProduct.className = 'mdl-grid';

    var innerDiv = document.createElement('div');
    innerDiv.className = 'mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--6-col mdl-cell--12-col-tablet mdl-cell--12-col-phone is-upgraded';

    var inputProduct = document.createElement('input');
    inputProduct.className = 'mdl-textfield__input';
    inputProduct.setAttribute("form", 'insert');
    inputProduct.setAttribute("type", 'text');
    inputProduct.setAttribute("name", 'products.key');

    inputProduct.setAttribute("id", 'productName' + ++i);

    var labelProduct = document.createElement('label');
    labelProduct.className = 'mdl-textfield__label mdl-color-text--grey';
    labelProduct.setAttribute("for", 'productName' + i);

    var placeholderProduct ='Название продукта';
    labelProduct.innerText = placeholderProduct;

    innerDiv.appendChild(inputProduct);
    innerDiv.appendChild(labelProduct);
    divProduct.appendChild(innerDiv);

    var div = document.getElementById('newProduct');
    var cont = div.parentNode;
    cont.insertBefore(divProduct, div);

    var innerSecondDiv = document.createElement('div');
    innerSecondDiv.className = 'mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--6-col mdl-cell--12-col-tablet mdl-cell--12-col-phone is-upgraded';

    var inputAmount = document.createElement('input');
    inputAmount.className = 'mdl-textfield__input';
    inputAmount.setAttribute("form", 'insert');
    inputAmount.setAttribute("type", 'text');
    inputAmount.setAttribute("name", 'products.value');
    inputAmount.setAttribute('pattern', '-?[0-9]*(\.[0-9]+)?');
    inputAmount.setAttribute("id", 'amountProduct' + i);

    var labelAmount = document.createElement('label');
    labelAmount.className = 'mdl-textfield__label mdl-color-text--grey';
    labelAmount.setAttribute("for", 'amountProduct' + i);

    var placeholderAmount ='Количество продукта';
    labelAmount.innerText = placeholderAmount;

    innerSecondDiv.appendChild(inputAmount);
    innerSecondDiv.appendChild(labelAmount);
    divProduct.appendChild(innerSecondDiv);

    div = document.getElementById('newProduct');
    cont = div.parentNode;
    cont.insertBefore(divProduct, div);

    componentHandler.upgradeAllRegistered();
};

function addDish() {
    var i = 1;
    var divDish = document.createElement('div');
    divDish.className = 'mdl-grid';

    var innerDiv = document.createElement('div');
    innerDiv.className = 'mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--6-col mdl-cell--12-col-tablet mdl-cell--12-col-phone is-upgraded';

    var inputDish = document.createElement('input');
    inputDish.className = 'mdl-textfield__input';
    inputDish.setAttribute("form", 'insert');
    inputDish.setAttribute("type", 'text');
    inputDish.setAttribute("name", 'dishes.key');

    inputDish.setAttribute("id", 'dishName' + ++i);

    var labelDish = document.createElement('label');
    labelDish.className = 'mdl-textfield__label mdl-color-text--grey';
    labelDish.setAttribute("for", 'dishName' + i);

    var placeholderProduct = 'Название блюда';
    labelDish.innerText = placeholderProduct;

    innerDiv.appendChild(inputDish);
    innerDiv.appendChild(labelDish);
    divDish.appendChild(innerDiv);

    var div = document.getElementById('newDish');
    var cont = div.parentNode;
    cont.insertBefore(divDish, div);

    var innerSecondDiv = document.createElement('div');
    innerSecondDiv.className = 'mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell--6-col mdl-cell--12-col-tablet mdl-cell--12-col-phone is-upgraded';

    var inputAmount = document.createElement('input');
    inputAmount.className = 'mdl-textfield__input';
    inputAmount.setAttribute("form", 'insert');
    inputAmount.setAttribute("type", 'text');
    inputAmount.setAttribute("name", 'dishes.value');
    inputAmount.setAttribute('pattern', '-?[0-9]*(\.[0-9]+)?');
    inputAmount.setAttribute("id", 'dishAmount' + i);

    var labelAmount = document.createElement('label');
    labelAmount.className = 'mdl-textfield__label mdl-color-text--grey';
    labelAmount.setAttribute("for", 'dishAmount' + i);

    var placeholderAmount = 'Количество блюда';
    labelAmount.innerText = placeholderAmount;

    innerSecondDiv.appendChild(inputAmount);
    innerSecondDiv.appendChild(labelAmount);
    divDish.appendChild(innerSecondDiv);

    div = document.getElementById('newDish');
    cont = div.parentNode;
    cont.insertBefore(divDish, div);

    componentHandler.upgradeAllRegistered();
}

function addDishMealtime() {
    var tdDishName = document.createElement('td');
    tdDishName.className = 'text-left mdl-data-table__cell--non-numeric';

    var placeholderDishName = 'Название блюда';
    tdDishName.innerText = placeholderDishName;

    var tr = document.getElementById('newDishMealtime');
    var parentElem = tr.parentNode;
    parentElem.insertBefore(tdDishName, tr);

    var tdDishAmount = document.createElement('td');
    tdDishAmount.className = 'text-left mdl-data-table__cell--non-numeric';
    var placeholderDishAmount = 'Количество блюда';
    tdDishAmount.innerText = placeholderDishAmount;

    tr = document.getElementById('newDishMealtime');
    parentElem = tr.parentNode;
    parentElem.insertBefore(tdDishAmount, tr);

    componentHandler.upgradeAllRegistered();
}